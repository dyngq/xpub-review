import React from 'react'
import { Route } from 'react-router'
import { DragDropContext } from 'react-dnd'
import { Footer } from '@hindawi/ui'
import HTML5Backend from 'react-dnd-html5-backend'
import styled from 'styled-components'
import { AppBar } from 'component-authentication/client'
import { AutosaveIndicator, SubmitDraft } from 'component-submission/client'
import { queries } from 'component-dashboard/client'
import { th } from '@pubsweet/ui-toolkit'

import { useJournal } from 'component-journal-info'

const HideOnPath = ({ component: Component, pathname }) => (
  <Route
    render={({ location }) => {
      if (location.pathname === pathname) return null
      return <Component />
    }}
  />
)

const App = ({ autosave, journal = {}, goTo, children }) => {
  const {
    supportEmail,
    name,
    links: { websiteLink, privacyLink, termsLink },
  } = useJournal()
  return (
    <Root>
      <HideOnPath
        component={() => (
          <AppBar
            autosaveIndicator={AutosaveIndicator}
            queries={{
              getManuscripts: queries.getManuscripts,
            }}
            submitButton={SubmitDraft}
          />
        )}
        pathname="/404"
      />

      <PageContent>{children}</PageContent>

      <HideOnPath
        component={() => (
          <Footer
            privacyLink={privacyLink}
            publisher={name}
            supportEmail={supportEmail}
            termsLink={termsLink}
            websiteLink={websiteLink}
          />
        )}
        pathname="/404"
      />
    </Root>
  )
}

export default DragDropContext(HTML5Backend)(App)

const Root = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${th('backgroundColor')};
  font-family: ${th('defaultFont')}, sans-serif;
  font-size: ${th('mainTextSize')};
  color: ${th('colorText')};
  line-height: ${th('mainTextLineHeight')};

  height: 100vh;

  * {
    box-sizing: border-box;
  }
`

const PageContent = styled.main`
  flex: 1;
  overflow-y: auto;
`
