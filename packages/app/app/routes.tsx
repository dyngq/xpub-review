import React, { Fragment } from "react";
import { Switch, Redirect } from "react-router";
import { get } from "lodash";
import { Dashboard, EADashboard } from "component-dashboard/client";

import {
  Login,
  SignUp,
  InfoPage,
  ResetPassword,
  ConfirmAccount,
  SetNewPassword,
  SSOReviewerRedirect,
  AuthenticatedRoute,
  SignUpFromInvitation,
  TrackedRoute,
} from "component-authentication/client";
import { EQSDecision, EQADecision } from "component-screening/client";
import {
  EmailResponse,
  ManuscriptDetails,
  ManuscriptDetailsGateway,
} from "component-peer-review/client";
import {
  SubmissionConfirmation,
  Wizard,
  SubmissionFromURL,
} from "component-submission/client";
import {
  AdminDashboard,
  AdminUsers,
  AdminRoute,
  AdminJournals,
  AdminJournalDetails,
} from "component-admin/client";
import { UserProfilePage, ChangePassword } from "component-user-profile/client";
import { useKeycloak } from "component-sso/client";
import FourOFour from "./FourOFour";
import ReviewerRedirect from "./ReviewerRedirect";
import ManuscriptRedirect from "./ManuscriptRedirect";

const resolveDashboard = keycloak => {
  enum roles {
    Editorial_assitant = "editorial_assistant",
    User = "user",
  }

  const rolesDashboards: { [key in roles]: JSX.Element } = {
    [roles.Editorial_assitant]: <Redirect push to="/ea-dashboard" />,
    [roles.User]: <Dashboard />,
  };

  if (!keycloak?.authenticated) return rolesDashboards.user;

  const keycloakClientId = get(keycloak, "clientId");
  const keycloakClientResourceAccess = get(keycloak, `resourceAccess`);

  const keycloakClientRoles =
    keycloakClientResourceAccess[keycloakClientId]?.roles ?? [];

  // we only check if editorial assitant role exists.
  // there are users with multiple roles and we'll have to deal with them
  if (keycloakClientRoles.includes(roles.Editorial_assitant)) {
    return rolesDashboards[roles.Editorial_assitant];
  }
  return rolesDashboards.user;
};

const Routes = () => {
  const keycloak = useKeycloak();

  return (
    <Switch>
      <TrackedRoute component={InfoPage} exact path="/info-page" />
      <TrackedRoute component={EQSDecision} exact path="/eqs-decision" />
      <TrackedRoute component={EQADecision} exact path="/eqa-decision" />
      <TrackedRoute
        component={SubmissionConfirmation}
        exact
        path="/confirmation-page"
      />
      {keycloak && (
        <TrackedRoute
          component={SSOReviewerRedirect}
          exact
          path="/emails/accept-review-new-user"
        />
      )}
      <TrackedRoute component={EmailResponse} exact path="/emails/:action" />
      <TrackedRoute
        component={ManuscriptRedirect}
        exact
        path="/projects/:projectId/versions/:versionId/details"
      />
      <TrackedRoute
        component={ReviewerRedirect}
        exact
        path="/invite-reviewer"
      />

      <AdminRoute component={AdminUsers} exact path="/admin/users" />
      <AdminRoute component={AdminJournals} exact path="/admin/journals" />
      <AdminRoute
        component={AdminJournalDetails}
        exact
        path="/admin/journals/:journalId"
      />
      <AdminRoute component={AdminDashboard} exact path="/admin" />

      <AuthenticatedRoute
        exact
        path="/"
        render={() => resolveDashboard(keycloak)}
      />
      <AuthenticatedRoute component={EADashboard} exact path="/ea-dashboard" />
      {/* Temporary route for redirect */}
      <AuthenticatedRoute
        component={ManuscriptDetailsGateway}
        exact
        path="/details/:customId"
      />

      <AuthenticatedRoute
        component={ManuscriptDetails}
        exact
        path="/details/:submissionId/:manuscriptId"
      />
      <AuthenticatedRoute
        component={ChangePassword}
        exact
        path="/profile/change-password"
      />
      <AuthenticatedRoute
        component={Wizard}
        exact
        path="/submit/:submissionId/:manuscriptId"
      />
      <AuthenticatedRoute component={SubmissionFromURL} exact path="/submit" />
      <AuthenticatedRoute component={UserProfilePage} exact path="/profile" />

      <TrackedRoute component={ConfirmAccount} exact path="/confirm-signup" />

      <TrackedRoute exact path="/login">
        {keycloak ? (
          <Redirect to="/" />
        ) : (
          <TrackedRoute component={Login} exact path="/login" />
        )}
      </TrackedRoute>

      {!keycloak && (
        <Fragment>
          <TrackedRoute component={SignUpFromInvitation} exact path="/invite" />
          <TrackedRoute component={SignUp} exact path="/signup" />
          <TrackedRoute
            component={ResetPassword}
            exact
            path="/password-reset"
          />
          <TrackedRoute
            component={SetNewPassword}
            exact
            path="/forgot-password"
          />
        </Fragment>
      )}
      <TrackedRoute component={ConfirmAccount} exact path="/invite" />
      <TrackedRoute component={FourOFour} exact path="/404" />
      <Redirect to="/404" />
    </Switch>
  );
};

export default Routes;
