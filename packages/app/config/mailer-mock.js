module.exports = {
  transport: {
    name: 'mailer-mock',
    version: '0.1.0',
    send: (mail, callback) => {
      callback(null, true)
    },
  },
}
