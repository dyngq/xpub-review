ALTER TABLE job
  ADD COLUMN journal_id uuid REFERENCES journal (id) ON DELETE CASCADE;
