ALTER TABLE manuscript
  ADD COLUMN special_issue_id uuid REFERENCES special_issue (id) ON DELETE CASCADE;

