exports.up = async knex => {
  try {
    await knex.schema.table('job', table => {
      table.dropForeign('team_member_id', 'job_team_member_id_fkey')
      table
        .foreign('team_member_id')
        .references('id')
        .inTable('team_member')
        .onDelete('CASCADE')
    })
  } catch (e) {
    throw new Error(e)
  }
}
