const logger = require('@pubsweet/logger')

exports.up = async knex =>
  knex.schema
    .table('comment', table => {
      table.index('review_id', null, 'hash')
      table.index('type', null, 'hash')
    })
    .then(() => logger.info('comment indexes added successfully'))
