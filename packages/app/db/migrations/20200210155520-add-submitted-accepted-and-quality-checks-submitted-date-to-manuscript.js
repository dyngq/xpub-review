exports.up = async knex =>
  knex.schema
    .table('manuscript', table => {
      table.timestamp('submitted_date')
      table.timestamp('accepted_date')
      table.timestamp('quality_checks_submitted_date')
    })
    .then(async () =>
      knex('manuscript').update({
        submitted_date: knex.raw(
          `to_timestamp(trunc(cast(publication_dates->0->>'date' as double precision)/1000))`,
        ),
      }),
    )
    .then(() =>
      knex.schema.table('manuscript', table => {
        table.dropColumn('publication_dates')
      }),
    )
