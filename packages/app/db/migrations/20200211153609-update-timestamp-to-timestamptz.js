exports.up = async knex => {
  try {
    await knex.schema.alterTable('team_member', table => {
      table.timestamp('responded').alter()
    })

    await knex.schema.alterTable('review', table => {
      table.timestamp('submitted').alter()
    })
  } catch (e) {
    throw new Error(e)
  }
}
