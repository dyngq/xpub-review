const config = require('config')

const publisherName = config.get('publisherName')

exports.seed = async knex => {
  await knex('article_type').insert([
    {
      name: 'Corrigendum',
      has_peer_review: false,
    },
    {
      name: 'Retraction',
      has_peer_review: false,
    },
    {
      name: 'Research Article',
      has_peer_review: true,
    },
    {
      name: 'Expression of Concern',
      has_peer_review: false,
    },
    {
      name: 'Case Series',
      has_peer_review: true,
    },
    {
      name: 'Editorial',
      has_peer_review: false,
    },
    {
      name: 'Letter to the Editor',
      has_peer_review: false,
    },
    {
      name: 'Case Report',
      has_peer_review: true,
    },
    {
      name: 'Review Article',
      has_peer_review: true,
    },
    {
      name: 'Erratum',
      has_peer_review: false,
    },
  ])
  if (publisherName === 'gsw') {
    await knex('article_type').insert([
      {
        name: 'Commentary',
        has_peer_review: false,
      },
    ])
  }
}
