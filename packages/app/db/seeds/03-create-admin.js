const uuid = require('uuid')
const config = require('config')

exports.seed = async knex => {
  const userId = uuid.v4()
  const adminEmail = config.get('adminEmail')
  await knex('user').insert([
    {
      id: userId,
      agree_tc: true,
      is_active: true,
      default_identity_type: 'local',
      is_subscribed_to_emails: true,
      unsubscribe_token: uuid.v4(),
    },
  ])

  const alias = {
    given_names: 'Admin',
    surname: 'Admin',
    title: 'dr',
    aff: 'Affiliation',
    country: 'UK',
    email: adminEmail,
  }
  await knex('identity').insert([
    {
      user_id: userId,
      type: 'local',
      is_confirmed: true,
      ...alias,
      password_hash:
        '$2b$12$.Ll6THdFQ1Tk26WSst75tu9/PvNjAjqu2xhkf9CZAIlomEh.Ah0pS',
    },
  ])

  const teamId = uuid.v4()
  await knex('team').insert([
    {
      id: teamId,
      role: 'admin',
    },
  ])

  await knex('team_member').insert([
    {
      alias,
      team_id: teamId,
      user_id: userId,
      status: 'pending',
    },
  ])
}
