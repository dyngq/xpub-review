exports.seed = async knex => {
  await knex('peer_review_model')
    .whereIn('name', ['Lithosphere Model', 'Section Editor'])
    .update({ has_sections: true })
}
