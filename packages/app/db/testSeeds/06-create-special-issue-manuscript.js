const Chance = require('chance')

const chance = new Chance()
const { createTeam, createTeamMember, createUser } = require('../seedUtils')

exports.seed = async knex => {
  const specialIssues = await knex.select().table('special_issue')
  if (specialIssues.length === 0) {
    throw new Error('No special Issues have been found')
  }
  const specialIssueId = specialIssues[0].id
  const { journalId } = specialIssues[0]

  const articleTypes = await knex.select().table('article_type')
  if (articleTypes.length === 0) {
    throw new Error('No article types have been found')
  }
  const articleTypeId = articleTypes[0].id

  const manuscript = await knex('manuscript')
    .insert([
      {
        submissionId: chance.guid(),
        status: 'submitted',
        version: '1',
        title: 'on SI',
        abstract: chance.paragraph(),
        journalId,
        agreeTc: true,
        articleTypeId,
        specialIssueId,
      },
    ])
    .returning('*')
    .then(results => results[0])

  const triageEditorUserAndIdentity = await createUser({ knex })
  const academicEditorUserAndIdentity = await createUser({ knex })
  const authorUserAndIdentity = await createUser({ knex })

  const specialIssueEditorialTeam = await createTeam({
    knex,
    specialIssueId,
    role: 'triageEditor',
  })
  await createTeamMember({
    knex,
    teamId: specialIssueEditorialTeam.id,
    userId: triageEditorUserAndIdentity.user.id,
    status: 'pending',
  })
  const manuscriptTriageEditorTeam = await createTeam({
    knex,
    manuscriptId: manuscript.id,
    role: 'triageEditor',
  })
  await createTeamMember({
    knex,
    teamId: manuscriptTriageEditorTeam.id,
    userId: triageEditorUserAndIdentity.user.id,
    status: 'active',
  })
  const specialIssueAcademicEditorTeam = await createTeam({
    knex,
    specialIssueId,
    role: 'academicEditor',
  })
  await createTeamMember({
    knex,
    teamId: specialIssueAcademicEditorTeam.id,
    userId: academicEditorUserAndIdentity.user.id,
    status: 'pending',
  })
  const manuscriptAuthorTeam = await createTeam({
    knex,
    manuscriptId: manuscript.id,
    role: 'author',
  })

  await createTeamMember({
    knex,
    teamId: manuscriptAuthorTeam.id,
    userId: authorUserAndIdentity.user.id,
    status: 'pending',
  })
}
