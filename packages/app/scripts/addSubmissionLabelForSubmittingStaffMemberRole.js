const { db } = require('@pubsweet/db-manager')

const execute = async () => {
  const { id: roleId } = await db
    .select('id')
    .from('role')
    .where('name', 'submittingStaffMember')
    .limit(1)
    .first()
  const { id: submissionStatusId } = await db
    .select('id')
    .from('submission_status')
    .where('name', 'draft')
    .limit(1)
    .first()
  const { id: submissionFilterId } = await db
    .select('id')
    .from('submission_filter')
    .where('name', 'inProgress')
    .limit(1)
    .first()

  await db('submission_label').insert({
    roleId,
    submissionStatusId,
    submissionFilterId,
    priority: 0,
    name: 'Complete Submission',
  })

  process.exit()
}
execute()
