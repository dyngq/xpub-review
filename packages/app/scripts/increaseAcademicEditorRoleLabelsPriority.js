const { db } = require('@pubsweet/db-manager')

const execute = async () => {
  const { id: roleId } = await db
    .select('id', 'name')
    .from('role')
    .where('name', 'academicEditor')
    .limit(1)
    .first()

  await db('submission_label')
    .where('role_id', roleId)
    .update('priority', 1)

  process.exit()
}

execute()
