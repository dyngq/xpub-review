const { db } = require('@pubsweet/db-manager')
const { updateUserRole } = require('component-sso')

const findUsersWithRole = db => async role =>
  db
    .select(db.raw('DISTINCT ON (i.email) i.email'))
    .from('identity as i')
    .join('user as u', 'i.userId', 'u.id')
    .join('team_member as tm', 'u.id', 'tm.userId')
    .join('team as t', 'tm.teamId', 't.id')
    .whereNotNull('i.email')
    .andWhere('t.role', role)
    .andWhere(builder => builder.whereIn('tm.status', ['active', 'accepted']))

const setRoleToSSO = ({ keycloak, role }) => async user => {
  try {
    await keycloak.updateUserRole(user.email, role)
  } catch (err) {
    console.error(err)
  }
  console.info(`Added role ${role} to users with email ${user.email}.`)
}

const treatRole = ({ db, keycloak, rolesToUpdate }) => async role => {
  const userEmailsOfThisRole = await findUsersWithRole(db)(role)

  console.info(`Found ${userEmailsOfThisRole.length} users with role ${role}.`)

  await Promise.all(
    userEmailsOfThisRole.map(
      setRoleToSSO({ keycloak, role: rolesToUpdate[role] }),
    ),
  )
}

const initialize = ({ db, keycloak }) => ({
  execute: async ({ rolesToUpdate }) => {
    const localRolesToBeUpdated = Object.keys(rolesToUpdate)

    await Promise.all(
      localRolesToBeUpdated.map(treatRole({ db, keycloak, rolesToUpdate })),
    )

    process.exit()
  },
})

const keycloak = { updateUserRole }

initialize({ db, keycloak }).execute({
  // add here whatever roles you need to update
  rolesToUpdate: {
    editorialAssistant: 'editorial_assistant',
  },
})
