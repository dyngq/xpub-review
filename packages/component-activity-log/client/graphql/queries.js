import gql from 'graphql-tag'
import { auditLogsFragment } from './fragments'

export const getAuditLogs = gql`
  query getAuditLogs($submissionId: String!) {
    getAuditLogs(submissionId: $submissionId) {
      ...auditLogs
    }
  }
  ${auditLogsFragment}
`
