import { compose, withProps } from 'recompose'

import withGQL from '../graphql'
import { ActivityLog } from '../components'

export default compose(
  withGQL,
  withProps(
    ({
      data: { getAuditLogs, loading, error },
      peerReviewModel,
      triageEditorLabel,
      academicEditorLabel,
    }) => ({
      events: getAuditLogs,
      loading,
      error,
      peerReviewModel,
      triageEditorLabel,
      academicEditorLabel,
    }),
  ),
)(ActivityLog)
