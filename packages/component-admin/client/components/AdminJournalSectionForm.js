import React, { Fragment } from 'react'
import {
  Row,
  ValidatedFormField,
  validators,
  Item,
  Label,
  FormModal,
} from '@hindawi/ui'
import { TextField } from '@pubsweet/ui'
import { setSectionInitialValues } from './utils'

const AdminJournalSectionForm = ({
  handleAddSection,
  handleEditSection,
  editMode,
  confirmText,
  hideModal,
  title,
  section,
}) => {
  const onSubmit = (values, props) =>
    editMode
      ? handleEditSection(values, props)
      : handleAddSection(values, props)

  return (
    <FormModal
      cancelText="CANCEL"
      confirmText={confirmText}
      content={FormFields}
      hideModal={hideModal}
      initialValues={!!section && setSectionInitialValues(section)}
      onSubmit={onSubmit}
      title={title}
    />
  )
}

const FormFields = () => (
  <Fragment>
    <Row alignItems="baseline" mt={6}>
      <Item mr={2} vertical>
        <Label required>Section Name</Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="section-name-input"
          inline
          name="name"
          validate={[validators.required]}
        />
      </Item>
    </Row>
  </Fragment>
)

export default AdminJournalSectionForm
