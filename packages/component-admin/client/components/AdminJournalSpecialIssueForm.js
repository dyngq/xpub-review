import React, { Fragment } from 'react'
import styled from 'styled-components'

import {
  Row,
  Menu,
  Item,
  Label,
  Textarea,
  FormModal,
  DatePicker,
  validators,
  ValidatedFormField,
} from '@hindawi/ui'
import { TextField } from '@pubsweet/ui'
import { setSpecialIssuesInitialValues } from './utils'

const AdminJournalSpecialIssueForm = ({
  handleEditSpecialIssue,
  handleAddSpecialIssue,
  specialIssue = {},
  confirmText,
  hideModal,
  sections,
  editMode,
  title,
}) => {
  const onSubmit = (values, props) =>
    editMode
      ? handleEditSpecialIssue(values, props)
      : handleAddSpecialIssue(values, props)

  const sectionsList = sections.map(section => ({
    label: section.name,
    value: section.id,
  }))

  return (
    <FormModal
      cancelText="CANCEL"
      confirmText={confirmText}
      content={FormFields}
      hideModal={hideModal}
      initialValues={setSpecialIssuesInitialValues(specialIssue)}
      onSubmit={onSubmit}
      sections={sectionsList}
      specialIssue={specialIssue}
      title={title}
    />
  )
}

const FormFields = ({ sections, specialIssue }) => (
  <Fragment>
    <Row alignItems="baseline" mt={6}>
      <Item mr={2} vertical>
        <Label required>Special Issue Name</Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="special-issue-name-input"
          inline
          name="name"
          validate={[validators.required]}
        />
      </Item>
    </Row>
    <Row>
      <Item mr={4} vertical>
        <Label disabled={specialIssue.isActive} required>
          Submission start date
        </Label>
        <ValidatedFormField
          component={DatePicker}
          data-test-id="special-issue-start-date"
          disabled={specialIssue.isActive}
          name="startDate"
          validate={[validators.required]}
        />
      </Item>
      <Item mr={2} vertical>
        <Label required>Submission end date</Label>
        <ValidatedFormField
          component={DatePicker}
          data-test-id="special-issue-end-date"
          minDate={new Date(+new Date() + 86400000)}
          name="endDate"
          validate={[validators.required]}
        />
      </Item>
    </Row>
    {sections.length > 0 && (
      <Row alignItems="baseline">
        <Item mr={2} vertical>
          <Label disabled={specialIssue.isActive} pb={2}>
            Link to Section
          </Label>
          <ValidatedFormField
            component={StyledMenu}
            data-test-id="section-input"
            disabled={specialIssue.isActive}
            inline
            name="sectionId"
            options={sections}
            placeholder="None"
          />
        </Item>
      </Row>
    )}
    <Row>
      <Item mr={2} vertical>
        <Label pb={2} required>
          Call for Papers
        </Label>
        <ValidatedFormField
          component={Textarea}
          data-test-id="call-for-papers-input"
          inline
          minHeight={70}
          name="callForPapers"
          placeholder="Type something here..."
          validate={[validators.required]}
        />
      </Item>
    </Row>
  </Fragment>
)

const StyledMenu = styled(Menu)`
  width: 100%;
`

export default AdminJournalSpecialIssueForm
