import React from 'react'
import { useQuery } from 'react-apollo'
import { get } from 'lodash'
import { Menu, ValidatedFormField, validators } from '@hindawi/ui'

import { getPeerReviewModels } from '../graphql/queries'

const PeerReviewModels = ({ journalStatus }) => {
  const { data } = useQuery(getPeerReviewModels)
  const peerReviewModels = get(data, 'getPeerReviewModels', []).map(prm => ({
    label: prm.name,
    value: prm.id,
  }))
  return (
    <ValidatedFormField
      component={Menu}
      data-test-id="prm-input"
      disabled={journalStatus}
      inline
      name="peerReviewModelId"
      options={peerReviewModels}
      placeholder="Select peer review"
      validate={[validators.required]}
    />
  )
}
export default PeerReviewModels
