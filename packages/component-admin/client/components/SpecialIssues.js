import React from 'react'
import styled from 'styled-components'
import { flatMap } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import { ActionLink, Icon } from '@hindawi/ui'
import { Modal } from 'component-modal'
import AdminJournalSpecialIssueForm from './AdminJournalSpecialIssueForm'
import SpecialIssuesTable from './SpecialIssuesTable'
import useAddSpecialIssue from './useAddSpecialIssue'

const SpecialIssues = ({
  journalId,
  sections,
  loading,
  journalSpecialIssues,
}) => {
  const { handleAddSpecialIssue } = useAddSpecialIssue(journalId)
  const sectionSpecialIssues = flatMap(
    sections,
    section => section.specialIssues,
  )
  const specialIssues = [...journalSpecialIssues, ...sectionSpecialIssues]
  return (
    <Root>
      <Modal
        component={AdminJournalSpecialIssueForm}
        confirmText="SAVE"
        handleAddSpecialIssue={handleAddSpecialIssue}
        modalKey="addJournalSpecialIssues"
        sections={sections}
        title="Add Special Issue"
      >
        {showModal => (
          <ActionLink fontWeight="bold" mb={2} mt={2} onClick={showModal}>
            <Icon fontSize="11px" icon="expand" mr={1} />
            Add special issue
          </ActionLink>
        )}
      </Modal>
      <SpecialIssuesTable
        journalId={journalId}
        loading={loading}
        sections={sections}
        specialIssues={specialIssues}
      />
    </Root>
  )
}

const Root = styled.div`
  min-height: calc(${th('gridUnit')} * 35);
  padding: calc(${th('gridUnit')} * 3);
`

export default SpecialIssues
