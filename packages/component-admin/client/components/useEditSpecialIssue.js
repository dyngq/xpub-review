import { useMutation } from 'react-apollo'
import { useParams } from 'react-router-dom'
import { mutations, queries } from '../graphql'
import { parseError } from './utils'

const useEditSpecialIssue = () => {
  const [editSpecialIssue] = useMutation(mutations.editSpecialIssue)
  const { journalId } = useParams()

  const handleEditSpecialIssue = (values, modalProps) => {
    const { id, name, startDate, endDate, sectionId, callForPapers } = values
    modalProps.setFetching(true)
    editSpecialIssue({
      variables: {
        id,
        input: {
          name,
          endDate,
          startDate,
          sectionId,
          callForPapers,
        },
      },
      refetchQueries: [
        {
          query: queries.getJournal,
          variables: {
            journalId,
          },
        },
      ],
    })
      .then(() => {
        modalProps.setFetching(false)
        modalProps.hideModal()
      })
      .catch(error => {
        modalProps.setFetching(false)
        modalProps.setError(parseError(error))
      })
  }

  return { handleEditSpecialIssue }
}

export default useEditSpecialIssue
