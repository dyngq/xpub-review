import { graphql } from 'react-apollo'
import { compose } from 'recompose'

import * as queries from './queries'
import * as mutations from './mutations'

export default compose(
  graphql(queries.getPeerReviewModels),
  graphql(queries.getArticleTypes),
  graphql(queries.getJournals),
  graphql(mutations.addJournal, {
    name: 'addJournal',
    options: {
      refetchQueries: [{ query: queries.getJournals }],
    },
  }),
  graphql(mutations.editJournal, {
    name: 'editJournal',
    options: {
      refetchQueries: [{ query: queries.getJournals }],
    },
  }),
)
