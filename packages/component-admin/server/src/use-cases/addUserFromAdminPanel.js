const uuid = require('uuid')

const initialize = ({
  useCases,
  SSOService,
  eventsService,
  notification: notificationService,
  models: { User, Identity, Team },
}) => ({
  execute: async ({
    isAdmin,
    isRIPE,
    givenNames,
    email,
    surname,
    aff,
    title,
    country,
  }) => {
    if (isAdmin && isRIPE) {
      throw new Error(
        'User cannot be both admin and research integrity publishing editor.',
      )
    }

    const newUserDetails = {
      agreeTc: true,
      isActive: true,
      isSubscribedToEmails: true,
      unsubscribeToken: uuid.v4(),
      defaultIdentityType: 'local',
      confirmationToken: uuid.v4(),
    }

    const user = new User(newUserDetails)

    const identityProps = {
      type: 'local',
      isConfirmed: false,
      passwordHash: null,
      givenNames,
      surname,
      email,
      aff,
      title,
      country,
    }

    const identity = new Identity(identityProps)

    await user.insertWithIdentity(identity)

    let useCase
    if (isAdmin) useCase = 'addAdminFromAdminPanelUseCase'
    if (isRIPE) useCase = 'addRIPEFromAdminPanelUseCase'

    if (useCase) {
      await useCases[useCase]
        .initialize({ models: { Team }, eventsService })
        .execute({ user })
    }

    if (SSOService) {
      await SSOService.createSSOUser({
        identity: identityProps,
        user: newUserDetails,
      })
    } else {
      await notificationService.notifyUserAddedByAdmin({
        user,
        identity,
        isAdmin,
        isRIPE,
      })
    }
    eventsService.publishUserEvent({
      userId: user.id,
      eventName: 'UserAdded',
    })

    return User.findOneWithDefaultIdentity(user.id)
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
