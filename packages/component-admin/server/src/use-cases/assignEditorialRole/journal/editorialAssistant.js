const initialize = ({
  eventsService,
  models: { Team, TeamMember },
  redistributeEditorialAssistants,
}) => ({
  execute: async ({ user, journalId, role }) => {
    let journalTeam = await Team.findOneByJournalAndRole({ journalId, role })
    if (!journalTeam) journalTeam = new Team({ journalId, role })

    journalTeam.addMember({
      user,
      options: { status: TeamMember.Statuses.pending },
    })

    await journalTeam.saveGraph({
      noDelete: true,
      noUpdate: true,
      relate: true,
    })

    await redistributeEditorialAssistants.execute(journalId)
    eventsService.publishJournalEvent({
      journalId,
      eventName: 'JournalEditorialAssistantAssigned',
    })
  },
})

module.exports = {
  initialize,
}
