const initialize = ({
  models,
  useCases,
  eventsService,
  redistributeEditorialAssistants,
}) => ({
  execute: async ({ user, journalId, role }) => {
    const { Team, TeamMember } = models
    const existingUserJournalMember = await TeamMember.findOneByJournalAndUser({
      userId: user.id,
      journalId,
    })
    if (existingUserJournalMember)
      throw new Error(`User already has an editorial role on this journal.`)

    let useCase
    switch (role) {
      case Team.Role.editorialAssistant:
        useCase = 'assignEditorialAssistantOnJournalUseCase'
        break
      default:
        useCase = 'assignEditorOnJournalUseCase'
    }
    await useCases[useCase]
      .initialize({
        models,
        eventsService,
        redistributeEditorialAssistants,
      })
      .execute({ user, journalId, role })
  },
})

module.exports = {
  initialize,
}
