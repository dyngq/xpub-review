const initialize = ({ models: { Team, TeamMember } }) => ({
  execute: async ({ id: teamMemberId }) => {
    const teamMember = await TeamMember.find(teamMemberId)
    const team = await Team.find(teamMember.teamId)
    const newCorrespondingTeamMember = await team.changeCorrespondingTeamMember(
      teamMember,
    )

    return newCorrespondingTeamMember
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
