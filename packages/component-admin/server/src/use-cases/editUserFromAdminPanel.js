const initialize = ({ eventsService, models: { User, Team, TeamMember } }) => ({
  execute: async ({ input, id, userId }) => {
    const { isAdmin, isRIPE } = input
    if (isAdmin && isRIPE) {
      throw new ConflictError(
        'A user cannot be both admin and research integrity publishing editor.',
      )
    }

    const adminRole = Team.Role.admin
    const updatedUser = await User.find(id, 'identities')

    if (isAdmin === false) {
      if (id === userId)
        throw new ConflictError('Cannot remove own admin role.')

      await TeamMember.deleteStaffMemberByUserIdAndRole({
        role: adminRole,
        userId: id,
      })
    } else {
      const adminTeam = await Team.findOneBy({
        queryObject: { role: adminRole },
      })
      const adminMember = adminTeam.addMember({
        user: updatedUser,
      })
      await adminMember.save()
    }

    const ripeRole = Team.Role.researchIntegrityPublishingEditor
    if (isRIPE === false) {
      await TeamMember.deleteStaffMemberByUserIdAndRole({
        role: ripeRole,
        userId: id,
      })
    } else {
      const ripeTeam = await Team.findOrCreate({
        queryObject: {
          role: ripeRole,
        },
        options: {
          role: ripeRole,
        },
      })

      const ripeMember = ripeTeam.addMember({
        user: updatedUser,
      })
      await ripeMember.save()
    }

    delete input.isAdmin
    delete input.isRIPE

    const localIdentity = updatedUser.defaultIdentity
    localIdentity.updateProperties(input)
    await localIdentity.save()

    eventsService.publishUserEvent({
      userId: updatedUser.id,
      eventName: 'UserUpdated',
    })

    return User.findOneWithDefaultIdentity(updatedUser.id)
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
