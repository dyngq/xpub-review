const initialize = ({ Journal }) => ({
  execute: async () => {
    const journals = await Journal.findAll({
      orderByField: 'created',
      order: 'desc',
      eagerLoadRelations: '[peerReviewModel]',
    })
    return journals
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
