const initialize = ({ User, Identity }) => ({
  execute: async ({ page = 0, pageSize = 20, searchValue, orderBy, order }) => {
    /* We have to make sure that each user has a
     * corresponding local identity (when adding
     * features like signup with a 3rd party)
     */
    const users = await User.findAllWithDefaultIdentity({
      page,
      order,
      orderBy,
      pageSize,
      searchValue,
    })

    return {
      users: users.results,
      totalCount: users.total,
    }
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
