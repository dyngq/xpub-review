const initialize = ({
  models: { TeamMember, Section, SpecialIssue },
  eventsService,
}) => ({
  execute: async teamMemberId => {
    const teamMember = await TeamMember.find(teamMemberId, 'team')
    if (!teamMember) throw new Error('Cannot remove editor.')

    await teamMember.delete()

    let eventObject = {
      journalId: teamMember.team.journalId,
      eventName: 'JournalEditorRemoved',
    }

    if (teamMember.team.sectionId) {
      const section = await Section.find(teamMember.team.sectionId)
      eventObject.journalId = section.journalId
      eventObject.eventName = 'JournalSectionEditorRemoved'
    }

    if (teamMember.team.specialIssueId) {
      const specialIssue = await SpecialIssue.find(
        teamMember.team.specialIssueId,
      )
      eventObject = {
        journalId: specialIssue.journalId,
        eventName: 'JournalSpecialIssueEditorRemoved',
      }
      if (specialIssue.sectionId) {
        const section = await Section.find(specialIssue.sectionId)
        eventObject.journalId = section.journalId
        eventObject.eventName = 'JournalSectionSpecialIssueEditorRemoved'
      }
    }

    eventsService.publishJournalEvent(eventObject)
  },
})

module.exports = { initialize }
