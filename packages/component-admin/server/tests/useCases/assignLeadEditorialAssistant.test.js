process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { assignLeadEditorialAssistantUseCase } = require('../../src/use-cases')

const { Journal, Team } = models

describe('Assign lead editorial assistant', () => {
  it('should make the assigned editorial assistant corresponding', async () => {
    const journal = fixtures.generateJournal({ Journal })
    const editorialAssistant = dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
      isCorresponding: false,
    })

    await assignLeadEditorialAssistantUseCase
      .initialize({ models })
      .execute({ id: editorialAssistant.id })

    expect(editorialAssistant.isCorresponding).toEqual(true)
  })
  it('should change the corresponding editorial assistant when multiple ones exist', async () => {
    const journal = fixtures.generateJournal({ Journal })
    const editorialAssistant1 = dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
      isCorresponding: true,
    })
    const editorialAssistant2 = dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
      isCorresponding: false,
    })

    await assignLeadEditorialAssistantUseCase
      .initialize({ models })
      .execute({ id: editorialAssistant2.id })

    expect(editorialAssistant1.isCorresponding).toEqual(false)
    expect(editorialAssistant2.isCorresponding).toEqual(true)
  })
})
