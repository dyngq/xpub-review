const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { editSpecialIssueUseCase } = require('../../src/use-cases')

const chance = new Chance()
const eventsService = {
  publishJournalEvent: jest.fn(),
}

const generateInput = () => ({
  name: chance.company(),
  endDate: new Date(chance.date({ year: 2030, string: true })),
  startDate: new Date(),
  callForPapers: chance.paragraph(),
})
const { Team, SpecialIssue, PeerReviewModel, Journal, Section } = models
dataService.createGlobalUser({
  models,
  fixtures,
  role: Team.Role.admin,
})

describe('Edit special issue use case', () => {
  let input, jobsService
  let journal
  let specialIssue
  beforeEach(async () => {
    input = generateInput()
    jobsService = {
      scheduleSpecialIssueActivation: jest.fn(),
      scheduleSpecialIssueDeactivation: jest.fn(),
    }
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.academicEditor],
      },
      PeerReviewModel,
    })

    journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    input.journalId = journal.id
    specialIssue = await fixtures.generateSpecialIssue({
      SpecialIssue,
    })
  })

  it('should edit the special issue info and activate the special issue if start date is today', async () => {
    specialIssue.isActive = false
    specialIssue.endDate = input.endDate
    const editedSpecialIssue = await editSpecialIssueUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, id: specialIssue.id })

    expect(editedSpecialIssue.name).toEqual(input.name)
    expect(editedSpecialIssue.startDate).toEqual(input.startDate)
    expect(editedSpecialIssue.callForPapers).toEqual(input.callForPapers)

    expect(editedSpecialIssue.isActive).toBeTruthy()
    expect(jobsService.scheduleSpecialIssueActivation).toHaveBeenCalledTimes(0)
    expect(eventsService.publishJournalEvent).toHaveBeenCalledWith({
      journalId: specialIssue.journalId,
      eventName: `JournalSpecialIssueUpdated`,
    })
  })
  it('should schedule the activation of the SI if the new start date is different than the old one', async () => {
    specialIssue.isActive = false
    specialIssue.startDate = new Date(chance.date({ year: 2000, string: true }))
    input.startDate = new Date(chance.date({ year: 2030, string: true }))
    input.endDate = new Date(chance.date({ year: 2090, string: true }))

    await editSpecialIssueUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, id: specialIssue.id })

    expect(jobsService.scheduleSpecialIssueActivation).toHaveBeenCalledTimes(1)
    expect(eventsService.publishJournalEvent).toHaveBeenCalledWith({
      journalId: specialIssue.journalId,
      eventName: `JournalSpecialIssueUpdated`,
    })
  })
  it('should not activate or schedule the activation of the SI if the new start date is equal to the old one', async () => {
    specialIssue.isActive = false
    const startDate = new Date(chance.date({ year: 2000, string: true }))
    specialIssue.startDate = startDate
    input.startDate = startDate

    await editSpecialIssueUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, id: specialIssue.id })

    expect(jobsService.scheduleSpecialIssueActivation).toHaveBeenCalledTimes(0)
  })
  it('should link to a section and remove journal id', async () => {
    const section = await fixtures.generateSection({ Section })
    input.sectionId = section.id
    const specialIssueEdited = await editSpecialIssueUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, id: specialIssue.id })

    expect(specialIssueEdited.sectionId).toEqual(section.id)
    expect(specialIssueEdited.journalId).toEqual(null)
  })
  it('should activate the SI if the new end date is in the future and it is different from the old one, which is in the past', async () => {
    specialIssue.isActive = false
    specialIssue.isCancelled = false
    specialIssue.endDate = new Date(chance.date({ year: 2019, string: true }))

    input.endDate = new Date(chance.date({ year: 2100, string: true }))

    const editedSpecialIssue = await editSpecialIssueUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, id: specialIssue.id })

    expect(editedSpecialIssue.isActive).toBeTruthy()
    expect(editedSpecialIssue.isCancelled).toBeFalsy()
    expect(jobsService.scheduleSpecialIssueDeactivation).toHaveBeenCalledTimes(
      1,
    )
    expect(eventsService.publishJournalEvent).toHaveBeenCalledWith({
      journalId: specialIssue.journalId,
      eventName: `JournalSpecialIssueExtended`,
    })
  })
  it('should not activate the SI if the new end date is in the future, it is different from the old one, but the start date is also in the future', async () => {
    specialIssue.isActive = false
    specialIssue.isCancelled = false
    const startDate = new Date(chance.date({ year: 2088, string: true }))
    specialIssue.startDate = startDate
    specialIssue.endDate = new Date(chance.date({ year: 2099, string: true }))

    input.startDate = startDate
    input.endDate = new Date(chance.date({ year: 2100, string: true }))

    const editedSpecialIssue = await editSpecialIssueUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, id: specialIssue.id })

    expect(editedSpecialIssue.isActive).toBeFalsy()
    expect(editedSpecialIssue.isCancelled).toBeFalsy()
    expect(jobsService.scheduleSpecialIssueActivation).toHaveBeenCalledTimes(0)
  })
})
