const { models, fixtures } = require('fixture-service')

const { getUsersForAdminPanelUseCase } = require('../../src/use-cases')

describe('get users for admin panel', () => {
  it('returns the existing users with correct total count', async () => {
    let team
    const { Team, User, Identity, TeamMember } = models
    team = fixtures.getTeamByRole(Team.Role.admin)
    if (!team)
      team = fixtures.generateTeam({
        properties: { role: Team.Role.admin },
        Team,
      })

    fixtures.generateUser({ User, Identity })
    fixtures.generateUser({ User, Identity })
    const user = fixtures.generateUser({ User, Identity })

    const teamMember = fixtures.generateTeamMember({
      properties: {
        userId: user.id,
        teamId: team.id,
      },
      TeamMember,
    })
    team.members.push(teamMember)

    const usersResult = await getUsersForAdminPanelUseCase
      .initialize(models)
      .execute({})

    expect(usersResult.totalCount).toEqual(3)
    expect(usersResult.users).toHaveLength(3)
  })
})
