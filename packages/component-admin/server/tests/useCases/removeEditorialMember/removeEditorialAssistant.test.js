const {
  getTeamRoles,
  generateTeamMember,
  generateManuscript,
  getTeamMemberStatuses,
  getManuscriptStatuses,
  getManuscriptInProgressStatuses,
} = require('component-generators')
const { removeEditorialAssistantUseCase } = require('../../../src/use-cases')

const models = {
  Manuscript: {
    InProgressStatuses: getManuscriptInProgressStatuses(),
    Statuses: getManuscriptStatuses(),
    findAllByJournalAndUserAndRole: jest.fn(),
  },
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: jest.fn(),
}
Object.assign(models.TeamMember, {
  Statuses: getTeamMemberStatuses(),
  find: jest.fn(),
  findAllByJournalAndRole: jest.fn(),
  findTeamMembersWorkloadByJournal: jest.fn(),
  findOneByUserAndRoleAndStatusOnManuscript: jest.fn(),
  removeEditorialAssistantTransaction: jest.fn(),
})
const { Team, TeamMember, Manuscript } = models

const eventsService = {
  publishJournalEvent: jest.fn(),
  publishSubmissionEvent: jest.fn(),
}

const IN_PROGRESS_STATUSES = [
  ...Manuscript.InProgressStatuses,
  Manuscript.Statuses.qualityChecksRequested,
  Manuscript.Statuses.qualityChecksSubmitted,
  Manuscript.Statuses.technicalChecks,
  Manuscript.Statuses.olderVersion,
  Manuscript.Statuses.draft,
]

describe('Remove Editorial Assistant use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('should remove the EA from journal and add another one on manuscript', async () => {
    const manucriptEditorialAssistant = generateTeamMember({
      id: 'some-manuscript-team-member-id',
      userId: 'some-user-id',
      teamId: 'some-manuscript-team-id',
      team: {
        manuscriptId: 'some-manuscript-id',
        role: Team.Role.editorialAssistant,
      },
    })
    const journalEditorialAssistant = generateTeamMember({
      id: 'some-team-member-id',
      userId: 'some-user-id',
      team: {
        journalId: 'some-journal-id',
        role: Team.Role.editorialAssistant,
      },
    })
    const otherJournalEditorialAssistant = generateTeamMember({
      id: 'some-other-team-member-id',
      userId: 'some-other-user-id',
      team: {
        journalId: 'some-journal-id',
        role: Team.Role.editorialAssistant,
      },
    })
    const lastManuscript = generateManuscript({
      id: 'some-manuscript-id',
      submissionId: 'some-submission-id',
      status: Manuscript.Statuses.technicalChecks,
    })

    jest
      .spyOn(TeamMember, 'find')
      .mockResolvedValueOnce(journalEditorialAssistant)
    jest
      .spyOn(TeamMember, 'findAllByJournalAndRole')
      .mockResolvedValueOnce([
        journalEditorialAssistant,
        otherJournalEditorialAssistant,
      ])
    jest
      .spyOn(Manuscript, 'findAllByJournalAndUserAndRole')
      .mockResolvedValueOnce([lastManuscript])

    jest
      .spyOn(TeamMember, 'findTeamMembersWorkloadByJournal')
      .mockResolvedValueOnce([
        {
          userId: 'some-user-id',
          workload: '1',
        },
      ])
    jest
      .spyOn(TeamMember, 'findOneByUserAndRoleAndStatusOnManuscript')
      .mockResolvedValueOnce(manucriptEditorialAssistant)
    jest
      .spyOn(TeamMember, 'removeEditorialAssistantTransaction')
      .mockResolvedValueOnce()

    await removeEditorialAssistantUseCase
      .initialize({
        models,
        eventsService,
      })
      .execute('some-team-member-id')

    expect(TeamMember.find).toHaveBeenCalledTimes(1)
    expect(TeamMember.find).toHaveBeenCalledWith('some-team-member-id', 'team')

    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledWith({
      journalId: 'some-journal-id',
      role: Team.Role.editorialAssistant,
    })

    expect(Manuscript.findAllByJournalAndUserAndRole).toHaveBeenCalledTimes(1)
    expect(Manuscript.findAllByJournalAndUserAndRole).toHaveBeenCalledWith({
      journalId: 'some-journal-id',
      userId: 'some-user-id',
      role: Team.Role.editorialAssistant,
    })

    expect(TeamMember.findTeamMembersWorkloadByJournal).toHaveBeenCalledTimes(1)
    expect(TeamMember.findTeamMembersWorkloadByJournal).toHaveBeenCalledWith({
      role: Team.Role.editorialAssistant,
      journalId: 'some-journal-id',
      teamMemberStatuses: [TeamMember.Statuses.active],
      manuscriptStatuses: IN_PROGRESS_STATUSES,
    })

    expect(
      TeamMember.findOneByUserAndRoleAndStatusOnManuscript,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findOneByUserAndRoleAndStatusOnManuscript,
    ).toHaveBeenCalledWith({
      manuscriptId: 'some-manuscript-id',
      role: 'editorialAssistant',
      status: TeamMember.Statuses.active,
      userId: 'some-user-id',
    })

    expect(
      TeamMember.removeEditorialAssistantTransaction,
    ).toHaveBeenCalledTimes(1)
  })
  it('should throw an error if trying to remove the only EA on the journal', async () => {
    const journalEditorialAssistant = generateTeamMember({
      id: 'some-team-member-id',
      userId: 'some-user-id',
      team: {
        journalId: 'some-journal-id',
        role: Team.Role.editorialAssistant,
      },
    })
    jest
      .spyOn(TeamMember, 'find')
      .mockResolvedValueOnce(journalEditorialAssistant)
    jest
      .spyOn(TeamMember, 'findAllByJournalAndRole')
      .mockResolvedValueOnce([journalEditorialAssistant])

    const res = removeEditorialAssistantUseCase
      .initialize({
        models,
        eventsService,
      })
      .execute('some-team-member-id')

    return expect(res).rejects.toThrowError(
      "You can't remove the only Editorial Assistant on the journal.",
    )
  })
})
