The set new password form.

```js
<SetNewPasswordForm
  setPassword={(values, props) => console.log('set password', values)}
/>
```

Set password request pending.

```js
<SetNewPasswordForm
  isFetching
  setPassword={(values, props) => console.log('set password', values)}
/>
```

Set password form with error.

```js
<SetNewPasswordForm
  fetchingError="Oops! Something went wrong..."
  setPassword={(values, props) => console.log('set password', values)}
/>
```
