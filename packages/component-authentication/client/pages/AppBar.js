import React from 'react'
import { get } from 'lodash'
import { Query } from 'react-apollo'
import { withRouter } from 'react-router'
import { useJournal } from 'component-journal-info'
import { compose, withProps, withHandlers } from 'recompose'
import { useKeycloak } from 'component-sso/client'

import { queries } from '../graphql'
import { AuthenticatedAppBar, UnauthenticatedAppBar } from '../components'

const unauthedPaths = [
  '/login',
  '/signup',
  '/invite',
  '/eqs-decision',
  '/confirm-signup',
  '/password-reset',
  '/forgot-password',
  '/emails/decline-review',
  '/emails/accept-review-new-user',
]
const AppBar = ({
  goTo,
  logout,
  submitButton,
  goToDashboard,
  isUnauthedRoute,
  autosaveIndicator,
}) => {
  const { logo, name } = useJournal()
  return isUnauthedRoute ? (
    <UnauthenticatedAppBar goTo={goTo} logo={logo} publisher={name} />
  ) : (
    <Query query={queries.currentUser}>
      {({ data, loading, client }) => {
        const currentUser = get(data, 'currentUser', null)
        return loading || !currentUser ? null : (
          <AuthenticatedAppBar
            autosaveIndicator={autosaveIndicator}
            currentUser={currentUser}
            goTo={goTo}
            goToDashboard={goToDashboard}
            logo={logo}
            logout={logout(client)}
            publisher={name}
            submitButton={submitButton}
          />
        )
      }}
    </Query>
  )
}

export default compose(
  withRouter,
  withProps(({ location }) => ({
    isUnauthedRoute: unauthedPaths.includes(location.pathname),
    keycloak: useKeycloak(),
  })),
  withHandlers({
    goToDashboard: ({ history }) => () => {
      history.push('/')
    },
    goTo: ({ history }) => path => {
      history.push(path)
    },
    logout: ({ history, keycloak }) => gqlClient => () => {
      gqlClient.resetStore()
      window.localStorage.removeItem('token')

      if (keycloak) {
        keycloak.logout()
      } else {
        history.replace('/login')
      }
    },
  }),
)(AppBar)
