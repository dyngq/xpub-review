import { useState } from 'react'

const SORT_VALUES = {
  ASC: 'asc',
  DESC: 'desc',
}
const STATUS_LABELS = {
  draft: 'Draft',
  technicalChecks: 'In Screening',
  inQA: 'In Quality Checks',
  refusedToConsider: 'Refused to Consider',
  rejected: 'Rejected',
  submitted: 'Submitted',
  academicEditorInvited: 'Academic Editor Invited',
  academicEditorAssigned: 'Academic Editor Agreed',
  reviewersInvited: 'Reviewers Invited',
  underReview: 'Reviewers Agreed',
  reviewCompleted: 'Reports Submitted',
  revisionRequested: 'Revision Requested',
  olderVersion: 'Older Version ',
  pendingApproval: 'Pending CE Approval ',
  makeDecision: 'Pending AE Decision',
  accepted: 'Accepted',
  qualityChecksRequested: 'QC Updates Required',
  qualityChecksSubmitted: 'QC Updates Submitted',
  published: 'Published',
  withdrawn: 'Withdrawn',
  deleted: 'Deleted',
  void: 'Void',
}

const MANUSCRIPT_PROPERTY_VALUES = {
  TITLE: 'title',
  CUSTOM_ID: 'customId',
  JOURNAL_NAME: 'journalName',
}

const hydrateFilters = () => {
  const defaultFilterValues = {
    sort: SORT_VALUES.DESC,
    status: 'all',
    manuscriptProperty: '',
  }

  const localFilterValues = localStorage.getItem('filters')

  // This check is temporary, we have to clear the filters from localStorage that contain authorName or authorEmail so dashboard don't crash
  const localDashboardFilters = JSON.parse(localFilterValues)

  const hasOldFilters =
    localDashboardFilters &&
    ['authorName', 'authorEmail'].includes(
      localDashboardFilters.manuscriptProperty,
    )
  if (hasOldFilters) {
    localStorage.setItem('filters', JSON.stringify(defaultFilterValues))
    return defaultFilterValues
  }
  if (localFilterValues) return JSON.parse(localFilterValues)

  return defaultFilterValues
}

const getSortOptions = () => [
  { label: 'Oldest first', value: SORT_VALUES.ASC },
  { label: 'Newest first', value: SORT_VALUES.DESC },
]

const getManuscriptPropertiesOptions = () => [
  { label: 'Manuscript title', value: MANUSCRIPT_PROPERTY_VALUES.TITLE },
  { label: 'Manuscript ID', value: MANUSCRIPT_PROPERTY_VALUES.CUSTOM_ID },
  { label: 'Journal name', value: MANUSCRIPT_PROPERTY_VALUES.JOURNAL_NAME },
]
const getStatusOptions = rawStatuses => {
  const allStatusesOption = { value: 'all', label: 'All' }
  const finalStatuses = rawStatuses
    .filter(rawStatus => rawStatus !== 'academicEditorAssignedEditorialType')
    .map(rawStatus => {
      if (!STATUS_LABELS[rawStatus])
        console.error(`No label set for ${rawStatus} in dropdown filter`) // eslint-disable-line
      return {
        value: rawStatus,
        label: STATUS_LABELS[rawStatus] || 'Unlabeled',
      }
    })
  return [allStatusesOption, ...finalStatuses]
}

const useFilters = statuses => {
  const [values, setFilterValues] = useState(hydrateFilters())

  const changeFilterValue = type => value => {
    const newValues = { ...values, [type]: value }
    setFilterValues(newValues)
    localStorage.setItem('filters', JSON.stringify(newValues))
  }

  const changeSort = changeFilterValue('sort')
  const changeStatus = changeFilterValue('status')
  const changeManuscriptProperty = changeFilterValue('manuscriptProperty')

  const options = {
    sort: getSortOptions(),
    status: getStatusOptions(statuses),
    manuscriptProperty: getManuscriptPropertiesOptions(),
  }

  return [values, options, changeSort, changeStatus, changeManuscriptProperty]
}

export default useFilters
