import gql from 'graphql-tag'

import { dashboardManuscriptDetails } from './fragments'

export const getManuscripts = gql`
  query getManuscripts($input: DashboardInput) {
    getManuscripts(input: $input) {
      manuscripts {
        ...dashboardManuscriptDetails
      }
      totalCount
      statuses
    }
  }
  ${dashboardManuscriptDetails}
`
