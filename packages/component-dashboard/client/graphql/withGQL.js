import { compose } from 'recompose'
import { graphql } from 'react-apollo'

import * as mutations from './mutations'

export default compose(
  graphql(mutations.deleteManuscript, {
    name: 'deleteManuscript',
  }),
  graphql(mutations.archiveManuscript, {
    name: 'archiveManuscript',
  }),
  graphql(mutations.withdrawManuscript, {
    name: 'withdrawManuscript',
  }),
)
