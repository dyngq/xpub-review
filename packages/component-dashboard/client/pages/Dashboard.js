import React, { Fragment, useEffect, useState } from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { Spinner } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { useLazyQuery } from 'react-apollo'
import { Text, Row, Item, Pagination } from '@hindawi/ui'

import withGQL from '../graphql/withGQL'
import { getManuscripts } from '../graphql/queries'
import {
  DashboardFilters,
  ManuscriptCard,
  usePagination,
  useFilters,
} from '../components'

const ITEMS_PER_PAGE = 10

const Dashboard = ({
  currentUser,
  filters,
  deleteManuscript,
  withdrawManuscript,
  archiveManuscript,
}) => {
  const [
    getManuscriptsMutation,
    { data, loading, called },
  ] = useLazyQuery(getManuscripts, { fetchPolicy: 'network-only' })
  const currentUserRole = get(currentUser, 'role')
  const [searchValue, setSearchValue] = useState('')
  const [isFirstFetch, setIsFirstFetch] = useState(true)
  const manuscripts = get(data, 'getManuscripts.manuscripts', [])
  const {
    nextPage,
    page,
    prevPage,
    setPage,
    toFirst,
    toLast,
    pageSize,
  } = usePagination(ITEMS_PER_PAGE, get(data, 'getManuscripts.totalCount', 0))
  const statuses = get(data, 'getManuscripts.statuses', [])

  const [
    values,
    options,
    changeSort,
    changeStatus,
    changeManuscriptProperty,
  ] = useFilters(statuses)

  const fetchManuscripts = ({ search }) => {
    if (currentUserRole === 'admin' && isFirstFetch) {
      setIsFirstFetch(false)
      return
    }

    getManuscriptsMutation({
      variables: {
        input: {
          searchValue: search || searchValue,
          statusFilter: get(values, 'status', 'all'),
          dateOrder: get(values, 'sort', 'desc'),
          manuscriptPropertyFilter: get(values, 'manuscriptProperty', ''),
          page,
          pageSize,
        },
      },
    })
  }

  useEffect(() => {
    fetchManuscripts({})
  }, [page])

  const handleDeleteManuscript = (manuscript, modalProps) => {
    if (manuscript.status === 'draft') {
      modalProps.setFetching(true)
      deleteManuscript({
        variables: {
          manuscriptId: manuscript.id,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          getManuscriptsMutation({
            variables: {
              input: {
                searchValue,
                statusFilter: get(values, 'status', 'all'),
                dateOrder: get(values, 'sort', 'desc'),
                manuscriptPropertyFilter: get(values, 'manuscriptProperty', ''),
                page,
                pageSize,
              },
            },
          })
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    } else if (
      manuscript.role === 'admin' ||
      manuscript.role === 'editorialAssistant'
    ) {
      modalProps.setFetching(true)
      archiveManuscript({
        variables: {
          submissionId: manuscript.submissionId,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          getManuscriptsMutation({
            variables: {
              input: {
                searchValue,
                statusFilter: get(values, 'status', 'all'),
                dateOrder: get(values, 'sort', 'desc'),
                manuscriptPropertyFilter: get(values, 'manuscriptProperty', ''),
                page,
                pageSize,
              },
            },
          })
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    }
  }

  const handleWithdrawManuscript = (manuscript, modalProps) => {
    modalProps.setFetching(true)
    withdrawManuscript({
      variables: {
        submissionId: manuscript.submissionId,
      },
    })
      .then(() => {
        modalProps.setFetching(false)
        getManuscriptsMutation({
          variables: {
            input: {
              searchValue,
              statusFilter: get(values, 'status', 'all'),
              dateOrder: get(values, 'sort', 'desc'),
              manuscriptPropertyFilter: get(values, 'manuscriptProperty', ''),
              page,
              pageSize,
            },
          },
        })
        modalProps.hideModal()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(e.message)
      })
  }

  return (
    <Fragment>
      <DashboardFilters
        changeManuscriptProperty={changeManuscriptProperty}
        changeSort={changeSort}
        changeStatus={changeStatus}
        fetchManuscripts={fetchManuscripts}
        goToFirstPage={toFirst}
        options={options}
        searchValue={searchValue}
        setSearchValue={setSearchValue}
        values={values}
      />
      <Root data-test-id="dashboard-list-items">
        {renderCards({
          data,
          noOfManuscripts: manuscripts.length,
          loading,
          called,
          handleDeleteManuscript,
          handleWithdrawManuscript,
        })}
        {get(data, 'getManuscripts.totalCount', 0) > ITEMS_PER_PAGE ? (
          <Item justify="flex-end" mb={2}>
            <Pagination
              itemsPerPage={ITEMS_PER_PAGE}
              nextPage={nextPage}
              page={page}
              prevPage={prevPage}
              setPage={setPage}
              toFirst={toFirst}
              toLast={toLast}
              totalCount={get(data, 'getManuscripts.totalCount', 0)}
            />
          </Item>
        ) : null}
      </Root>
    </Fragment>
  )
}

const renderCards = ({
  data,
  noOfManuscripts,
  loading,
  called,
  handleDeleteManuscript,
  handleWithdrawManuscript,
}) => {
  if (!called) {
    return (
      <Row mt={15}>
        <StyledText>
          Please use the Search bar to search for a manuscript...
        </StyledText>
      </Row>
    )
  }
  if (noOfManuscripts === 0 && loading)
    return (
      <Row mt={15}>
        <Spinner mt={4} size={4} />
      </Row>
    )
  if (noOfManuscripts === 0 && called) {
    return (
      <Row mt={15}>
        <StyledText>No results found</StyledText>
      </Row>
    )
  }

  return get(data, 'getManuscripts.manuscripts', []).map((m, index) => (
    <ManuscriptCard
      academicEditor={m.academicEditor}
      data-test-id="row"
      deleteManuscript={handleDeleteManuscript}
      isLast={index === get(data, 'getManuscripts.manuscripts', []).length - 1}
      key={m.id}
      manuscript={m}
      role={m.role}
      withdrawManuscript={handleWithdrawManuscript}
    />
  ))
}

export default withGQL(Dashboard)

// #region styles
const Root = styled.div`
  padding: 0 calc(${th('gridUnit')} * 20);
  padding-top: calc(${th('gridUnit')} * 8);
`

const StyledText = styled(Text)`
  font-weight: 700;
  font-size: 20px;
  color: ${th('actionSecondaryColor')};
`
// #endregion
