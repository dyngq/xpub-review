const initialize = () => ({
  getCopy({ emailType, titleText, journalName }) {
    let paragraph
    const hasSignature = true
    switch (emailType) {
      case 'author-manuscript-withdrawn':
      case 'reviewer-manuscript-withdrawn':
      case 'academic-editor-manuscript-withdrawn':
      case 'triage-editor-manuscript-withdrawn':
        paragraph = `Unfortunately, the manuscript titled ${titleText} has been withdrawn from ${journalName}. Accordingly, the review process has been discontinued.<br/><br/>
      We appreciate your efforts and apologise if you had already started reviewing the manuscript.<br/>
      Thank you for your contribution to our journal and we look forward to future collaborations.`
        break
      default:
        throw new Error(`The ${emailType} email type is not defined.`)
    }
    return { paragraph, hasSignature }
  },
})

module.exports = {
  initialize,
}
