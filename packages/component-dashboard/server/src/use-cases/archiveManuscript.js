const { flatMap } = require('lodash')

const initialize = ({
  models: { Manuscript, Team, TeamMember },
  jobsService,
}) => ({
  async execute(submissionId) {
    const draftManuscript = await Manuscript.findOneBySubmissionIdAndStatuses({
      submissionId,
      statuses: [Manuscript.Statuses.draft],
    })
    if (draftManuscript && draftManuscript.version === '1')
      throw new ValidationError('Draft manuscripts cannot be deleted.')

    const deletedManuscript = await Manuscript.findOneBySubmissionIdAndStatuses(
      {
        submissionId,
        statuses: [Manuscript.Statuses.deleted],
      },
    )
    if (deletedManuscript)
      throw new ValidationError('Manuscript already deleted.')

    let manuscripts = await Manuscript.findBy({ submissionId })
    manuscripts.forEach(m => (m.status = Manuscript.Statuses.deleted))
    manuscripts = await Manuscript.updateMany(manuscripts)

    // Jobs
    const reviewers = await TeamMember.findAllBySubmissionAndRoleAndStatuses({
      submissionId,
      role: Team.Role.reviewer,
      statuses: [TeamMember.Statuses.pending, TeamMember.Statuses.accepted],
      eagerLoadRelations: 'jobs',
    })

    jobsService.cancelJobs([...flatMap(reviewers, r => r.jobs)])
  },
})

const authsomePolicies = ['admin', 'isEditorialAssistant']

module.exports = {
  initialize,
  authsomePolicies,
}
