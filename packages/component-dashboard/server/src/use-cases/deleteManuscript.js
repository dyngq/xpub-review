const { get } = require('lodash')

const initialize = ({ Manuscript, User, Team, TeamMember }) => ({
  async execute({ manuscriptId, userId }) {
    const manuscript = await Manuscript.find(manuscriptId)

    if (!manuscript) throw new Error('Could not find manuscript')

    if (manuscript.status !== Manuscript.Statuses.draft)
      throw new ValidationError('Only draft manuscripts can be deleted')

    const isAdmin = !!(await TeamMember.findOneByUserAndRole({
      userId,
      role: Team.Role.admin,
    }))
    const isEditorialAssistant = !!(await TeamMember.findAllByManuscriptAndRole(
      {
        manuscriptId,
        role: Team.Role.editorialAssistant,
      },
    ))
    const submittingAuthor = await TeamMember.findSubmittingAuthor(manuscriptId)
    const isSubmittingAuthor = get(submittingAuthor, 'userId') === userId

    if (!isAdmin && !isEditorialAssistant && !isSubmittingAuthor)
      throw new Error('Not enough rights to delete this manuscript')

    await manuscript.delete()
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
