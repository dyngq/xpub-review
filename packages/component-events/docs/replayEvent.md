# How to replay an event

In order to replay an event that has not been processed by its handler, we need to first download the event from S3, then we need to parse the event, add the event and other data to the `replayEvent.js` script and then run the script.

## Download events from S3

To be able to download events from S3, we need to setup the AWS credentials on our local machine:

```
vim ~/.aws/credentials
aws_access_key_id = YOUR-KEY-ID
aws_secret_access_key = YOUR-ACCESS-KEY
region=your-region
```

Then we need to run the command which downloads the events from a certain date (i.e. August 13, 2020):

```
aws s3 cp s3://phenom-event-storage-prod-3/2020/08/13 ./s3 --recursive --profile hindawi
```

## Parse the event

```
const fs = require("fs");

const filePath =
  "../s3-08-19/22/serverlessrepo-prod-event-storage-pip-BackupStream-1FBAH1QA0ZPP2-3-2020-08-19-22-40-20-543c93aa-1607-4417-a476-8c9a41f1f60a";

const file = fs.readFileSync(filePath, "utf-8");

for (const line of file.split("\n")) {
  if (line === "") continue;
  const msg = JSON.parse(line);
  const body = JSON.parse(msg.body);

  // conditions to find a single event for a single submission
  if (
    body.event !== "pen:hindawi:hindawi-review:SubmissionQualityChecksSubmitted"
  ) {
    continue;
  }

  if (body.data.submissionId !== "cb6edcef-a31c-4ee1-b3a9-4a35e90ad352") {
    continue;
  }

  const outputFilename = "./output.json";
  fs.writeFile(outputFilename, JSON.stringify(body, null, 4), (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("JSON saved to " + outputFilename);
    }
  });
}
```

## Add data to the `replayEvent.js` script

Create a new file in the `phenom-review` root folder based on this example:

```
const AWS = require('aws-sdk')
const { SqsPublishConsumer } = require('@hindawi/eve')

const sqs = new AWS.SQS({
  secretAccessKey: 'your-secret-access-key',
  accessKeyId: 'your-key-id',
  endpoint: 'your-endpoint',
  region: 'your-region',
})
const sqsPublisher = new SqsPublishConsumer(sqs, 'your-queue')
async function main() {
  const event = {
    event: 'pen:Hindawi:hindawi-screening:EventName',
    timestamp: '2020-08-13T08:44:46.010Z',
    data: {
      ...
    },
  }
  try {
    await sqsPublisher.consume({ Message: JSON.stringify(event) })
  } catch (err) {
    console.log(err)
  }
}
main()
```

After you update your SQS credentials, the correct queue, the event name and the timestamp, simply take the JSON data obtained in the previous step and add it in the `event` object.

After that, run the script using `node scriptName.js` and you're done.
