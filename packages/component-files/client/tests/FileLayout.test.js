import React from 'react'
import { cleanup, fireEvent, render } from '@testing-library/react'

import { FileLayout } from '../components'

const file = {
  id: '123454312',
  size: 12341,
  mimeType: 'application/pdf',
  originalName: 'sample.pdf',
}

describe('File', () => {
  afterEach(cleanup)

  it('renders the item with preview', () => {
    const onPreviewClickMock = jest.fn()
    const onPreviewMock = jest.fn(() => onPreviewClickMock)

    const { getAllByTestId } = render(
      <FileLayout item={file} onPreview={onPreviewMock} />,
    )

    expect(onPreviewMock).toHaveBeenCalledTimes(1)
    expect(onPreviewMock).toHaveBeenCalledWith(file)

    fireEvent.click(getAllByTestId(/-preview/i)[0])
    expect(onPreviewClickMock).toHaveBeenCalledTimes(1)
  })

  it('can delete the item', () => {
    const onDeleteMock = jest.fn()

    const { getByTestId } = render(
      <FileLayout hasDelete item={file} onDelete={onDeleteMock} />,
    )

    fireEvent.click(getByTestId(/-delete/i))
    expect(onDeleteMock).toHaveBeenCalledTimes(1)
  })
})
