const { pick } = require('lodash')

const decomposeFilename = (fileName = '') => {
  const ext = fileName.split('.').reverse()[0]
  const name = fileName.replace(`.${ext}`, '')
  return [name, ext]
}

const getFileNumber = file => {
  const [name] = decomposeFilename(file.fileName)
  const [originalName] = decomposeFilename(file.originalName)
  return (
    Number(
      name
        .replace(originalName, '')
        .trim()
        .replace(/[()]/g, ''),
    ) + 1
  )
}

const MIMETYPES = {
  pdf: 'application/pdf',
  doc: 'application/msword',
  docx:
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  txt: 'text/plain',
  rdf: 'application/rdf+xml',
  odt: 'application/vnd.oasis.opendocument.text',
  png: 'image/png',
  jpg: 'image/jpeg',
  jpeg: 'image/jpeg',
}

const ALL_MIMETYPES = Object.values(MIMETYPES)

const ALLOWED_FILE_TYPES = {
  manuscript: Object.values(
    pick(MIMETYPES, ['pdf', 'doc', 'docx', 'txt', 'rdf', 'odt']),
  ),
  supplementary: ALL_MIMETYPES,
  coverLetter: ALL_MIMETYPES,
  figure: ALL_MIMETYPES,
  reviewComment: ALL_MIMETYPES,
  responseToReviewers: ALL_MIMETYPES,
}

const isMimetypeValid = ({ mimetype, type, File }) => {
  if (type === File.Types.figure) return true
  return ALLOWED_FILE_TYPES[type].includes(mimetype)
}

module.exports = {
  getFileNumber,
  isMimetypeValid,
  decomposeFilename,
}
