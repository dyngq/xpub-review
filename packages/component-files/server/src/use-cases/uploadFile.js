const initialize = ({ logEvent, s3Service, models, useCases }) => {
  const { File } = models
  const manuscriptTypes = [
    File.Types.manuscript,
    File.Types.supplementary,
    File.Types.coverLetter,
    File.Types.figure,
  ]
  const commentTypes = [
    File.Types.reviewComment,
    File.Types.responseToReviewers,
  ]
  return {
    execute: async ({
      params: { entityId, fileInput, file: fileData },
      userId,
    }) => {
      const isCommentFile = commentTypes.includes(fileInput.type)
      const isManuscriptFile = manuscriptTypes.includes(fileInput.type)

      let useCase
      if (isManuscriptFile) useCase = 'uploadManuscriptFile'
      else if (isCommentFile) useCase = 'uploadCommentFile'
      else throw new Error('File must have a valid type.')

      return useCases[useCase]
        .initialize({ models, s3Service, logEvent })
        .execute({
          entityId,
          fileInput,
          fileData,
          userId,
        })
    },
  }
}

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
