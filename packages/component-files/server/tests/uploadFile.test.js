const { uploadManuscriptFile } = require('../src/use-cases')
const { generateFileTypes } = require('component-generators')

const models = {
  File: {
    Types: generateFileTypes(),
  },
}

const { File } = models

describe('uploadManuscriptFile use-case', () => {
  it('should throw error if the filetype has .rar extension', async () => {
    const res = uploadManuscriptFile.initialize({ models }).execute({
      fileData: { mimetype: 'application/vnd.rar' },
      fileInput: { type: File.Types.manuscript },
    })

    await expect(res).rejects.toThrowError(
      'Please upload one of the supported file types.',
    )
  })
  it('should throw error if the filetype has .zip extension', async () => {
    const res = uploadManuscriptFile.initialize({ models }).execute({
      fileData: { mimetype: 'application/zip' },
      fileInput: { type: File.Types.manuscript },
    })

    await expect(res).rejects.toThrowError(
      'Please upload one of the supported file types.',
    )
  })
  it('should throw error if the filetype has .7z extension', async () => {
    const res = uploadManuscriptFile.initialize({ models }).execute({
      fileData: { mimetype: 'application/x-7z-compressed' },
      fileInput: { type: File.Types.manuscript },
    })

    await expect(res).rejects.toThrowError(
      'Please upload one of the supported file types.',
    )
  })
})
