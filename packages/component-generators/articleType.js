const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateArticleType(props) {
    return {
      id: chance.guid(),
      name: chance.name(),
      hasPeerReview: true,
      save: jest.fn(),
      updateProperties: jest.fn(),
      ...props,
    }
  },
}
