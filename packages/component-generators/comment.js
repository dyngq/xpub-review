const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateComment(props) {
    return {
      id: chance.guid(),
      content: chance.paragraph(),
      ...props,
    }
  },
  getCommentTypes() {
    return types
  },
}

const types = {
  public: 'public',
  private: 'private',
}
