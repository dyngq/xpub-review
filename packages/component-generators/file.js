module.exports = {
  generateFileTypes() {
    return types
  },
}

const types = {
  figure: 'figure',
  manuscript: 'manuscript',
  coverLetter: 'coverLetter',
  supplementary: 'supplementary',
  reviewComment: 'reviewComment',
  responseToReviewers: 'responseToReviewers',
}
