const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateIdentity(props) {
    return {
      id: chance.guid(),
      surname: chance.name(),
      givenNames: chance.name(),
      email: chance.email(),
      userId: chance.guid(),
      save: jest.fn(),
      updateProperties: jest.fn(),
      ...props,
    }
  },
}
