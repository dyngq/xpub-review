const { generateJob } = require('./job')
const { generateUser } = require('./user')
const { generateFileTypes } = require('./file')
const { generateSection } = require('./section')
const { generateJournal } = require('./journal')
const { generateIdentity } = require('./identity')
const { generateAuditLog } = require('./auditLog')
const { generateArticleType } = require('./articleType')
const { generateTeam, getTeamRoles } = require('./team')
const { generateSpecialIssue } = require('./specialIssue')
const { generatePeerReviewModel } = require('./peerReviewModel')
const { generateReview, getRecommendations } = require('./review')
const { generateComment, getCommentTypes } = require('./comment')
const { generateReviewerSuggestion } = require('./reviewerSuggestion')
const { generateTeamMember, getTeamMemberStatuses } = require('./teamMember')
const {
  generateManuscript,
  getManuscriptStatuses,
  getManuscriptInProgressStatuses,
  getManuscriptNonActionableStatuses,
} = require('./manuscript')

module.exports = {
  generateJob,
  generateUser,
  getTeamRoles,
  generateTeam,
  generateReview,
  generateJournal,
  generateSection,
  generateComment,
  getCommentTypes,
  generateIdentity,
  generateAuditLog,
  generateFileTypes,
  getRecommendations,
  generateTeamMember,
  generateManuscript,
  generateArticleType,
  generateSpecialIssue,
  getManuscriptStatuses,
  getTeamMemberStatuses,
  generatePeerReviewModel,
  generateReviewerSuggestion,
  getManuscriptInProgressStatuses,
  getManuscriptNonActionableStatuses,
}
