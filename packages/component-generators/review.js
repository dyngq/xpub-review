const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateReview(props) {
    return {
      id: chance.guid(),
      isValid: true,
      ...props,
    }
  },
  getRecommendations() {
    return recommendations
  },
}

const recommendations = {
  responseToRevision: 'responseToRevision',
  minor: 'minor',
  major: 'major',
  reject: 'reject',
  publish: 'publish',
  revision: 'revision',
  returnToAcademicEditor: 'returnToAcademicEditor',
}
