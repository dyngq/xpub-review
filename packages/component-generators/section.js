const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateSection(props) {
    return {
      id: chance.guid(),
      journalId: chance.guid(),
      name: chance.name(),
      save: jest.fn(),
      updateProperties: jest.fn(),
      ...props,
    }
  },
}
