const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateSpecialIssue(props) {
    return {
      journalId: chance.guid(),
      name: chance.name(),
      start_date: chance.date(),
      end_date: chance.date(),
      peer_review_model_id: chance.guid(),
      save: jest.fn(),
      updateProperties: jest.fn(),
      ...props,
    }
  },
}
