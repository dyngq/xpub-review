const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateTeamMember(props) {
    return {
      id: chance.guid(),
      userId: chance.guid(),
      status: statuses.pending,
      updateProperties: jest.fn(),
      save: jest.fn(),
      alias: {
        email: chance.email(),
      },
      user: {
        id: chance.guid(),
      },
      delete: jest.fn(),
      ...props,
    }
  },
  getTeamMemberStatuses() {
    return statuses
  },
}

const statuses = {
  pending: 'pending',
  accepted: 'accepted',
  declined: 'declined',
  submitted: 'submitted',
  expired: 'expired',
  removed: 'removed',
  active: 'active',
}
