import styled from 'styled-components'
import { space } from 'styled-system'
import { th } from '@pubsweet/ui-toolkit'

const Box = styled.div`
  background-color: ${th('box.backgroundColor')};
  border: ${th('box.border')};
  border-radius: ${th('box.borderRadius')};

  ${space};
`

export default Box
