import { space } from 'styled-system'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const Bullet = styled.span.attrs(props => ({
  children: '\u2022',
}))`
  color: ${th('textSecondaryColor')};
  font-size: ${th('secondaryTextSize')};

  ${space};
`
export default Bullet
