import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

import { CopyText, Tooltip } from '@hindawi/ui'

const CopyEmail = ({ children, email }) => {
  if (!email) return <Fragment>{children}</Fragment>

  const tippyOptions = {
    placement: 'top-start',
    interactive: true,
    interactiveDebounce: 300,
  }

  return (
    <div>
      <Tooltip
        content={<CopyText textAfterCopy="Email copied!">{email}</CopyText>}
        tippyOptions={tippyOptions}
      >
        {children}
      </Tooltip>
    </div>
  )
}

CopyEmail.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element,
    PropTypes.string,
  ]).isRequired,
  email: PropTypes.string,
}

CopyEmail.defaultProps = {
  email: undefined,
}

export default CopyEmail
