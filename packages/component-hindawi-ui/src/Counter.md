A simple counter.

```js
<Counter number={-1} />
<Counter number={0} />
<Counter number={1} />
<Counter number={2} />
<Counter number={10} />
<Counter number={999999} />
```
