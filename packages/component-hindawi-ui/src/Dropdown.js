import React from 'react'
import styled from 'styled-components'
import { compose, withStateHandlers } from 'recompose'
import { th } from '@pubsweet/ui-toolkit'
import { Icon } from '..'

const Dropdown = ({ toggleDropdown, expanded, children }) => (
  <Root>
    <Icon fontSize="16px" icon="moreDefault" onClick={toggleDropdown} />
    {expanded && (
      <DropdownOptionsWrapper data-test-id="user-panel-dropdown">
        {children}
      </DropdownOptionsWrapper>
    )}
    {expanded && <ToggleOverlay onClick={toggleDropdown} />}
  </Root>
)

const Root = styled.div`
  position: relative;
`

const ToggleOverlay = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  opacity: 0;
`

export const DropdownOption = styled.div.attrs(props => ({
  'data-test-id': props['data-test-id'] || 'dropdown-option',
}))`
  align-items: center;
  color: ${th('colorText')};
  cursor: pointer;
  display: flex;
  justify-content: flex-start;

  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBase')};
  line-height: ${th('lineHeightBase')};

  height: calc(${th('gridUnit')} * 8);
  padding: calc(${th('gridUnit')} * 2);

  &:hover {
    background-color: ${th('menu.optionBackground')};
  }
`

const DropdownOptionsWrapper = styled.div.attrs(props => ({
  'data-test-id': props['data-test-id'] || 'admin-dropdown',
}))`
  background-color: ${th('appBar.colorBackground')};
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};

  position: absolute;
  right: 0px;
  width: calc(${th('gridUnit')} * 36);
  z-index: 10;
`

Dropdown.DropdownOption = DropdownOption

export default compose(
  withStateHandlers(
    { expanded: false },
    { toggleDropdown: ({ expanded }) => () => ({ expanded: !expanded }) },
  ),
)(Dropdown)
