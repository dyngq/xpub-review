import React from 'react'
import { space } from 'styled-system'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { IconButton, Label } from '../'

const IconCard = ({ label, icon, iconSize, onClick, ...rest }) => (
  <Root onClick={onClick} {...rest}>
    <IconButton icon={icon} iconSize={iconSize} />
    <Label>{label}</Label>
  </Root>
)

export default IconCard

// #region styles
const Root = styled.div`
  align-items: center;
  background-color: ${th('colorBackgroundHue')};
  box-shadow: ${th('boxShadow')};
  border-radius: ${th('borderRadius')};
  cursor: pointer;
  display: flex;
  flex-direction: column;
  justify-content: center;

  height: calc(${th('gridUnit')} * 38);
  width: calc(${th('gridUnit')} * 52);
  svg {
    stroke: ${th('actionPrimaryColor')};
  }

  ${space};
`
// #endregion
