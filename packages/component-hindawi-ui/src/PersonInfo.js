import React from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { compose, withProps } from 'recompose'

import { Text, Row, Label, Item, withCountries } from '../'

const PersonInfo = ({ aff, country, email, givenNames, surname }) => (
  <Row>
    <WithEllipsis mr={1} vertical>
      <Label>Email</Label>
      <Text ellipsis title={email}>
        {email}
      </Text>
    </WithEllipsis>
    <Item mr={1} vertical>
      <Label>First name</Label>
      <Text title={givenNames}>{givenNames}</Text>
    </Item>
    <Item mr={1} vertical>
      <Label>Last name</Label>
      <Text>{surname}</Text>
    </Item>
    <WithEllipsis mr={1} vertical>
      <Label>Affiliation</Label>
      <Text ellipsis title={aff}>
        {aff}
      </Text>
    </WithEllipsis>
    <WithEllipsis vertical>
      <Label>Country</Label>
      <Text ellipsis>{country}</Text>
    </WithEllipsis>
  </Row>
)

PersonInfo.propTypes = {
  /** Person with information */
  person: PropTypes.shape({
    id: PropTypes.string,
    isCorresponding: PropTypes.bool,
    isSubmitting: PropTypes.bool,
    alias: PropTypes.shape({
      aff: PropTypes.string,
      email: PropTypes.string,
      country: PropTypes.string,
      name: PropTypes.shape({
        givenNames: PropTypes.string,
        surname: PropTypes.string,
        title: PropTypes.string,
      }),
    }),
  }).isRequired,
}

export default compose(
  withCountries,
  withProps(({ person, countryLabel }) => ({
    aff: get(person, 'alias.aff', ''),
    email: get(person, 'alias.email', ''),
    surname: get(person, 'alias.name.surname', ''),
    givenNames: get(person, 'alias.name.givenNames', ''),
    country: countryLabel(get(person, 'alias.country', '')),
  })),
)(PersonInfo)

// #region styles
const WithEllipsis = styled(Item)`
  display: inline-grid;
  flex-direction: column;
  flex: 1;
`
// #endregion
