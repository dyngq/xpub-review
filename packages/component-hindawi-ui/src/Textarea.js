import { get } from 'lodash'
import PropTypes from 'prop-types'
import { space } from 'styled-system'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

const textColor = ({ validationStatus }) =>
  validationStatus === 'error' ? th('colorError') : th('colorText')

const validatedBorder = ({ validationStatus }) =>
  validationStatus === 'error' ? th('colorError') : th('colorFurniture')

const activeBorder = ({ validationStatus }) =>
  validationStatus === 'error' ? th('colorError') : th('textField.activeBorder')

const minHeight = props =>
  css`
    min-height: calc(${th('gridUnit')} * ${get(props, 'minHeight', 4)});
  `

const Textarea = styled.textarea`
  border-radius: ${th('borderRadius')};
  border-color: ${validatedBorder};
  color: ${textColor};
  font-size: ${th('fontSizeBase')};
  font-family: ${th('defaultFont')};
  padding: calc(${th('gridUnit')} * 2);
  width: 100%;
  resize: vertical;
  border-style: ${th('borderStyle')};
  border-width: ${th('borderWidth')};

  ${minHeight};
  ${space};

  &:active,
  &:focus {
    border-color: ${activeBorder};
    outline: none;
  }

  &:read-only {
    background-color: ${th('colorBackgroundHue')};
  }
`
Textarea.propTypes = {
  /** The minimum height that the text box should have */
  minHeight: PropTypes.number,
}

Textarea.defaultProps = {
  minHeight: 20,
}

/** @component */
export default Textarea
