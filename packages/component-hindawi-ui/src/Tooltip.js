import React, { Fragment, useState } from 'react'
import PropTypes from 'prop-types'
import Tippy from '@tippy.js/react'
import styled, { ThemeProvider, withTheme, css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { Row } from '../'

const TooltipContent = ({ theme = {}, content }) => (
  <ThemeProvider theme={theme}>
    <Fragment>
      <Row>{content}</Row>
    </Fragment>
  </ThemeProvider>
)

const Tooltip = ({
  theme = {},
  overflow,
  children,
  content,
  tippyOptions = {},
}) => {
  const [mounted, setMounted] = useState(true)

  const _content = mounted ? (
    <TooltipContent content={content} theme={theme} />
  ) : (
    ''
  )

  return (
    <StyledTippy
      arrow
      content={_content}
      onHidden={_ => setMounted(false)}
      onShow={_ => setMounted(true)}
      {...tippyOptions}
    >
      <StyledText overflow={overflow}>{children}</StyledText>
    </StyledTippy>
  )
}

Tooltip.propTypes = {
  /** User can hover over an item, without clicking it, and a new window will appear with a new title */
  content: PropTypes.element,
}
Tooltip.defaultProps = {
  content:
    'β-Carboline Silver Compound Binding Studies with Human Serum Albumin: A Comprehensive Multispectroscopic Analysis and Molecular Modeling Study',
}

const overflowHelper = props => {
  if (props.overflow) {
    return css`
      overflow: ${props.overflow};
    `
  }
}

const StyledText = styled.span`
  ${overflowHelper}
`

// Tooltip component should have some general styles defined below
const StyledTippy = styled(Tippy)`
  background: ${th('grey70')};
  background-color: ${th('grey70')};

  > .tippy-content {
    padding: 3px 7px;
    font-size: 13px;
    line-height: 18px;
    font-weight: 600;
  }

  &[data-placement^='top'] > .tippy-arrow {
    border-top-color: ${th('grey70')};
  }
`

export default withTheme(Tooltip)
