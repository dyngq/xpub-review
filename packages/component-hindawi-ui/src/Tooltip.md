Tooltip

Simple Tooltip

```js
const content = 'β-Carboline Silver Compound Binding Studies with Human Serum Albumin: A Comprehensive Multispectroscopic Analysis and Molecular Modeling Study';

<Tooltip
  content={content}
>
  hover me
</Tooltip>
```

Tooltip with content that is not text

```js
import CopyText from './CopyText';

const tippyOptions = {
  placement: 'top-start',
  interactive: true,
  interactiveDebounce: 300,
};

<Tooltip
  content={
    <CopyText textAfterCopy="Email copied!">email@superadmin.ro</CopyText>
  }
  tippyOptions={tippyOptions}
>
  Dwight Schrute
</Tooltip>
```

Tooltip with tippyOptions

```js
const content = 'β-Carboline Silver Compound Binding Studies with Human Serum Albumin: A Comprehensive Multispectroscopic Analysis and Molecular Modeling Study';

const tippyOptions = {
  placement: 'top-start',
  interactive: true,
  interactiveDebounce: 300,
};

<Tooltip
  content={content}
  tippyOptions={tippyOptions}
>
  hover me
</Tooltip>
```