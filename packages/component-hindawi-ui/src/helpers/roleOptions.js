export default [
  { label: 'user', value: 'User' },
  { label: 'admin', value: 'Admin' },
  { label: 'triageEditor', value: 'Chief Editor' },
  { label: 'academicEditor', value: 'Academic Editor' },
  { label: 'editorialAssistant', value: 'Editorial Assistant' },
]
