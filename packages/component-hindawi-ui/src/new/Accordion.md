A simple accordion.

```js
<Accordion>
  <Accordion.Item title="Abstract">long text</Accordion.Item>
  <Accordion.Item title="Author Declaration">some declarations</Accordion.Item>
  <Accordion.Item title="Files" number={8}>
    many files
  </Accordion.Item>
</Accordion>
```
