const NodeEnvironment = require('jest-environment-node')
const pg = require('pg')
const logger = require('@pubsweet/logger')
const config = require('config')
const knex = require('knex')
const { knexSnakeCaseMappers } = require('objection')

const dbConfig = config['pubsweet-server'] && config['pubsweet-server'].db
let client

const dbName = `test_${Math.floor(Math.random() * 9999999)}`
const pool = config['pubsweet-server'] && config['pubsweet-server'].pool
logger.info(`${dbName} has been created.`)

const db = knex({
  client: 'pg',
  connection: {
    database: dbName,
  },
  pool,
  ...knexSnakeCaseMappers(),
  acquireConnectionTimeout: 5000,
  asyncStackTraces: true,
})

class DatabaseTestEnvironment extends NodeEnvironment {
  async setup() {
    client = await new pg.Client(dbConfig)
    try {
      await client.connect()
    } catch (e) {
      logger.warn(e)

      return
    }

    this.global.__testDbName = dbName
    await client.query(`CREATE DATABASE ${dbName}`)

    await db.raw('DROP SCHEMA public CASCADE;')
    await db.raw('CREATE SCHEMA public;')
    await db.raw('GRANT ALL ON SCHEMA public TO public;')
    await db.migrate.latest()
    logger.info('Dropped all tables and ran all migrations')

    await db.seed.run({
      directory: '../app/seeds',
    })
    await db.seed.run({
      directory: '../app/testSeeds',
    })
    await super.setup()
  }

  async teardown() {
    db.destroy()
    // terminate other connections from test before dropping db
    await client.query(`REVOKE CONNECT ON DATABASE ${dbName} FROM public`)
    await client.query(`
      SELECT pg_terminate_backend(pg_stat_activity.pid)
      FROM pg_stat_activity
      WHERE pg_stat_activity.datname = '${dbName}'`)
    await client.query(`DROP DATABASE ${dbName}`)
    logger.info(`${dbName} has been dropped.`)
    await client.end()
    await super.teardown()
  }
}

module.exports = DatabaseTestEnvironment
