exports.up = async knex => {
  await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"')
  await knex.schema.createTable('user', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table.string('default_identity_type').notNullable()
    table.boolean('is_subscribed_to_emails').notNullable()
    table.string('invitation_token')
    table.string('confirmation_token').defaultTo(knex.raw('uuid_generate_v4()'))
    table.string('password_reset_token')
    table.string('unsubscribe_token').defaultTo(knex.raw('uuid_generate_v4()'))
    table.boolean('is_active').notNullable()
    table.boolean('agree_tc').notNullable()
    table.timestamp('password_reset_timestamp')
  })

  await knex.schema.createTable('identity', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table.uuid('user_id').notNullable()
    table.string('type').notNullable()
    table.string('identifier')
    table.string('surname')
    table.string('given_names')
    table.string('title')
    table.string('aff')
    table.string('country')
    table.boolean('is_confirmed').notNullable()
    table.string('password_hash')
    table.string('email')

    table.foreign('user_id').references('user.id')
  })

  await knex.schema.createTable('peer_review_model', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .string('name')
      .unique()
      .notNullable()
    table.specificType('approval_editors', 'text ARRAY')
    table.boolean('has_figurehead_editor').notNullable()
    table.string('figurehead_editor_label')
    table.boolean('has_triage_editor').notNullable()
    table.string('triage_editor_label')
    table.specificType('triage_editor_assignment_tool', 'text ARRAY')
    table.string('academic_editor_label')
    table.specificType('reviewer_assignment_tool', 'text ARRAY')
    table.boolean('has_sections').defaultTo(false)
    table.boolean('academic_editor_automatic_invitation')
  })

  await knex.schema.createTable('journal', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .string('name')
      .unique()
      .notNullable()
    table.string('publisher_name')
    table.string('code')
    table.string('email')
    table.string('issn')
    table.integer('apc')
    table.boolean('is_active').defaultTo(false)
    table.timestamp('activation_date')
    table.uuid('peer_review_model_id').notNullable()

    table.foreign('peer_review_model_id').references('peer_review_model.id')
  })

  await knex.schema.createTable('article_type', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .string('name')
      .unique()
      .notNullable()
    table.boolean('has_peer_review').defaultTo(false)
  })

  await knex.schema.createTable('journal_article_type', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table.uuid('journal_id').notNullable()
    table.uuid('article_type_id').notNullable()

    table.foreign('journal_id').references('journal.id')
    table.foreign('article_type_id').references('article_type.id')
  })

  await knex.schema.createTable('section', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .string('name')
      .unique()
      .notNullable()
    table.uuid('journal_id').notNullable()

    table.foreign('journal_id').references('journal.id')
  })

  await knex.schema.createTable('special_issue', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .string('name')
      .unique()
      .notNullable()
    table.uuid('journal_id')
    table.uuid('section_id')
    table.timestamp('start_date').notNullable()
    table.timestamp('end_date').notNullable()
    table.boolean('is_active').defaultTo(false)
    table.string('call_for_papers').notNullable()
    table.boolean('is_cancelled').defaultTo(false)
    table.uuid('peer_review_model_id')
    table
      .string('custom_id')
      .defaultTo(
        knex.raw(
          "lpad(((floor((random() * (999999)::double precision)))::character varying)::text, 6, '0'::text)",
        ),
      )
    table.string('cancel_reason')

    table.foreign('journal_id').references('journal.id')
    table.foreign('section_id').references('section.id')
    table.foreign('peer_review_model_id').references('peer_review_model.id')
  })

  await knex.schema.createTable('manuscript', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table.uuid('journal_id')
    table.string('status')
    table.text('title')
    table.text('abstract')
    table.string('custom_id')
    table.string('version')
    table.boolean('has_passed_eqs')
    table.boolean('has_passed_eqa')
    table.uuid('technical_check_token')
    table.uuid('submission_id').defaultTo(knex.raw('uuid_generate_v4()'))
    table.boolean('agree_tc')
    table.string('conflict_of_interest')
    table.string('data_availability')
    table.string('funding_statement')
    table.uuid('article_type_id')
    table.uuid('section_id')
    table.uuid('special_issue_id')
    table.boolean('is_post_acceptance').defaultTo(false)
    table.timestamp('submitted_date')
    table.timestamp('accepted_date')
    table.timestamp('quality_checks_submitted_date')
    table.boolean('allowAcademicEditorAutomaticInvitation').defaultTo(null)

    table.foreign('article_type_id').references('article_type.id')
    table.foreign('journal_id').references('journal.id')
    table.foreign('section_id').references('section.id')
    table.foreign('special_issue_id').references('special_issue.id')
  })

  await knex.schema.createTable('audit_log', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table.uuid('user_id')
    table.uuid('manuscript_id')
    table.string('action').notNullable()
    table.string('object_type').notNullable()
    table.uuid('object_id').notNullable()
    table.uuid('journal_id')

    table.foreign('user_id').references('user.id')
    table.foreign('manuscript_id').references('manuscript.id')
    table.foreign('journal_id').references('journal.id')
  })

  await knex.schema.createTable('team', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table.string('role').notNullable()
    table.uuid('manuscript_id')
    table.uuid('journal_id')
    table.uuid('section_id')
    table.uuid('special_issue_id')

    table.foreign('manuscript_id').references('manuscript.id')
    table.foreign('journal_id').references('journal.id')
    table.foreign('section_id').references('section.id')
    table.foreign('special_issue_id').references('special_issue.id')
  })

  await knex.schema.createTable('team_member', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table.uuid('user_id').notNullable()
    table.uuid('team_id').notNullable()
    table.integer('position')
    table.boolean('is_submitting')
    table.boolean('is_corresponding')
    table.string('status').notNullable()
    table.jsonb('alias')
    table.integer('reviewer_number')
    table.timestamp('responded')

    table.foreign('user_id').references('user.id')
    table.foreign('team_id').references('team.id')
  })

  await knex.schema.createTable('editor_suggestion', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table.uuid('manuscript_id').notNullable()
    table.uuid('team_member_id').notNullable()
    table.float('score').notNullable()

    table.foreign('manuscript_id').references('manuscript.id')
    table.foreign('team_member_id').references('team_member.id')
  })

  await knex.schema.createTable('job', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table.uuid('team_member_id').notNullable()
    table.uuid('manuscript_id')
    table.uuid('journal_id')
    table.uuid('special_issue_id')
    table.string('job_type')

    table.foreign('team_member_id').references('team_member.id')
    table.foreign('manuscript_id').references('manuscript.id')
    table.foreign('journal_id').references('journal.id')
    table.foreign('special_issue_id').references('special_issue.id')
  })

  await knex.schema.createTable('review', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table.timestamp('submitted')
    table.uuid('manuscript_id').notNullable()
    table.uuid('team_member_id').notNullable()
    table.string('recommendation')

    table.foreign('manuscript_id').references('manuscript.id')
    table.foreign('team_member_id').references('team_member.id')
  })

  await knex.schema.createTable('comment', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table.uuid('review_id').notNullable()
    table.string('type')
    table.string('content')

    table.foreign('review_id').references('review.id')
  })

  await knex.schema.createTable('file', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table.uuid('manuscript_id')
    table.uuid('comment_id')
    table.string('type').notNullable()
    table.string('label')
    table.string('file_name')
    table.string('url')
    table.string('mime_type')
    table.integer('size')
    table.string('original_name').notNullable()
    table.integer('position')
    table.string('provider_key')

    table.foreign('manuscript_id').references('manuscript.id')
    table.foreign('comment_id').references('comment.id')
  })

  await knex.schema.createTable('reviewer_suggestion', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table.uuid('manuscript_id').notNullable()
    table.string('given_names')
    table.string('surname')
    table.integer('number_of_reviews').notNullable()
    table.string('aff').notNullable()
    table.string('email').notNullable()
    table.string('profile_url').notNullable()
    table.boolean('is_invited').notNullable()
    table.string('type').notNullable()

    table.foreign('manuscript_id').references('manuscript.id')
  })

  await knex.schema.createTable('entities', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table.jsonb('data')
  })

  await knex.schema.createTable('migrations', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table.timestamp('run_at').defaultTo(knex.fn.now())
  })

  await knex.schema.createTable('role', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .string('name')
      .unique()
      .notNullable()
  })

  await knex.schema.createTable('submission_status', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .string('name')
      .unique()
      .notNullable()
    table.string('category').notNullable()
  })

  await knex.schema.createTable('submission_filter', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .string('name')
      .unique()
      .notNullable()
  })

  await knex.schema.createTable('submission_label', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())
    table.uuid('role_id').notNullable()
    table.uuid('submission_status_id').notNullable()
    table.uuid('submission_filter_id')
    table.integer('priority').defaultTo(0)
    table.string('name').notNullable()

    table.foreign('role_id').references('role.id')
    table.foreign('submission_status_id').references('submission_status.id')
    table.foreign('submission_filter_id').references('submission_filter.id')
  })
}
exports.down = async knex => knex.schema.dropTable('role')
