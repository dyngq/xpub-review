const models = require('@pubsweet/models')

const { groupBy } = require('lodash')
const { db } = require('@pubsweet/db-manager')

const {
  getManuscriptEditorsLabelsUseCase,
} = require('../../component-model/src/useCases')

describe('get manuscript editors labels', () => {
  const { Manuscript } = models
  let manuscript = {}
  beforeAll(async () => {
    const manuscripts = await Manuscript.all()
    const groupedSubmissions = groupBy(manuscripts, 'submissionId')
    const submissionWithManyVersions = Object.values(groupedSubmissions).find(
      submission => submission.length > 1,
    )
    ;[manuscript] = submissionWithManyVersions
  })
  afterAll(done => {
    db.destroy()
    done()
  })

  it('returns Academic Editor and Chief Editor when the PRM is Chief Minus', async () => {
    const editorsLabels = await getManuscriptEditorsLabelsUseCase
      .initialize(models)
      .execute({ manuscript })

    expect(editorsLabels).toBeInstanceOf(Object)
    expect(editorsLabels).toHaveProperty('academicEditorLabel')
    expect(editorsLabels).toHaveProperty('triageEditorLabel')

    expect(editorsLabels.academicEditorLabel).toEqual('Academic Editor')
    expect(editorsLabels.triageEditorLabel).toEqual('Chief Editor')
  })
  it('returns Academic Editor and Chief Editor when the manuscript is on a Special Issue but there is a conflict of interest', async () => {
    const manuscript = await Manuscript.findOneBy({
      queryObject: { title: 'COI' },
    })
    const editorsLabels = await getManuscriptEditorsLabelsUseCase
      .initialize(models)
      .execute({ manuscript })

    expect(editorsLabels.academicEditorLabel).toEqual('Academic Editor')
    expect(editorsLabels.triageEditorLabel).toEqual('Chief Editor')
  })
})
