module.exports.initialize = ({
  models: { Job, Manuscript, Team, TeamMember },
  logger,
  logEvent,
  eventsService,
}) => ({
  async handle(job) {
    try {
      const { invitationId, manuscriptId, action } = job.data
      if (!invitationId) {
        logger.warn(
          `Invitation id is undefined when trying to cancel academic editor on manuscript ${manuscriptId}`,
        )
        return
      }

      const teamMember = await TeamMember.find(invitationId)
      if (!teamMember) {
        logger.warn(`No team member has been found for id ${invitationId}`)
        return
      }
      teamMember.updateProperties({ status: TeamMember.Statuses.expired })
      await teamMember.save()

      const manuscript = await Manuscript.find(manuscriptId)

      const activeAcademicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
        {
          role: Team.Role.academicEditor,
          status: TeamMember.Statuses.accepted,
          manuscriptId: manuscript.id,
        },
      )
      if (!activeAcademicEditor) {
        manuscript.updateProperties({ status: Manuscript.Statuses.submitted })
        await manuscript.save()
      }

      logEvent({
        userId: null,
        manuscriptId,
        action,
        objectType: logEvent.objectType.user,
        objectId: teamMember.userId,
      })

      logger.info(`Successfully expired academic editor ${invitationId}`)

      eventsService.publishSubmissionEvent({
        submissionId: manuscript.submissionId,
        eventName: 'SubmissionAcademicEditorInvitationExpired',
      })

      return job.done()
    } catch (e) {
      return job.done(e)
    }
  },
  async subscribe() {
    Job.subscribe({
      queueName: 'academic-editor-removal',
      jobHandler: this.handle,
    })
  },
})
