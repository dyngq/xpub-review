const { get } = require('lodash')

module.exports.initialize = ({
  baseUrl,
  services,
  footerText,
  unsubscribeSlug,
  getModifiedText,
}) => ({
  getProps({ emailProps, user }) {
    emailProps.toUser = {
      email: get(user, 'alias.email', ''),
      name: get(user, 'alias.surname', ''),
    }
    emailProps.unsubscribeLink = services.createUrl(baseUrl, unsubscribeSlug, {
      id: user.userId,
      token: get(user, 'user.unsubscribeToken'),
    })
    emailProps.content.footerText = getModifiedText(footerText, {
      pattern: '{recipientEmail}',
      replacement: get(user, 'alias.email', ''),
    })
    return emailProps
  },
})
