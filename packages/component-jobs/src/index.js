const config = require('config')
const models = require('@pubsweet/models')
const events = require('component-events')
const logger = require('@pubsweet/logger')
const { services } = require('helper-service')
const Email = require('@pubsweet/component-email-templating')
const { getModifiedText } = require('component-transform-text')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')
const { logEvent } = require('component-activity-log/server')
const getProps = require('./emailPropsService')
const existingEmailJobs = require('./existingEmailJobs')
const existingRemovalJobs = require('./existingRemovalJobs')

const reviewerEmailJobs = require('./reviewerEmailJobs')
const academicEditorEmailJobs = require('./academicEditorEmailJobs')
const triageEditorEmailJobs = require('./triageEditorEmailJobs')
const editorialAssistantEmailJobs = require('./editorialAssistantEmailJobs')

const academicEditorRemovalJobs = require('./academicEditorRemovalJobs')
const acceptedReviewerRemovalJobs = require('./acceptedReviewerRemovalJobs')
const pendingReviewerRemovalJobs = require('./pendingReviewerRemovalJobs')

const journalActivationJobs = require('./journalActivationJobs')
const specialIssueActivationJobs = require('./specialIssueActivationJobs')
const specialIssueDeactivationJobs = require('./specialIssueDeactivationJobs')

module.exports = () => () => {
  const eventsService = events.initialize({ models })

  // Email Subscriptions
  existingEmailJobs
    .initialize({
      Email,
      models,
      logger,
      logEvent,
      eventsService,
    })
    .subscribe()

  existingRemovalJobs
    .initialize({
      models,
      logger,
      logEvent,
      eventsService,
    })
    .subscribe()

  academicEditorEmailJobs
    .initialize({
      models,
      Email,
      logEvent,
    })
    .subscribe()

  reviewerEmailJobs
    .initialize({
      models,
      Email,
      logEvent,
    })
    .subscribe()

  triageEditorEmailJobs
    .initialize({
      models,
      Email,
      logEvent,
    })
    .subscribe()

  editorialAssistantEmailJobs
    .initialize({
      models,
      Email,
      logEvent,
    })
    .subscribe()

  // Removal Subscriptions

  academicEditorRemovalJobs
    .initialize({ models, eventsService, logEvent, logger })
    .subscribe()

  acceptedReviewerRemovalJobs
    .initialize({ models, logger, logEvent, eventsService })
    .subscribe()

  pendingReviewerRemovalJobs
    .initialize({ models, eventsService, logger, logEvent })
    .subscribe()

  journalActivationJobs
    .initialize({
      models,
      eventsService,
    })
    .subscribe()

  specialIssueActivationJobs
    .initialize({ models, eventsService, logger })
    .subscribe()

  const getPropsService = getProps.initialize({
    baseUrl,
    services,
    footerText,
    unsubscribeSlug,
    getModifiedText,
  })
  specialIssueDeactivationJobs
    .initialize({
      Email,
      models,
      logger,
      getPropsService,
      eventsService,
    })
    .subscribe()
}
