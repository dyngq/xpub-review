module.exports.initialize = ({
  models: { Job, SpecialIssue, Section },
  logger,
  eventsService,
}) => ({
  async handle(job) {
    try {
      const { specialIssueId } = job.data
      const specialIssue = await SpecialIssue.find(specialIssueId)

      if (specialIssue.isActive) return

      specialIssue.updateProperties({ isActive: true })
      await specialIssue.save()

      logger.info(`${specialIssueId} has been updated`)

      const { sectionId } = specialIssue
      let { journalId } = specialIssue
      let eventName = 'JournalSpecialIssueOpened'

      if (sectionId) {
        ;({ journalId } = await Section.find(sectionId))
        eventName = 'JournalSectionSpecialIssueOpened'
      }

      eventsService.publishJournalEvent({
        journalId,
        eventName,
      })

      return job.done()
    } catch (e) {
      return job.done(e)
    }
  },
  async subscribe() {
    Job.subscribe({
      queueName: 'special-issue-activation*',
      jobHandler: this.handle,
    })
  },
})
