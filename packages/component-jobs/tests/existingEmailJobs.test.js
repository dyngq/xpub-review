const Chance = require('chance')

const chance = new Chance()
const {
  generateJob,
  getTeamRoles,
  generateTeamMember,
  generateManuscript,
  getTeamMemberStatuses,
  getManuscriptStatuses,
  getManuscriptInProgressStatuses,
} = require('component-generators')

const { initialize } = require('../src/existingEmailJobs')

const models = {
  Job: {
    // subscribe: jest.fn(),
  },
  Manuscript: {
    find: jest.fn(),
    Statuses: getManuscriptStatuses(),
    InProgressStatuses: getManuscriptInProgressStatuses(),
  },
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: {
    find: jest.fn(),
    Statuses: getTeamMemberStatuses(),
    findOneByManuscriptAndUser: jest.fn(),
  },
}

const logEvent = jest.fn()

const logger = {
  error: jest.fn(),
  warn: jest.fn(),
  info: jest.fn(),
}
const Email = jest.fn().mockImplementation(() => ({
  async sendEmail() {
    return jest.fn()
  },
}))
const { Manuscript, Team, TeamMember } = models

describe('existing email jobs', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('has the handler exposed', () => {
    const initializer = initialize({ models, Email, logger, logEvent })
    const handler = 'handle'
    expect(initializer).toHaveProperty(handler)
    expect(typeof initializer[handler]).toBe('function')
  })

  it('returns an error when the manuscript id is not found', async () => {
    const manuscript = generateManuscript()
    const job = generateJob({ manuscriptId: manuscript.id })
    jest.spyOn(Manuscript, 'find').mockResolvedValue(undefined)

    await initialize({
      models,
      Email,
      logger,
      logEvent,
    }).handle(job)

    expect(logger.error).toHaveBeenCalledWith(
      `No manuscript found with id ${manuscript.id}`,
    )
  })

  it('skips manuscripts that are no longer in progress', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.accepted,
    })
    const job = generateJob({ manuscriptId: manuscript.id })
    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)

    await initialize({
      models,
      Email,
      logger,
      logEvent,
    }).handle(job)

    expect(logger.warn).toHaveBeenCalledWith(
      `Skipping emails for manuscript ${manuscript.id} with status accepted`,
    )
  })

  it('skips manuscripts that are close to a decision', async () => {
    const status = chance.pickone([
      Manuscript.Statuses.makeDecision,
      Manuscript.Statuses.reviewCompleted,
      Manuscript.Statuses.pendingApproval,
      Manuscript.Statuses.revisionRequested,
    ])
    const manuscript = generateManuscript({
      status,
    })
    const job = generateJob({ manuscriptId: manuscript.id })
    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)

    await initialize({
      models,
      Email,
      logger,
      logEvent,
    }).handle(job)

    expect(logger.info).toHaveBeenCalledWith(
      `Skipping emails for manuscript ${manuscript.id} with status ${status}`,
    )
  })

  it('returns an error when the invitation id finds no team member', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.reviewersInvited,
    })
    const teamMember = generateTeamMember()
    const job = generateJob({
      manuscriptId: manuscript.id,
      invitationId: teamMember.id,
    })
    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'find').mockResolvedValue(undefined)

    await initialize({
      models,
      Email,
      logger,
      logEvent,
    }).handle(job)

    expect(logger.error).toHaveBeenCalledWith(
      `No team member found with id ${teamMember.id}`,
    )
  })

  it('returns an error when the user id finds no team member', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.reviewersInvited,
    })
    const teamMember = generateTeamMember()
    const job = generateJob({
      manuscriptId: manuscript.id,
      userId: teamMember.userId,
      teamMemberId: undefined,
      invitationId: undefined,
    })
    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndUser')
      .mockResolvedValue(undefined)

    await initialize({
      models,
      Email,
      logger,
      logEvent,
    }).handle(job)

    expect(logger.error).toHaveBeenCalledWith(
      `No team member found with userId ${teamMember.userId}`,
    )
  })

  it('returns an error when the team member id finds no team member', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.reviewersInvited,
    })
    const teamMember = generateTeamMember()

    const job = generateJob({
      manuscriptId: manuscript.id,
      teamMemberId: teamMember.id,
      invitationId: undefined,
      userId: undefined,
    })
    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'find').mockResolvedValue(undefined)

    await initialize({
      models,
      Email,
      logger,
      logEvent,
    }).handle(job)

    expect(logger.error).toHaveBeenCalledWith(
      `No team member found with id ${teamMember.id}`,
    )
  })

  it('returns an error when no team members can be found', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.reviewersInvited,
    })
    const job = generateJob({
      manuscriptId: manuscript.id,
      invitationId: undefined,
      teamMemberId: undefined,
      userId: undefined,
    })
    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'find').mockResolvedValue(undefined)

    await initialize({
      models,
      Email,
      logger,
      logEvent,
    }).handle(job)

    expect(logger.error).toHaveBeenCalledWith(
      `No team member found for manuscript id ${manuscript.id}`,
    )
  })

  it('skips other roles than reviewer and academic editor', async () => {
    const role = chance.pickone([
      Team.Role.admin,
      Team.Role.author,
      Team.Role.triageEditor,
      Team.Role.editorialAssistant,
      Team.Role.researchIntegrityPublishingEditor,
    ])
    const teamMember = generateTeamMember({
      team: { role },
    })
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.submitted,
    })
    const job = generateJob({
      manuscriptId: manuscript.id,
      teamMemberId: teamMember.id,
    })

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'find').mockResolvedValue(teamMember)

    await initialize({
      models,
      Email,
      logger,
      logEvent,
    }).handle(job)

    expect(logger.warn).toHaveBeenCalledWith(
      `Skipping emails for manuscript ${manuscript.id} with status ${manuscript.status} and team member role ${teamMember.role}`,
    )
  })

  it('skips manuscripts with status submitted', async () => {
    const teamMember = generateTeamMember({
      team: { role: Team.Role.reviewer },
    })
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.submitted,
    })
    const job = generateJob({
      manuscriptId: manuscript.id,
      teamMemberId: teamMember.id,
    })
    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'find').mockResolvedValue(teamMember)

    await initialize({
      models,
      Email,
      logger,
      logEvent,
    }).handle(job)

    expect(logger.warn).toHaveBeenCalledWith(
      `Skipping emails for submitted manuscript ${manuscript.id} when no Academic Editor is invited`,
    )
  })

  it(`skips emails for academic editor that don't need to take action`, async () => {
    const manuscriptStatus = chance.pickone(
      Manuscript.InProgressStatuses.filter(
        status =>
          ![
            Manuscript.Statuses.makeDecision,
            Manuscript.Statuses.reviewCompleted,
            Manuscript.Statuses.pendingApproval,
            Manuscript.Statuses.revisionRequested,
            Manuscript.Statuses.submitted,
            Manuscript.Statuses.academicEditorAssigned,
            Manuscript.Statuses.reviewersInvited,
            Manuscript.Statuses.underReview,
          ].includes(status),
      ),
    )
    const teamMember = generateTeamMember({
      status: TeamMember.Statuses.accepted,
      team: { role: Team.Role.academicEditor },
    })
    const manuscript = generateManuscript({
      status: manuscriptStatus,
    })
    const job = generateJob({
      manuscriptId: manuscript.id,
      teamMemberId: teamMember.id,
    })

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'find').mockResolvedValue(teamMember)

    await initialize({
      models,
      Email,
      logger,
      logEvent,
    }).handle(job)

    expect(logger.warn).toHaveBeenCalledWith(
      `Skipping emails for accepted Academic Editors on manuscript ${manuscript.id} with status ${manuscript.status} because no action is needed`,
    )
  })

  it('skips emails for pending academic editors', async () => {
    const manuscriptStatus = chance.pickone(
      Manuscript.InProgressStatuses.filter(
        status =>
          ![
            Manuscript.Statuses.makeDecision,
            Manuscript.Statuses.reviewCompleted,
            Manuscript.Statuses.pendingApproval,
            Manuscript.Statuses.revisionRequested,
            Manuscript.Statuses.submitted,
            Manuscript.Statuses.academicEditorInvited,
          ].includes(status),
      ),
    )
    const teamMember = generateTeamMember({
      status: TeamMember.Statuses.pending,
      team: { role: Team.Role.academicEditor },
    })
    const manuscript = generateManuscript({
      status: manuscriptStatus,
    })
    const job = generateJob({
      manuscriptId: manuscript.id,
      teamMemberId: teamMember.id,
    })
    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'find').mockResolvedValue(teamMember)

    await initialize({
      models,
      Email,
      logger,
      logEvent,
    }).handle(job)

    expect(logger.warn).toHaveBeenCalledWith(
      `Skipping emails for pending Academic Editors on manuscript ${manuscript.id} with status ${manuscript.status} because no action is needed`,
    )
  })
})
