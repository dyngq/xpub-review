const { initialize } = require('../src/journalActivationJobs')
const { generateJob, generateJournal } = require('component-generators')

const models = {
  Job: {
    subscribe: jest.fn(),
  },
  Journal: {
    find: jest.fn(),
  },
}

const eventsService = {
  publishJournalEvent: jest.fn(),
}

const job = generateJob()

describe('journalActivationJobs', () => {
  beforeAll(() => {
    jest.clearAllMocks()
  })

  it('should have the handler exposed', () => {
    const wrapper = initialize({ models, eventsService })
    const handler = 'handle'
    expect(wrapper).toHaveProperty(handler)
    expect(typeof wrapper[handler]).toBe('function')
  })

  it('should have the subscribe function exposed', () => {
    const wrapper = initialize({ models, eventsService })
    const subscribe = 'subscribe'
    expect(wrapper).toHaveProperty(subscribe)
    expect(typeof wrapper[subscribe]).toBe('function')
  })

  it('should activate the journal', async () => {
    const journal = generateJournal({ isActive: false })
    jest.spyOn(models.Journal, 'find').mockResolvedValue(journal)

    job.data = { journalId: journal.id }

    await initialize({
      models,
      eventsService,
    }).handle(job)

    expect(journal.save).toHaveBeenCalled()
    expect(eventsService.publishJournalEvent).toHaveBeenCalledWith({
      journalId: journal.id,
      eventName: 'JournalActivated',
    })
  })

  it('should have called job.done', () => {
    const wrapper = initialize({ models, eventsService })
    wrapper.handle(job)
    expect(job.done).toHaveBeenCalled()
  })
})
