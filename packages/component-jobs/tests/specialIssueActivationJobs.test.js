const { initialize } = require('../src/specialIssueActivationJobs')
const {
  generateSection,
  generateSpecialIssue,
  generateJob,
} = require('component-generators')

const models = {
  Job: {
    subscribe: jest.fn(),
  },
  SpecialIssue: { find: jest.fn() },
  Section: { find: jest.fn() },
}

const eventsService = {
  publishJournalEvent: jest.fn(),
}

const job = generateJob()

const logger = {
  info: jest.fn(),
}

describe('specialIssueActivationJobs', () => {
  let wrapper
  beforeAll(() => {
    jest.clearAllMocks()
    wrapper = initialize({ models, eventsService, logger })
  })

  it('should have the handler exposed', () => {
    const handler = 'handle'
    expect(wrapper).toHaveProperty(handler)
    expect(typeof wrapper[handler]).toBe('function')
  })

  it('should have the subscribe function exposed', () => {
    const subscribe = 'subscribe'
    expect(wrapper).toHaveProperty(subscribe)
    expect(typeof wrapper[subscribe]).toBe('function')
  })

  it('should have called job.done', async () => {
    await wrapper.handle(job)
    expect(job.done).toHaveBeenCalled()
  })

  it('should activate special issue when isActive is false', async () => {
    const specialIssue = generateSpecialIssue({ isActive: false })
    jest.spyOn(models.SpecialIssue, 'find').mockResolvedValue(specialIssue)
    job.data.specialIssueId = specialIssue.id

    await wrapper.handle(job)

    expect(specialIssue.save).toHaveBeenCalled()
    expect(models.SpecialIssue.find).toHaveBeenCalled()
    expect(eventsService.publishJournalEvent).toHaveBeenCalledWith({
      journalId: specialIssue.journalId,
      eventName: 'JournalSpecialIssueOpened',
    })
  })
  it('should emit JournalSectionSpecialIssueOpen event', async () => {
    const section = generateSection()
    const specialIssue = generateSpecialIssue({
      isActive: false,
      sectionId: section.id,
    })
    jest.spyOn(models.SpecialIssue, 'find').mockResolvedValue(specialIssue)
    jest.spyOn(models.Section, 'find').mockResolvedValue(section)
    job.data.specialIssueId = specialIssue.id

    await wrapper.handle(job)

    expect(eventsService.publishJournalEvent).toHaveBeenCalledWith({
      journalId: section.journalId,
      eventName: 'JournalSectionSpecialIssueOpened',
    })
  })
})
