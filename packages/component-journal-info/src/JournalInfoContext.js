import { createContext } from 'react'

export default createContext({
  publisher: null,
})
