import React from 'react'

import JournalContext from './JournalInfoContext'

function JournalProvider({ children, publisher }) {
  return (
    <JournalContext.Provider value={{ ...publisher }}>
      {children}
    </JournalContext.Provider>
  )
}

export default JournalProvider
