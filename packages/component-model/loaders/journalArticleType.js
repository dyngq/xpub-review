const JournalArticleType = require('../model/journalArticleType')
const { groupResultsByKey } = require('../../component-dataloader-tools')

module.exports = {
  journalArticleTypeLoader: async journalIds => {
    const articleTypes = await JournalArticleType.query()
      .select('journalId', 'articleType.*')
      .join('articleType', 'articleType.id', 'articleTypeId')
      .whereIn('journalId', journalIds)
    const articleTypesByJournal = groupResultsByKey({
      keys: journalIds,
      results: articleTypes,
      getKey: articleType => articleType.journalId,
    })
    return Promise.all(articleTypesByJournal)
  },
}
