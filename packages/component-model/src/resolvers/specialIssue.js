const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('../useCases')

const resolvers = {
  SpecialIssue: {
    peerReviewModel(specialIssue) {
      return useCases.getSpecialIssuePeerReviewModelUseCase
        .initialize(models)
        .execute({ specialIssue })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
