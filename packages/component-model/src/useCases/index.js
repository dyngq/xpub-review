const userUseCases = require('./user')
const journalUseCases = require('./journal')
const manuscriptUseCases = require('./manuscript')
const teamMemberUseCases = require('./teamMember')
const submissionUseCases = require('./submission')
const specialIssueUseCases = require('./specialIssue')

module.exports = {
  ...userUseCases,
  ...journalUseCases,
  ...manuscriptUseCases,
  ...teamMemberUseCases,
  ...submissionUseCases,
  ...specialIssueUseCases,
}
