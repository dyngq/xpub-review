const initialize = ({ Journal }) => ({
  execute: async journalId => {
    const journal = await Journal.find(journalId, [
      'teams.[members.[user.[identities]]]',
      'peerReviewModel',
      'sections.[teams.members.user.identities, specialIssues.[section,teams.members]]',
      'specialIssues.teams.members.user.identities',
      'preprints',
      'journalPreprints',
    ])

    return journal
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
