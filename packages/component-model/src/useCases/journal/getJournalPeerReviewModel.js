const initialize = () => ({
  execute: ({ journal }) =>
    journal.peerReviewModel && journal.peerReviewModel.toDTO(),
})

module.exports = {
  initialize,
}
