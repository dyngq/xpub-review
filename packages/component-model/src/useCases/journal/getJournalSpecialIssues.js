const initialize = () => ({
  execute: ({ journal }) =>
    journal.specialIssues.length
      ? journal.specialIssues.map(si => si.toDTO())
      : [],
})

module.exports = {
  initialize,
}
