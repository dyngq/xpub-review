const initialize = models => ({
  execute: async ({ manuscript }) => {
    if (manuscript.academicEditor) return manuscript.academicEditor
    const { Team, TeamMember } = models

    const hideEmailIfNotAdminOrEa = aeDTO => {
      const { role } = manuscript
      const eligibleRoles = [
        Team.Role.admin,
        Team.Role.editorialAssistant,
        Team.Role.researchIntegrityPublishingEditor,
      ]
      const canSeeEmail = !!role && eligibleRoles.includes(role)
      if (aeDTO.alias && aeDTO.alias.email && !canSeeEmail) {
        delete aeDTO.alias.email
      }
      return aeDTO
    }

    let academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus({
      manuscriptId: manuscript.id,
      role: Team.Role.academicEditor,
      status: TeamMember.Statuses.accepted,
    })

    const academicEditorDTO = academicEditor
      ? academicEditor.toDTO()
      : academicEditor

    academicEditor = academicEditorDTO
      ? hideEmailIfNotAdminOrEa(academicEditorDTO)
      : academicEditorDTO

    return academicEditor
  },
})

module.exports = {
  initialize,
}
