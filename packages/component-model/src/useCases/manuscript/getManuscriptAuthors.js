const { orderBy } = require('lodash')

const initialize = models => ({
  execute: async ({ manuscript }) => {
    const { Team, TeamMember } = models
    if (manuscript.authors && manuscript.authors.length > 0)
      return manuscript.authors

    const authors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.author,
    })

    return orderBy(authors, 'position', 'asc').map(a => a.toDTO())
  },
})

module.exports = {
  initialize,
}
