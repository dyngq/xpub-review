const initialize = ({ models, useCases, userId }) => ({
  execute: async ({ manuscript }) => {
    const { Manuscript, Team, Review, PeerReviewModel } = models
    const { reject, publish, revision } = Review.Recommendations

    // As an approval editor I can't make any decision on a draft manuscript
    if (manuscript.status === Manuscript.Statuses.draft) return []

    // As an academic editor, i should be able to take a decision on the manuscript
    // if the manuscript has conflicts of interest between the triage editor
    // and the author
    if (manuscript.hasTriageEditorConflictOfInterest)
      return [publish, revision, reject]

    // As an approval editor I can only publish or reject the manuscript, if
    // there is no peer review cycle
    if (!manuscript.articleType.hasPeerReview) return [publish, reject]

    const prm = await PeerReviewModel.findOneByManuscriptParent(manuscript)

    let useCase =
      'getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase'
    if (prm.approvalEditors.includes(Team.Role.triageEditor))
      useCase =
        'getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase'

    return useCases[useCase]
      .initialize({ models, userId })
      .execute({ manuscript })
  },
})

module.exports = {
  initialize,
}
