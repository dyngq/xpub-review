const initialize = ({ models: { Team, TeamMember, Review }, userId }) => ({
  execute: async ({ manuscript }) => {
    const {
      reject,
      publish,
      revision,
      returnToAcademicEditor,
    } = Review.Recommendations

    const manuscriptEditorRecommendations = await Review.findAllValidByManuscriptAndRole(
      { manuscriptId: manuscript.id, role: Team.Role.academicEditor },
    )
    // As an approval editor I should not be able to take any decision if the
    // current version has a revision recommendation
    const revisionRecommendationTypes = [
      Review.Recommendations.minor,
      Review.Recommendations.major,
      Review.Recommendations.revision,
    ]
    const revisionRecommendation = manuscriptEditorRecommendations.find(r =>
      revisionRecommendationTypes.includes(r.recommendation),
    )
    if (revisionRecommendation) return []

    // As an approval editor I can publish, reject or return the manuscript
    // if I already have a publish or reject recommendation
    const finalRecommendationTypes = [
      Review.Recommendations.reject,
      Review.Recommendations.publish,
    ]
    const finalRecommendation = manuscriptEditorRecommendations.find(r =>
      finalRecommendationTypes.includes(r.recommendation),
    )
    if (finalRecommendation) return [publish, returnToAcademicEditor, reject]

    const submissionReviewerReports = await Review.findAllValidAndSubmitedBySubmissionAndRole(
      { submissionId: manuscript.submissionId, role: Team.Role.reviewer },
    )
    // As an approval editor, i can publish, reject or ask for a revision if I
    // have a reviewer report on any previous version, even if I don't have a
    // recommendation on the current version
    if (submissionReviewerReports.length) {
      return [publish, revision, reject]
    }

    const pendingOrAcceptedAcademicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatuses(
      {
        role: Team.Role.academicEditor,
        statuses: [TeamMember.Statuses.pending, TeamMember.Statuses.accepted],
        manuscriptId: manuscript.id,
      },
    )
    // As an approval editor I should only be able to reject the
    // manuscript if the academic editor is pending/accepted and I have no
    // previous editor recommendations or reviewer reports
    if (pendingOrAcceptedAcademicEditor) return [reject]

    return [revision, reject]
  },
})

module.exports = {
  initialize,
}
