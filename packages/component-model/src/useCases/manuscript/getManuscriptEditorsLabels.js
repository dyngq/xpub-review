const { get } = require('lodash')

const initialize = models => ({
  execute: async ({ manuscript }) => {
    const { PeerReviewModel, Team, TeamMember } = models
    let peerReviewModel
    const hasSpecialIssueEditorialConflictOfInterest = await manuscript.hasSpecialIssueEditorialConflictOfInterest(
      { Team, TeamMember },
    )

    if (
      manuscript.specialIssueId &&
      !hasSpecialIssueEditorialConflictOfInterest
    ) {
      peerReviewModel = await PeerReviewModel.findOneBySpecialIssue(
        manuscript.specialIssueId,
      )
    } else {
      peerReviewModel = await PeerReviewModel.findOneByJournal(
        manuscript.journalId,
      )
    }

    const academicEditorLabel = get(peerReviewModel, 'academicEditorLabel')
    const triageEditorLabel = get(peerReviewModel, 'triageEditorLabel')
    return { academicEditorLabel, triageEditorLabel }
  },
})

module.exports = {
  initialize,
}
