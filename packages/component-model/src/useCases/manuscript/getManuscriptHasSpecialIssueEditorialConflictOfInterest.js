const initialize = models => ({
  execute: async ({ manuscript }) => {
    const hasSpecialIssueEditorialConflictOfInterest = await manuscript.hasSpecialIssueEditorialConflictOfInterest(
      models,
    )
    return hasSpecialIssueEditorialConflictOfInterest
  },
})

module.exports = {
  initialize,
}
