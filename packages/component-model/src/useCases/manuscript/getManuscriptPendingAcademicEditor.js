const initialize = models => ({
  execute: async ({ manuscript }) => {
    if (manuscript.pendingAcademicEditor)
      return manuscript.pendingAcademicEditor

    const { Team, TeamMember } = models
    const pendingAcademicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.pending,
      },
    )

    return pendingAcademicEditor ? pendingAcademicEditor.toDTO() : undefined
  },
})

module.exports = {
  initialize,
}
