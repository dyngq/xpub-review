const initialize = models => ({
  execute: async ({ manuscript }) => {
    const { Team, TeamMember } = models
    if (manuscript.reviewers && manuscript.reviewers.length > 0)
      return manuscript.reviewers

    const hideEmailIfNotAdminOrEa = reDTO => {
      const { role } = manuscript
      const eligibleRoles = [
        Team.Role.admin,
        Team.Role.editorialAssistant,
        Team.Role.researchIntegrityPublishingEditor,
      ]
      const canSeeEmail = !!role && eligibleRoles.includes(role)
      if (reDTO.alias && reDTO.alias.email && !canSeeEmail) {
        delete reDTO.alias.email
      }
      return reDTO
    }

    const reviewers = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.reviewer,
    })

    return reviewers.map(reviewer => {
      const reviewerDTO = reviewer ? reviewer.toDTO() : reviewer
      reviewer = reviewerDTO
        ? hideEmailIfNotAdminOrEa(reviewerDTO)
        : reviewerDTO
      return reviewer
    })
  },
})

module.exports = {
  initialize,
}
