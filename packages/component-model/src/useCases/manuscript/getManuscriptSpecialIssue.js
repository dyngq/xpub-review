const initialize = models => ({
  execute: async ({ manuscript }) => {
    const { SpecialIssue } = models
    if (manuscript.specialIssue) return manuscript.specialIssue

    if (manuscript.specialIssueId) {
      const specialIssue = await SpecialIssue.find(manuscript.specialIssueId)
      return specialIssue
    }
  },
})

module.exports = {
  initialize,
}
