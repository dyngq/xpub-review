const capitalize = s => {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}
const initialize = _ => ({
  execute: async ({ manuscript }) => {
    const statusCategory = manuscript.statusCategory || 'inProgress'
    return `status${capitalize(statusCategory)}`
  },
})

module.exports = {
  initialize,
}
