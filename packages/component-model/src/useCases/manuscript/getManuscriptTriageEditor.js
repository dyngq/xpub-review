const initialize = models => ({
  execute: async ({ manuscript }) => {
    if (manuscript.triageEditor) return manuscript.triageEditor
    const { Team, TeamMember } = models

    const hideEmailIfNotAdminOrEa = teDTO => {
      const { role } = manuscript
      const eligibleRoles = [
        Team.Role.admin,
        Team.Role.editorialAssistant,
        Team.Role.researchIntegrityPublishingEditor,
      ]
      const canSeeEmail = !!role && eligibleRoles.includes(role)
      if (teDTO.alias && teDTO.alias.email && !canSeeEmail) {
        delete teDTO.alias.email
      }
      return teDTO
    }

    let triageEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus({
      manuscriptId: manuscript.id,
      role: Team.Role.triageEditor,
      status: TeamMember.Statuses.active,
    })

    const triageEditorDTO = triageEditor ? triageEditor.toDTO() : triageEditor
    triageEditor = triageEditorDTO
      ? hideEmailIfNotAdminOrEa(triageEditorDTO)
      : triageEditorDTO

    return triageEditor
  },
})

module.exports = {
  initialize,
}
