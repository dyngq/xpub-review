const initialize = ({ models: { TeamMember, Team }, Promise }) => ({
  execute: async ({
    submission,
    newEditorialAssistant,
    journalEditorialAssistant,
  }) => {
    const currentEditorialAssistants = []
    const newEditorialAssistants = []
    await Promise.each(submission, async manuscript => {
      // remove current EA from manuscript
      const currentEditorialAssistant = await TeamMember.findOneByUserAndRoleAndStatusOnManuscript(
        {
          manuscriptId: manuscript.id,
          status: TeamMember.Statuses.active,
          role: Team.Role.editorialAssistant,
          userId: journalEditorialAssistant.userId,
        },
      )
      currentEditorialAssistant.updateProperties({
        status: TeamMember.Statuses.removed,
      })
      currentEditorialAssistants.push(currentEditorialAssistant)

      // assign the found EA to the manuscript
      const newManuscriptEditorialAssistant = new TeamMember({
        userId: newEditorialAssistant.userId,
        teamId: currentEditorialAssistant.teamId,
        status: TeamMember.Statuses.active,
        alias: newEditorialAssistant.alias,
      })
      newEditorialAssistants.push(newManuscriptEditorialAssistant)
    })

    return { currentEditorialAssistants, newEditorialAssistants }
  },
})

module.exports = { initialize }
