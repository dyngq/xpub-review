const getUpdatedEditorialAssistantsUseCase = require('./getUpdatedEditorialAssistants')
const redistributeEditorialAssistantsUseCase = require('./redistributeEditorialAssistants')
const getSubmissionsWithUpdatedEditorialAssistantsUseCase = require('./getSubmissionsWithUpdatedEditorialAssistants')

module.exports = {
  getUpdatedEditorialAssistantsUseCase,
  redistributeEditorialAssistantsUseCase,
  getSubmissionsWithUpdatedEditorialAssistantsUseCase,
}
