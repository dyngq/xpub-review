const initialize = ({ logger, models, useCases, eventsService, Promise }) => ({
  execute: async journalId => {
    const { TeamMember, Team, Manuscript } = models
    // TO DO: figure out in progress statuses for EAs
    const editorialAssistantWorkingStatuses = [
      ...Manuscript.InProgressStatuses,
      Manuscript.Statuses.qualityChecksRequested,
      Manuscript.Statuses.qualityChecksSubmitted,
      Manuscript.Statuses.technicalChecks,
    ]
    const editorialAssistants = await TeamMember.findAllByJournalWithWorkload({
      journalId,
      role: Team.Role.editorialAssistant,
      teamMemberStatuses: [TeamMember.Statuses.active],
      manuscriptStatuses: editorialAssistantWorkingStatuses,
    })
    if (editorialAssistants.length < 2) {
      logger.warn(`No editorial assistants found on journal ${journalId}`)
      return
    }

    const {
      count: numberOfSubmissions,
    } = await Manuscript.countSubmissionsByJournalAndStatuses({
      journalId,
      statuses: editorialAssistantWorkingStatuses,
    })
    const averageWorkload = Math.floor(
      numberOfSubmissions / editorialAssistants.length,
    )

    await Promise.each(editorialAssistants, async editorialAssistant => {
      const numberOfExtraSubmissions =
        editorialAssistant.workload - averageWorkload
      if (numberOfExtraSubmissions < 1) {
        logger.info(
          `User ${
            editorialAssistant.userId
          } with workload ${editorialAssistant.workload || 0} does 
           not have any extra submissions. Skipping...`,
        )
        return
      }

      const submissionIds = await Manuscript.findSubmissionsByJournalAndUserAndRoleAndTeamMemberStatuses(
        {
          journalId,
          limit: numberOfExtraSubmissions,
          userId: editorialAssistant.userId,
          role: Team.Role.editorialAssistant,
          manuscriptStatuses: editorialAssistantWorkingStatuses,
          teamMemberStatuses: [TeamMember.Statuses.active],
        },
      )
      const extraSubmissions = []
      await Promise.each(submissionIds, async submission => {
        const { submissionId } = submission
        const manuscriptsInSubmission = await Manuscript.findAll({
          queryObject: {
            submissionId,
          },
        })
        if (!manuscriptsInSubmission.length) return

        extraSubmissions.push(manuscriptsInSubmission)
      })

      const {
        editorRemovedSubmissions,
        editorAssignedSubmissions,
      } = await useCases.getSubmissionsWithUpdatedEditorialAssistantsUseCase
        .initialize({ models, useCases, Promise })
        .execute({
          averageWorkload,
          submissions: extraSubmissions,
          editorialAssistantWorkingStatuses,
          journalEditorialAssistant: editorialAssistant,
          journalEditorialAssistants: editorialAssistants,
        })
      editorRemovedSubmissions.forEach(submissionId => {
        eventsService.publishSubmissionEvent({
          submissionId,
          eventName: 'SubmissionEditorialAssistantRemoved',
        })
      })
      editorAssignedSubmissions.forEach(submissionId => {
        eventsService.publishSubmissionEvent({
          submissionId,
          eventName: 'SubmissionEditorialAssistantAssigned',
        })
      })
    })
  },
})

module.exports = {
  initialize,
}
