const initialize = ({ PeerReviewModel }) => ({
  execute: async ({ specialIssue }) => {
    if (specialIssue.peerReviewModel) return specialIssue.peerReviewModel

    const peerReviewModel = await PeerReviewModel.find(
      specialIssue.peerReviewModelId,
    )
    return peerReviewModel.toDTO()
  },
})

module.exports = {
  initialize,
}
