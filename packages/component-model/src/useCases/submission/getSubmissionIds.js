// Temporary use-case for redirect

const initialize = models => ({
  execute: async ({ customId }) => {
    const { Manuscript } = models

    const manuscript = await Manuscript.findLastManuscriptByCustomId({
      customId,
    })

    return manuscript
  },
})

const authsomePolicies = ['hasAccessToSubmissionByCustomId', 'admin']

module.exports = {
  initialize,
  authsomePolicies,
}
