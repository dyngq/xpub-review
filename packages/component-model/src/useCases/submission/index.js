const getSubmissionUseCase = require('./getSubmission')
const getSubmissionIdsUseCase = require('./getSubmissionIds')

module.exports = {
  getSubmissionUseCase,
  getSubmissionIdsUseCase,
}
