module.exports.initialize = ({ Manuscript, Team, TeamMember }) => ({
  async execute(teamMember) {
    const workload = await TeamMember.findTeamMemberWorkloadByUser({
      userId: teamMember.userId,
      role: Team.Role.academicEditor,
      manuscriptStatuses: Manuscript.InProgressStatuses,
      teamMemberStatuses: [
        TeamMember.Statuses.pending,
        TeamMember.Statuses.accepted,
      ],
    })
    return workload.count
  },
})
