const { isEmpty, chain } = require('lodash')

function initialize({
  models: { TeamMember, EditorSuggestion, Manuscript, Team },
}) {
  return {
    execute,
  }

  async function execute({
    manuscriptId,
    journalId,
    sectionId,
    specialIssueId,
  }) {
    const role = Team.Role.academicEditor
    const teamMemberStatuses = [
      TeamMember.Statuses.pending,
      TeamMember.Statuses.accepted,
    ]
    const manuscriptStatuses = Manuscript.InProgressStatuses

    let teamMembers
    let manuscriptTeamMembersAndWorkload

    if (specialIssueId) {
      teamMembers = await TeamMember.findAllBySpecialIssueAndRole({
        role,
        specialIssueId,
      })
      manuscriptTeamMembersAndWorkload = await TeamMember.findTeamMembersWorkloadBySpecialIssue(
        {
          role,
          specialIssueId,
          teamMemberStatuses,
          manuscriptStatuses,
        },
      )
    }

    if (isEmpty(teamMembers) && sectionId) {
      teamMembers = await TeamMember.findAllBySectionAndRole({
        sectionId,
        role,
      })
      manuscriptTeamMembersAndWorkload = await TeamMember.findTeamMembersWorkloadBySection(
        {
          role,
          sectionId,
          teamMemberStatuses,
          manuscriptStatuses,
        },
      )
    }

    if (isEmpty(teamMembers) && journalId) {
      teamMembers = await TeamMember.findAllByJournalAndRole({
        journalId,
        role,
      })
      manuscriptTeamMembersAndWorkload = await TeamMember.findTeamMembersWorkloadByJournal(
        {
          role,
          journalId,
          teamMemberStatuses,
          manuscriptStatuses,
        },
      )
    }

    if (isEmpty(teamMembers)) {
      throw new Error('No manuscript parent ID has been provided')
    }

    const editorSuggestions = await EditorSuggestion.findBy({
      manuscriptId,
    })

    const manuscriptAcademicEditors = await TeamMember.findAllByManuscriptAndRole(
      {
        manuscriptId,
        role: Team.Role.academicEditor,
      },
    )

    const user = chain(teamMembers)
      .filter(teamMember => {
        const memberIsAlreadyInvited = manuscriptAcademicEditors.find(
          academicEditor => academicEditor.userId === teamMember.userId,
        )
        return !memberIsAlreadyInvited
      })
      .map(teamMember => {
        const editorSuggestion = editorSuggestions.find(
          editorSubmissionScore =>
            editorSubmissionScore.teamMemberId === teamMember.id,
        )

        const editorWorkload = manuscriptTeamMembersAndWorkload.find(
          editorWithWorkload => editorWithWorkload.userId === teamMember.userId,
        )

        return {
          ...teamMember,
          score: editorSuggestion ? editorSuggestion.score : 0,
          workload: editorWorkload ? editorWorkload.workload : 0,
        }
      })
      .orderBy(['score', 'workload'], ['desc', 'asc'])
      .head()
      .value()

    return user
  }
}
module.exports = {
  initialize,
}
