module.exports.initialize = ({ models, useCases }) => ({
  execute: async ({ teamMember, ctx }) => {
    const { Team } = models

    const loadedTeamMember = await ctx.loaders.TeamMember.teamMemberTeamLoader.load(
      teamMember.id,
    )
    const { role } = loadedTeamMember.team

    if (role !== Team.Role.academicEditor) return null
    const useCase = 'getAcademicEditorWorkloadUseCase'

    return useCases[useCase].initialize(models).execute(teamMember)
  },
})
