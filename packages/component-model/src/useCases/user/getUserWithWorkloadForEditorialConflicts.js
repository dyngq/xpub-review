const { unionBy, orderBy, isEmpty, intersectionBy } = require('lodash')

const initialize = ({ TeamMember, SpecialIssue }) => ({
  execute: async ({
    role,
    journalId,
    specialIssueId,
    manuscriptStatuses,
    teamMemberStatuses,
  }) => {
    let teamMembers
    let manuscriptTeamMembersAndWorkload

    const specialIssue = await SpecialIssue.find(specialIssueId)
    if (specialIssue.sectionId) {
      teamMembers = await TeamMember.findAllBySectionAndRole({
        sectionId: specialIssue.sectionId,
        role,
      })
      const sectionTeamMembersAndWorkload = await TeamMember.findTeamMembersWorkloadBySection(
        {
          role,
          sectionId: specialIssue.sectionId,
          teamMemberStatuses,
          manuscriptStatuses,
        },
      )
      const specialIssueTeamMembersAndWorkload = await TeamMember.findTeamMembersWorkloadBySpecialIssue(
        {
          role,
          specialIssueId,
          teamMemberStatuses,
          manuscriptStatuses,
        },
      )
      const sectionAndSpecialIssueTeamMembersAndWorkload = [
        ...sectionTeamMembersAndWorkload,
        ...specialIssueTeamMembersAndWorkload,
      ]
      manuscriptTeamMembersAndWorkload = intersectionBy(
        sectionAndSpecialIssueTeamMembersAndWorkload,
        teamMembers,
        'userId',
      )
    } else {
      teamMembers = await TeamMember.findAllByJournalAndRole({
        role,
        journalId,
      })
      const specialIssueAndJournalTeamMembersAndWorkload = await TeamMember.findTeamMembersWorkloadByJournalAndSpecialIssue(
        {
          role,
          journalId,
          teamMemberStatuses,
          manuscriptStatuses,
        },
      )
      manuscriptTeamMembersAndWorkload = intersectionBy(
        specialIssueAndJournalTeamMembersAndWorkload,
        teamMembers,
        'userId',
      )
    }
    if (isEmpty(teamMembers))
      throw new Error('No manuscript parent ID has been provided')

    const usersDefaultWorkload = teamMembers.map(member => ({
      userId: member.userId,
      workload: 0,
    }))

    const users = orderBy(
      unionBy(manuscriptTeamMembersAndWorkload, usersDefaultWorkload, 'userId'),
      ['workload'],
      ['asc'],
    )

    return users[0]
  },
})

module.exports = { initialize }
