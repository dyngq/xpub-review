module.exports = {
  getJsonTemplate: (config = {}) => ({
    _declaration: {
      _attributes: {
        version: '1.0',
        encoding: 'utf-8',
      },
    },
    _doctype: config.doctype,
    article: {
      _attributes: {
        'dtd-version': config.dtdVersion,
        'article-type': config.articleType.name,
      },
      front: {
        'journal-meta': {
          'journal-id': [
            {
              _attributes: {
                'journal-id-type': 'publisher',
              },
              _text: config.journalIdPublisher,
            },
            {
              _attributes: {
                'journal-id-type': 'email',
              },
              _text: config.email,
            },
          ],
          'journal-subcode': {
            _text: config.journalIdPublisher,
          },
          'journal-title-group': {
            'journal-title': {
              _text: config.journalTitle,
            },
          },
          issn: [
            {
              _attributes: {
                'pub-type': 'epub',
              },
              _text: config.issn,
            },
          ],
          publisher: {
            'publisher-name': {
              _text: 'Hindawi',
            },
          },
        },
        'article-meta': {
          'pub-date': {
            _attributes: {
              'pub-type': 'publication-year',
            },
            year: {
              _text: '2019',
            },
          },
          'article-id': [
            {
              _attributes: {
                'pub-id-type': 'publisher-id',
              },
              _text: 'HINDAWI-D-00-00000',
            },
            {
              _attributes: {
                'pub-id-type': 'manuscript',
              },
              _text: 'HINDAWI-D-00-00000',
            },
          ],
          'article-categories': {
            'subj-group': [
              {
                _attributes: {
                  'subj-group-type': 'Article Type',
                },
                subject: {
                  _text: config.articleType.name,
                },
              },
            ],
          },
          'article-version': {
            _text: '1',
          },
          'title-group': {
            'article-title': {
              _text: 'Untitled Article Title ',
            },
          },
          'contrib-group': {
            contrib: {
              _attributes: {
                'contrib-type': 'author',
                corresp: 'yes',
              },
              'contrib-id': {
                _attributes: {
                  'contrib-id-type': 'orcid',
                },
              },
              role: {
                _attributes: {
                  'content-type': '1',
                },
              },
              name: {
                surname: {
                  _text: 'First Name',
                },
                'given-names': {
                  _text: 'Last Name',
                },
                prefix: {
                  _text: 'Dr.',
                },
              },
              email: {
                _text: config.email,
              },
              xref: {
                _attributes: {
                  'ref-type': 'aff',
                  rid: 'aff1',
                },
                sup: {
                  _text: '1',
                },
              },
            },
          },
          history: {
            date: {
              _attributes: {
                'date-type': 'received',
              },
              day: {
                _text: '01',
              },
              month: {
                _text: '01',
              },
              year: {
                _text: '1970',
              },
            },
          },
          abstract: {
            _text: 'No abstract provided',
          },
          'funding-group': {},
        },
        files: {
          file: [],
        },
        questions: {
          question: [],
        },
        'rev-group': {
          rev: [
            {
              _attributes: {
                'rev-type': '',
              },
              name: {
                surname: {
                  _text: 'Last Name',
                },
                'given-names': {
                  _text: 'First Name',
                },
                prefix: {
                  _text: 'Dr',
                },
              },
              email: {
                _text: config.email,
              },
              xref: {
                _attributes: {
                  'ref-type': 'aff',
                  rid: 'aff1',
                },
              },
              date: [
                {
                  _attributes: {
                    'date-type': 'assignment',
                  },
                  day: {
                    _text: '24',
                  },
                  month: {
                    _text: '10',
                  },
                  year: {
                    _text: '2018',
                  },
                },
                {
                  _attributes: {
                    'date-type': 'submission',
                  },
                  day: {
                    _text: '24',
                  },
                  month: {
                    _text: '10',
                  },
                  year: {
                    _text: '2018',
                  },
                },
              ],
              comment: {
                _attributes: {
                  'comment-type': 'comment',
                },
                _text: '',
              },
              files: {
                file: [
                  {
                    item_type: {
                      _text: '',
                    },
                    item_description: {
                      _text: '',
                    },
                    item_name: {
                      _text: '',
                    },
                  },
                ],
              },
              recommendation: {
                _text: '',
              },
            },
          ],
          aff: {
            _attributes: {
              id: 'aff1',
            },
            'addr-line': {
              _text: 'University of Fort Hare, Alice 5700, South Africa',
            },
          },
        },
      },
    },
  }),
}
