const convert = require('xml-js')
const { get, find, pick } = require('lodash')

const { getJsonTemplate } = require('./getJsonTemplate')

const {
  setFiles,
  setCounts,
  setHistory,
  setMetadata,
  setContacts,
  setReviewers,
  createFileName,
  setContributors,
  setDeclarations,
  setEmptyReviewers,
} = require('./templateSetters')

module.exports = {
  convertToXML: ({ json = {}, options, prefix }) => {
    const content = convert.json2xml(json, options)
    const customId = get(
      json,
      'article.front.article-meta.article-id[0]._text',
      createFileName({ prefix }),
    )
    const name = `${customId}.xml`
    return {
      name,
      content,
    }
  },
  composeJson: ({
    config,
    options,
    isEQA = false,
    manuscript = {},
    screeners = [],
    figureFilesCount,
  }) => {
    const {
      teams = [],
      files = [],
      reviews = [],
      updated,
      status,
      submittedDate,
      peerReviewPassedDate,
    } = manuscript
    const journalEditorialAssistantTeam = manuscript.journal.teams.find(
      team => team.role === 'editorialAssistant',
    )
    const manuscriptEditorialAssistantTeam = teams.find(
      team => team.role === 'editorialAssistant',
    )

    const journalPreprintsTypes = config.preprints
      ? config.preprints.map(pp => pp.type)
      : []

    const meta = pick(manuscript, [
      'title',
      'abstract',
      'articleType',
      'specialIssue',
      'version',
      'section',
      'preprintValue',
    ])
    const finalRevision = get(find(updated), 'date', Date.now())

    const jsonTemplate = getJsonTemplate(config)
    const fileName = createFileName({
      id: manuscript.customId,
      prefix: config.prefix,
    })

    let composedJson = {
      ...jsonTemplate,
      ...setMetadata({
        meta,
        options,
        fileName,
        jsonTemplate,
        journalPreprintsTypes,
      }),
      ...setContributors(teams, jsonTemplate),
      ...setHistory(
        peerReviewPassedDate,
        submittedDate,
        finalRevision,
        status,
        jsonTemplate,
      ),
      ...setFiles(files, jsonTemplate),
      ...setDeclarations(manuscript, jsonTemplate),
      ...setEmptyReviewers(jsonTemplate),
    }

    if (isEQA) {
      const mtsReviews = reviews.filter(rev => rev.submitted)
      const authorTeam = teams.find(t => t.role === 'author')

      composedJson = setReviewers({
        jsonTemplate,
        reviews: mtsReviews,
        authorsLength: authorTeam.members.length,
      })
    }
    if (status === 'published') {
      composedJson = {
        ...setCounts(jsonTemplate, figureFilesCount),
        ...setContacts(
          screeners,
          jsonTemplate,
          journalEditorialAssistantTeam,
          manuscriptEditorialAssistantTeam,
        ),
      }
    }
    return composedJson
  },
  getRemoteFolder: ({ isEQA, isManuscriptRejected, manuscript }) => {
    const remoteRoot = `/${manuscript.journal.code}`

    if (!isEQA && !isManuscriptRejected) return remoteRoot

    return isManuscriptRejected
      ? `${remoteRoot}/_Rejected`
      : `${remoteRoot}/_Accepted`
  },
}
