const { get } = require('lodash')
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { getJsonTemplate } = require('../src/getJsonTemplate')
const { defaultConfig, defaultParseXmlOptions } = require('../config/default')
const { convertToXML, composeJson, getRemoteFolder } = require('../src/helpers')

const {
  Team,
  Review,
  Comment,
  Section,
  Journal,
  Manuscript,
  TeamMember,
  ArticleType,
  SpecialIssue,
} = models
const chance = new Chance()

describe('MTS helpers', () => {
  describe('composeJson', () => {
    let journal
    let section
    let articleType
    let specialIssue
    let token
    beforeAll(async () => {
      journal = fixtures.generateJournal({ Journal })
      await dataService.createUserOnJournal({
        models,
        journal,
        fixtures,
        role: Team.Role.editorialAssistant,
        input: { isCorresponding: true },
      })

      fixtures.generateArticleTypes(ArticleType)
      section = fixtures.generateSection({
        properties: {
          name: 'Cellular Biology',
        },
        Section,
      })
      articleType = fixtures.getArticleTypeByPeerReview(true)
      specialIssue = fixtures.generateSpecialIssue({
        properties: {
          name: 'Super Special Issue',
          customId: '133751',
          isActive: true,
        },
        SpecialIssue,
      })
      token = chance.guid()
    })
    it('returns a json with correct article-meta data', async () => {
      const manuscript = fixtures.generateManuscript({
        properties: {
          section,
          version: '1',
          articleType,
          specialIssue,
        },
        Manuscript,
      })
      manuscript.journal = journal

      await dataService.createUserOnManuscript({
        models,
        fixtures,
        manuscript,
        role: Team.Role.author,
        input: { isSubmitting: true },
      })
      await dataService.createUserOnManuscript({
        models,
        fixtures,
        manuscript,
        role: Team.Role.academicEditor,
        input: { status: 'accepted' },
      })

      const composedJson = composeJson({
        manuscript,
        isEQA: false,
        config: defaultConfig,
        options: defaultParseXmlOptions,
      })

      expect(
        get(
          composedJson,
          'article.front.article-meta.title-group.article-title._text',
        ),
      ).toBe(manuscript.title)

      expect(
        get(composedJson, 'article.front.article-meta.abstract._text'),
      ).toBe(manuscript.abstract)

      expect(
        get(composedJson, 'article.front.article-meta.article-version._text'),
      ).toBe(manuscript.version)

      expect(
        get(composedJson, 'article.front.article-meta.special-issue-id._text'),
      ).toBe(manuscript.specialIssue.customId)

      expect(
        get(
          composedJson,
          'article.front.article-meta.special-issue-title._text',
        ),
      ).toBe(manuscript.specialIssue.name)

      expect(
        get(
          composedJson,
          'article.front.article-meta.article-categories.subj-group[0].subject._text',
        ),
      ).toBe(manuscript.articleType.name)

      expect(
        get(composedJson, 'article.front.article-meta.subject-area._text'),
      ).toBe(section.name)

      expect(
        get(composedJson, 'article.front.article-meta.contrib-group.contrib'),
      ).toHaveLength(2)
    })
    it('returns a json with a rev-group when isEQA is true', async () => {
      const manuscript = fixtures.generateManuscript({
        properties: {
          journal,
          section,
          articleType,
          hasPassedEqa: null,
          technicalCheckToken: token,
          status: Manuscript.Statuses.inQA,
        },
        Manuscript,
      })
      await dataService.createUserOnManuscript({
        models,
        fixtures,
        manuscript,
        role: Team.Role.author,
        input: { isSubmitting: true },
      })
      await dataService.createUserOnManuscript({
        models,
        fixtures,
        manuscript,
        role: Team.Role.academicEditor,
        input: { status: 'accepted' },
      })
      const reviewer = await dataService.createUserOnManuscript({
        models,
        fixtures,
        manuscript,
        role: Team.Role.reviewer,
        input: { status: TeamMember.Statuses.submitted },
      })
      const review = fixtures.generateReview({
        properties: {
          teamMemberId: reviewer.id,
          manuscriptId: manuscript.id,
          recommendation: Review.Recommendations.publish,
        },
        Review,
      })
      const comment = fixtures.generateComment({
        properties: {
          files: [],
          reviewId: review.id,
          content: chance.sentence(),
          type: Comment.Types.public,
        },
        Comment,
      })
      review.comments = [comment]
      review.manuscript = manuscript
      review.member = reviewer
      manuscript.assignReview(review)

      const composedJson = composeJson({
        manuscript,
        isEQA: true,
        config: defaultConfig,
        options: defaultParseXmlOptions,
      })

      expect(composedJson.article.front['rev-group']).toHaveProperty('rev')
      expect(composedJson.article.front['rev-group'].rev).toHaveLength(
        manuscript.reviews.length,
      )
    })
    it("returns a json without section if it doesn't have it", async () => {
      const manuscript = fixtures.generateManuscript({
        properties: {
          articleType,
        },
        Manuscript,
      })
      manuscript.journal = journal

      await dataService.createUserOnManuscript({
        models,
        fixtures,
        manuscript,
        role: Team.Role.author,
        input: { isSubmitting: true },
      })
      await dataService.createUserOnManuscript({
        models,
        fixtures,
        manuscript,
        role: Team.Role.academicEditor,
        input: { status: 'accepted' },
      })

      const composedJson = composeJson({
        manuscript,
        isEQA: false,
        config: defaultConfig,
        options: defaultParseXmlOptions,
      })

      expect(
        get(composedJson, 'article.front.article-meta.custom-meta-group'),
      ).toBe(undefined)
    })
    it("returns a json without academic editor if it doesn't have it", async () => {
      const manuscript = fixtures.generateManuscript({
        properties: {
          section: {},
          articleType,
        },
        Manuscript,
      })
      manuscript.journal = journal

      await dataService.createUserOnManuscript({
        models,
        fixtures,
        manuscript,
        role: Team.Role.author,
        input: { isSubmitting: true },
      })

      const composedJson = composeJson({
        manuscript,
        isEQA: false,
        config: defaultConfig,
        options: defaultParseXmlOptions,
      })

      expect(
        get(composedJson, 'article.front.article-meta.contrib-group.contrib'),
      ).toHaveLength(1)

      expect(
        get(
          composedJson,
          'article.front.article-meta.contrib-group.contrib[0]._attributes.contrib-type',
        ),
      ).toBe('Author')
    })
  })
  describe('convertToXML', () => {
    it('returns a properly formatted object', () => {
      const jsonTemplate = getJsonTemplate(defaultConfig)
      const xmlFile = convertToXML({
        prefix: defaultConfig.prefix,
        options: defaultParseXmlOptions,
        json: jsonTemplate,
      })

      expect(xmlFile).toHaveProperty('name')
      expect(xmlFile).toHaveProperty('content')
      expect(xmlFile.content).toContain(
        jsonTemplate.article.front['article-meta']['title-group'][
          'article-title'
        ]._text,
      )
    })
  })
  describe('getRemoteFolder', () => {
    const manuscript = {
      journal: {
        code: 'TEST',
      },
    }
    it('returns /code for Submit', () => {
      const path = getRemoteFolder({
        isEQA: false,
        isManuscriptRejected: false,
        manuscript,
      })
      expect(path).toBe('/TEST')
    })
    it('returns /code/_Rejected for DeclineEQS', () => {
      const path = getRemoteFolder({
        isEQA: false,
        isManuscriptRejected: true,
        manuscript,
      })
      expect(path).toBe('/TEST/_Rejected')
    })
    it('returns /code/_Accepted for ApproveDecision', () => {
      const path = getRemoteFolder({
        isEQA: true,
        isManuscriptRejected: false,
        manuscript,
      })
      expect(path).toBe('/TEST/_Accepted')
    })
    it('returns /code/_Rejected for RejectDecision', () => {
      const path = getRemoteFolder({
        isEQA: true,
        isManuscriptRejected: true,
        manuscript,
      })
      expect(path).toBe('/TEST/_Rejected')
    })
  })
})
