import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withProps, compose } from 'recompose'
import { Row, Text, ContextualBox } from '@hindawi/ui'

import { ReviewerReportAuthor } from './'

const SubmittedReportsNumberForAuthorReviews = ({ reports }) => (
  <Row fitContent justify="flex-end">
    <Text customId mr={2}>
      {reports}
    </Text>
    <Text mr={2} pr={2} secondary>
      submitted
    </Text>
  </Row>
)

const AuthorReviews = ({ toggle, recommendations, submittedReports }) =>
  submittedReports.length > 0 && (
    <ContextualBox
      label="Reviewer Reports"
      mb={4}
      mt={4}
      rightChildren={() => (
        <SubmittedReportsNumberForAuthorReviews
          reports={submittedReports.length}
        />
      )}
      toggle={toggle}
    >
      <Root>
        {submittedReports.map(r => (
          <ReviewerReportAuthor
            key={r.id}
            recommendations={recommendations}
            report={r}
          />
        ))}
      </Root>
    </ContextualBox>
  )

export default compose(
  withProps(({ reviewerReports }) => ({
    submittedReports: reviewerReports.filter(review => review.submitted),
  })),
)(AuthorReviews)

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
`
