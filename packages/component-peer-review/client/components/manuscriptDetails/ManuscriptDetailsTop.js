import React, { Fragment } from 'react'
import { chain, get, isEmpty } from 'lodash'
import PropTypes from 'prop-types'
import { DateParser, Button } from '@pubsweet/ui'
import { DownloadZip, PreviewFile } from 'component-files/client'
import { compose, withHandlers, withProps } from 'recompose'
import { Row, Item, Icon, Text, NewTheme, useNavigation } from '@hindawi/ui'

import { ManuscriptVersion } from './'

const { Breadcrumbs } = NewTheme

const ManuscriptDetailsTop = ({
  match,
  history,
  goToEdit,
  versions,
  manuscript,
  hasPeerReview,
  manuscriptFile,
  canEditManuscript,
}) => {
  const { goTo } = useNavigation()
  return (
    <Row alignItems="center" height={4} mb={4}>
      <Breadcrumbs
        backLabel="BACK"
        entityName="ARTICLE DETAILS"
        goBack={goTo('/')}
        label="DASHBOARD"
      />

      <Item alignItems="center" justify="flex-end">
        {canEditManuscript && (
          <Button
            data-test-id="button-qa-manuscript-edit"
            mr={6}
            onClick={goToEdit}
            secondary
            xs
          >
            <Icon fontSize="14px" icon="edit" mr={1} />
            Edit
          </Button>
        )}

        {!isEmpty(manuscript.files) && (
          <Fragment>
            <PreviewFile file={manuscriptFile} mr={4} />
            <DownloadZip manuscript={manuscript} mr={4} />
          </Fragment>
        )}

        <DateParser
          dateFormat="YYYY-MM-DD"
          durationThreshold={0}
          timestamp={get(manuscript, 'created', '')}
        >
          {timestamp => (
            <Text mr={6} secondary>
              Updated on {timestamp}
            </Text>
          )}
        </DateParser>
        {hasPeerReview && (
          <ManuscriptVersion
            history={history}
            match={match}
            versions={versions}
          />
        )}
      </Item>
    </Row>
  )
}

export default compose(
  withProps(({ manuscript }) => ({
    hasPeerReview: get(manuscript, 'articleType.hasPeerReview', ''),
    status: get(manuscript, 'status', 'draft'),
    version: get(manuscript, 'version', ''),
    customId: get(manuscript, 'customId', ''),
    manuscriptFile: chain(manuscript)
      .get('files', [])
      .find(file => file.type === 'manuscript')
      .value(),
    role: get(manuscript, 'role', ''),
  })),
  withProps(({ status, isAdminOrEditorialAssistant, isLatestVersion }) => ({
    canEditManuscript:
      isAdminOrEditorialAssistant &&
      isLatestVersion &&
      ![
        'rejected',
        'withdrawn',
        'void',
        'published',
        'refusedToConsider',
      ].includes(status),
  })),
  withHandlers({
    goToEdit: ({ history, manuscript }) => () => {
      history.push(`/submit/${manuscript.submissionId}/${manuscript.id}`)
    },
  }),
)(ManuscriptDetailsTop)

ManuscriptDetailsTop.propTypes = {
  /** Object containing the selected fragment. */
  fragment: PropTypes.object, //eslint-disable-line
  /** Object containing the selected collection. */
  collection: PropTypes.object, //eslint-disable-line
  /** Object with versions of manuscript. */
  versions: PropTypes.array, //eslint-disable-line
  /** An async call that takes you to edit. */
  goToEdit: PropTypes.func,
  /** Object containing token for current user. */
  currentUser: PropTypes.object, //eslint-disable-line
}

ManuscriptDetailsTop.defaultProps = {
  fragment: {},
  collection: {},
  versions: [],
  goToEdit: () => {},
  currentUser: {},
}
