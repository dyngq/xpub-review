import React, { Fragment, useState, useEffect } from 'react'
import { get } from 'lodash'
import { space } from 'styled-system'
import styled from 'styled-components'
import { Modal } from 'component-modal'
import { Button, TextField } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import {
  Row,
  Item,
  Text,
  Icon,
  Label,
  roles,
  Loader,
  MultiAction,
  ContextualBox,
} from '@hindawi/ui'
import { useLazyQuery } from 'react-apollo'
import { getAcademicEditors } from '../../graphql/queries'
import { useAssignAcademicEditor } from '..'
import AssignMyselfButton from './AssignMyselfButton'

const AssignAcademicEditor = ({
  role,
  match,
  toggle,
  expanded,
  manuscript,
  currentUser,
  academicEditorLabel,
}) => {
  const [
    getAcademicEditorsMutation,
    { data, loading },
  ] = useLazyQuery(getAcademicEditors, { fetchPolicy: 'network-only' })
  const academicEditors = get(data, 'getAcademicEditors', [])

  const {
    inviteAcademicEditor,
    assignAcademicEditor,
  } = useAssignAcademicEditor({
    role,
    match,
    manuscript,
  })

  const [searchValue, setSearchValue] = useState('')
  const [inputValue, setInputValue] = useState(searchValue)
  const clearSearch = () => {
    setSearchValue('')
    setInputValue('')
  }
  useEffect(() => {
    getAcademicEditorsMutation({
      variables: {
        searchValue,
        manuscriptId: manuscript.id,
      },
    })
  }, [searchValue, manuscript, getAcademicEditorsMutation])

  const onInvite = ({
    name,
    email,
    searchIndex,
    user: { id: userId },
  }) => modalProps => inviteAcademicEditor(userId, modalProps)

  const filteredAcademicEditors = academicEditors.map(academicEditor => {
    const givenNames = get(academicEditor, 'alias.name.givenNames', '')
    const surname = get(academicEditor, 'alias.name.surname', '')
    const email = get(academicEditor, 'alias.email', '')
    return {
      ...academicEditor,
      email,
      name: `${givenNames} ${surname}`,
      searchIndex: `${givenNames} ${surname} ${email}`,
    }
  })
  const academicEditorStatus = get(manuscript, 'academicEditor.status', '')
  const pendingAcademicEditor = get(manuscript, 'pendingAcademicEditor', '')
  const assignedEditorId = get(manuscript, 'academicEditor.user.id', '')
  const pendingEditorId = get(manuscript, 'pendingAcademicEditor.user.id', '')

  const canReassign =
    academicEditorStatus && academicEditorStatus === 'accepted'
  const canAssignOneself =
    manuscript.specialIssue &&
    manuscript.role === roles.TRIAGE_EDITOR &&
    !manuscript.hasSpecialIssueEditorialConflictOfInterest

  const pendingApprovalLabel = () =>
    pendingAcademicEditor && (
      <Row alignItems="baseline" fitContent justify="flex-end">
        <Text fontWeight={700} invited mr={6} small upperCase>
          Pending Response
        </Text>
      </Row>
    )

  return (
    <ContextualBox
      expanded={expanded}
      height={8}
      highlight={!canReassign}
      label={
        canReassign
          ? `Reassign ${academicEditorLabel}`
          : `Assign ${academicEditorLabel}`
      }
      mt={4}
      rightChildren={pendingApprovalLabel}
      toggle={toggle}
    >
      <Root>
        <Row justify="space-between" p={2} pb={4}>
          <TextContainer>
            <TextField
              data-test-id="manuscript-assign-academic-editor-filter"
              inline
              onChange={e => setInputValue(e.target.value)}
              onKeyPress={e =>
                e.key === 'Enter' &&
                setSearchValue(e.target.value.toLowerCase())
              }
              placeholder="Search by name or email"
              value={inputValue}
            />
            {searchValue !== '' && (
              <StyledIcon
                data-test-id="clear-filter"
                fontSize="16px"
                icon="remove"
                onClick={clearSearch}
              />
            )}
          </TextContainer>
          {canAssignOneself && (
            <AssignMyselfButton
              academicEditorLabel={academicEditorLabel}
              assignAcademicEditor={assignAcademicEditor}
              currentUser={currentUser}
            />
          )}
        </Row>
        {loading && (
          <Row>
            <Loader mb={2} />
          </Row>
        )}
        {!loading && filteredAcademicEditors.length === 0 && (
          <Row alignItems="baseline" justify="flex-start" pl={2}>
            <Icon color="colorError" icon="warning" pr={1} />
            <Text medium secondary>
              No {academicEditorLabel}s have been found.
            </Text>
          </Row>
        )}
        {!loading && filteredAcademicEditors.length > 0 && (
          <Fragment>
            <Row height={6} pl={4}>
              <Item flex={1}>
                <Label>Full name</Label>
              </Item>
              <Item flex={2}>
                <Label>Email</Label>
              </Item>
              <Item flex={1} justify="flex-end">
                <Label>Workload</Label>
              </Item>
              <Item flex={1} />
            </Row>

            {filteredAcademicEditors.map((academicEditor, index) => (
              <CustomRow
                data-test-id={`manuscript-assign-academic-editor-invite-${academicEditor.id}`}
                height={8}
                isFirst={index === 0}
                isLast={index === filteredAcademicEditors.length - 1}
                key={academicEditor.id}
                pl={4}
              >
                <Fragment>
                  <Item display="inline-grid">
                    <Text ellipsis title={academicEditor.name}>
                      {academicEditor.name}
                    </Text>
                  </Item>
                  <Item flex={2}>
                    <Text>{academicEditor.email}</Text>
                  </Item>
                  <Item flex={1} justify="flex-end">
                    <Text
                      color={
                        academicEditor.workload < 4
                          ? th('actionPrimaryColor')
                          : th('warningColor')
                      }
                      fontWeight={700}
                    >
                      {academicEditor.workload}
                    </Text>
                  </Item>

                  <Item flex={1} justify="flex-end">
                    {assignedEditorId === academicEditor.user.id && (
                      <CustomAssignedButton disabled mr={6} xs>
                        Assigned
                      </CustomAssignedButton>
                    )}
                    {pendingEditorId === academicEditor.user.id && (
                      <Text fontWeight={700} invited mr={6} small upperCase>
                        Pending Response
                      </Text>
                    )}
                    {assignedEditorId !== academicEditor.user.id &&
                      pendingEditorId !== academicEditor.user.id && (
                        <Modal
                          cancelText="CANCEL"
                          component={MultiAction}
                          confirmText={canReassign ? 'Reassign' : 'Invite'}
                          modalKey={`${academicEditor.id}-inviteAcademicEditor`}
                          onConfirm={onInvite(academicEditor)}
                          subtitle={
                            canReassign
                              ? `Are you sure you want to assign as ${academicEditorLabel}?`
                              : academicEditor.name
                          }
                          title={
                            canReassign
                              ? academicEditor.name
                              : 'Confirm Invitation'
                          }
                        >
                          {showModal => (
                            <CustomButton mr={6} onClick={showModal} xs>
                              Invite
                            </CustomButton>
                          )}
                        </Modal>
                      )}
                  </Item>
                </Fragment>
              </CustomRow>
            ))}
          </Fragment>
        )}
      </Root>
    </ContextualBox>
  )
}

export default AssignAcademicEditor

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;

  ${space};
`

const TextContainer = styled.div`
  width: calc(${th('gridUnit')} * 80);
`

const CustomAssignedButton = styled(Button)`
  pointer-events: none;
  :disabled {
    background-color: ${th('actionSecondaryColor')};
    border-color: ${th('actionSecondaryColor')};
    color: ${th('backgroundColor')};
  }
`

const CustomButton = styled(Button)`
  height: calc(${th('gridUnit')} * 6);
  opacity: 0;
  line-height: calc(${th('gridUnit')} * 4);
  background-color: #000;
  border-color: #000;
  color: ${th('white')};

  &:hover {
    background-color: ${th('actionSecondaryColor')};
    border-color: ${th('actionSecondaryColor')};
    color: ${th('backgroundColor')};
  }
`

const CustomRow = styled(Row)`
  border-top: ${props => props.isFirst && th('box.border')};
  border-bottom: ${props => !props.isLast && th('box.border')};

  &:hover {
    background-color: #eee;
    border-bottom-left-radius: ${props => props.isLast && '3px'};
    border-bottom-right-radius: ${props => props.isLast && '3px'};

    ${CustomButton} {
      opacity: 1;
    }
  }
`

const StyledIcon = styled(Icon)`
  position: absolute;
  top: 7px;
  right: 8px;
  color: ${({ disabled }) =>
    disabled ? th('colorBorder') : th('textPrimaryColor')};
  cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};
`

// #endregion
