import React from 'react'
import { Formik } from 'formik'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withHandlers } from 'recompose'
import { Button, H3, TextField, Spinner } from '@pubsweet/ui'

import {
  Row,
  Item,
  Label,
  Text,
  MenuCountry,
  ItemOverrideAlert,
  ValidatedFormField,
  validators,
  withFetching,
  trimFormStringValues,
} from '@hindawi/ui'

const InviteReviewersForm = ({ onInvite, isFetching, fetchingError }) => (
  <Formik onSubmit={onInvite}>
    {({ handleSubmit, resetForm, values }) => (
      <Root>
        <Row justify="space-between" mb={2} mt={5}>
          <H3>Invite Reviewer</H3>
          {fetchingError && (
            <Item justify="flex-end">
              <Text error>An error has occurred on your request!</Text>
            </Item>
          )}
          {isFetching ? (
            <Item justify="flex-end">
              <Spinner />
            </Item>
          ) : (
            <Item justify="flex-end">
              <Button
                data-type-id="button-invite-reviewer-clear"
                ml={4}
                onClick={() => resetForm({})}
                secondary
                small
                width={32}
              >
                CLEAR
              </Button>

              <Button
                data-type-id="button-invite-reviewer-invite"
                ml={4}
                onClick={handleSubmit}
                primary
                small
                width={32}
              >
                SEND
              </Button>
            </Item>
          )}
        </Row>
        <Row>
          <Item mr={4} vertical>
            <Label required>Email</Label>
            <ValidatedFormField
              component={TextField}
              data-test-id="reviewer-email"
              inline
              name="email"
              validate={[validators.required, validators.emailValidator]}
            />
          </Item>

          <Item mr={4} vertical>
            <Label required>First Name</Label>
            <ValidatedFormField
              component={TextField}
              data-test-id="reviewer-givenNames"
              inline
              name="givenNames"
              validate={[validators.required]}
            />
          </Item>

          <Item mr={4} vertical>
            <Label required>Last Name</Label>
            <ValidatedFormField
              component={TextField}
              data-test-id="reviewer-surname"
              inline
              name="surname"
              validate={[validators.required]}
            />
          </Item>
          <Item mr={4} vertical>
            <Label>Affiliation</Label>
            <ValidatedFormField
              component={TextField}
              data-test-id="reviewer-aff"
              inline
              name="aff"
            />
          </Item>

          <ItemOverrideAlert vertical>
            <Label>Country</Label>
            <ValidatedFormField
              component={MenuCountry}
              data-test-id="reviewer-country"
              name="country"
            />
          </ItemOverrideAlert>
        </Row>
      </Root>
    )}
  </Formik>
)

InviteReviewersForm.propTypes = {
  /** Callback function fired after confirming a reviewer invitation.
   * @param {Reviewer} reviewer
   * @param {object} props
   */
  onInvite: PropTypes.func, // eslint-disable-line
}

InviteReviewersForm.defaultProps = {
  onInvite: () => {},
}

export default compose(
  withFetching,
  withHandlers({
    onInvite: ({ onInvite }) => (values, formProps) => {
      onInvite(trimFormStringValues(values), formProps)
    },
  }),
)(InviteReviewersForm)

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue3')};
  padding: 0 calc(${th('gridUnit')} * 4);
  padding-bottom: calc(${th('gridUnit')} * 2);
`
// #endregion
