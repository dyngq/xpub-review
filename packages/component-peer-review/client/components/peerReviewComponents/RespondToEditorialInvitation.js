import React from 'react'
import { Formik } from 'formik'
import { Button } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withModal } from 'component-modal'
import { compose, withHandlers } from 'recompose'

import {
  Item,
  Text,
  Label,
  Textarea,
  validators,
  RadioGroup,
  MultiAction,
  ContextualBox,
  ValidatedFormField,
} from '@hindawi/ui'

const options = [
  { label: 'Agree', value: 'yes' },
  { label: 'Decline', value: 'no' },
]
const RespondToEditorialInvitation = ({
  onSubmit,
  highlight,
  startExpanded,
  academicEditorLabel,
}) => (
  <ContextualBox
    highlight={highlight}
    label="Respond to Editorial Invitation"
    mt={4}
    startExpanded={startExpanded}
  >
    <Formik onSubmit={onSubmit}>
      {({ handleSubmit, values }) => (
        <Root>
          <Item vertical>
            <Label mb={2} required>
              Do you agree to be the {academicEditorLabel} for this manuscript?
            </Label>
            <ValidatedFormField
              component={RadioGroup}
              name="academicEditorDecision"
              options={options}
              validate={[validators.required]}
            />
          </Item>

          {values.academicEditorDecision === 'no' && (
            <Item vertical>
              <Label>
                Decline Reason
                <Text ml={2} secondary>
                  Optional
                </Text>
              </Label>
              <ValidatedFormField component={Textarea} name="reason" />
            </Item>
          )}

          <Item justify="flex-end">
            <Button onClick={handleSubmit} primary width={48}>
              Respond to Invitation
            </Button>
          </Item>
        </Root>
      )}
    </Formik>
  </ContextualBox>
)

export default compose(
  withModal({
    modalKey: 'academic-editor-respond',
    component: MultiAction,
  }),
  withHandlers({
    onSubmit: ({ showModal, onSubmit }) => values => {
      const title =
        values.academicEditorDecision === 'yes'
          ? 'Please confirm your agreement.'
          : 'Decline this invitation?'
      const confirmText =
        values.academicEditorDecision === 'yes' ? 'Agree' : 'Decline'

      showModal({
        title,
        onConfirm: modalProps => onSubmit(values, modalProps),
        confirmText,
      })
    },
  }),
)(RespondToEditorialInvitation)

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-radius: ${th('borderRadius')};
  padding: calc(${th('gridUnit')} * 4);
`
// #endregion
