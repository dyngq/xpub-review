import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { chain, get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { DateParser } from '@pubsweet/ui'
import { File } from 'component-files/client'
import { compose, withProps } from 'recompose'

import { Row, Item, Label, Text } from '@hindawi/ui'

const ReviewerReport = ({
  isCurrentUserReview,
  reviewerNumber,
  recommendation,
  reviewerReport,
  privateReport,
  reviewerName,
  publicReport,
  privateFile,
  reportFile,
  onDownload,
  publicFile,
  onPreview,
  submitted,
  onDelete,
  options,
  ...rest
}) => (
  <Root>
    <Row justify="space-between" mb={6}>
      <Item vertical>
        <Label mb={2}>Recommendation</Label>
        <Text>{recommendation}</Text>
      </Item>
      <Item alignItems="center" justify="flex-end">
        <Text>{reviewerName}</Text>
        {reviewerName.trim() && <Text ml={2}> -</Text>}
        {reviewerNumber && (
          <Text customId ml={4}>
            {`Reviewer ${reviewerNumber}`}
          </Text>
        )}
        {submitted && (
          <DateParser timestamp={submitted}>
            {date => (
              <Text ml={4} secondary>
                | {date}
              </Text>
            )}
          </DateParser>
        )}
      </Item>
    </Row>

    {publicReport && (
      <Row mb={4}>
        <Item vertical>
          {isCurrentUserReview ? (
            <Label mb={2}>Your Report</Label>
          ) : (
            <Label mb={2}>Report</Label>
          )}
          <Text whiteSpace="pre-wrap">{publicReport}</Text>
        </Item>
      </Row>
    )}

    {publicFile && (
      <Fragment>
        <Label mb={2}>Report file</Label>
        <Row justify="flex-start" mb={4}>
          <Item flex={0} mr={2}>
            <File item={publicFile} />
          </Item>
        </Row>
      </Fragment>
    )}

    {privateReport && (
      <Row mb={4}>
        <Item vertical>
          <Text alert mb={2}>
            Confidential note for the Editorial Team
          </Text>
          <Text whiteSpace="pre-wrap">{privateReport}</Text>
        </Item>
      </Row>
    )}
  </Root>
)

export default compose(
  withProps(({ reviewerReport, options }) => ({
    submitted: get(reviewerReport, 'submitted'),
    recommendation: chain(options)
      .find(({ value }) => value === get(reviewerReport, 'recommendation'))
      .get('label')
      .value(),
    reviewerNumber: get(reviewerReport, 'member.reviewerNumber'),
    reviewerName: `${get(
      reviewerReport,
      'member.alias.name.givenNames',
      '',
    )} ${get(reviewerReport, 'member.alias.name.surname', '')}`,
    privateReport: chain(reviewerReport)
      .get('comments', [])
      .find(({ type }) => type === 'private')
      .get('content')
      .value(),
    publicReport: chain(reviewerReport)
      .get('comments', [])
      .find(({ type }) => type === 'public')
      .get('content')
      .value(),
    publicFile: chain(reviewerReport)
      .get('comments', [])
      .find(({ type }) => type === 'public')
      .get('files')
      .first()
      .value(),
    isCurrentUserReview: get(reviewerReport, 'currentUserReview', false),
  })),
)(ReviewerReport)

ReviewerReport.propTypes = {
  reviewerReport: PropTypes.shape({
    /** Unique id for report. */
    id: PropTypes.string,
    /** Comments by reviewers. */
    comments: PropTypes.arrayOf(PropTypes.object),
    /** When the comment was created. */
    created: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    /** When the comment was submited. */
    submitted: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    /** The recommendation given by reviewer. */
    recommendation: PropTypes.string,
    /** The number of reviewer. */
    reviewerNumber: PropTypes.number,
    /** Details about reviewer. */
    member: PropTypes.object,
  }),
}

ReviewerReport.defaultProps = {
  reviewerReport: {},
}

const Root = styled.div`
  background-color: ${th('colorBackgroundHue')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBackgroundHue3')};
  border-radius: ${th('borderRadius')};
  padding-top: calc(${th('gridUnit')} * 4);
  padding-right: calc(${th('gridUnit')} * 4);
  padding-left: calc(${th('gridUnit')} * 4);
  margin: calc(${th('gridUnit')} * 2);
`
