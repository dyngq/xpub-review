import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { orderReviewersByInvitation, sameReviewers } from './utils'

import {
  NoReviewersHint,
  TableHead,
  NameTableCell,
  InvitationDateTableCell,
  ResponseDateTableCell,
  SubmissionDateTableCell,
  InviteActionTableCell,
  StatusCell,
} from './components'
import { HiddenCell } from './styles.js'

const ReviewersTable = ({
  reviewers: unorderedReviewers,
  onCancelReviewerInvitation,
  canCancelReviewerInvitation = true,
}) => {
  if (!unorderedReviewers.length) {
    return <NoReviewersHint />
  }

  const reviewers = orderReviewersByInvitation(unorderedReviewers)

  return (
    <Table>
      <TableHead showExtraColumnHead={canCancelReviewerInvitation} />
      <tbody>
        {reviewers.map(reviewer => (
          <TableRow data-test-id={`reviewer-${reviewer.id}`} key={reviewer.id}>
            <NameTableCell reviewer={reviewer} />
            <InvitationDateTableCell reviewer={reviewer} />
            <ResponseDateTableCell reviewer={reviewer} />
            <SubmissionDateTableCell reviewer={reviewer} />
            <StatusCell reviewer={reviewer} />
            <InviteActionTableCell
              onRevoke={onCancelReviewerInvitation}
              reviewer={reviewer}
              show={canCancelReviewerInvitation}
            />
          </TableRow>
        ))}
      </tbody>
    </Table>
  )
}

ReviewersTable.defaultProps = {
  reviewers: [],
}

export default React.memo(ReviewersTable, sameReviewers)

// #region styles
const Table = styled.table`
  border-collapse: collapse;
  width: 100%;

  & thead {
    border-bottom: 1px solid ${th('colorBorder')};
    background-color: ${th('colorBackgroundHue2')};
  }

  & th {
    height: calc(${th('gridUnit')} * 8);
  }

  & th,
  & td {
    border: none;
    font-family: ${th('defaultFont')};
    min-width: calc(${th('gridUnit')} * 24);
    padding-left: calc(${th('gridUnit')} * 4);
    text-align: start;
    vertical-align: middle;
  }
`

const TableRow = styled.tr`
  background-color: ${th('colorBackgroundHue2')};
  border-bottom: 1px solid ${th('colorBorder')};
  height: calc(${th('gridUnit')} * 8);

  & td:first-child {
    min-width: calc(${th('gridUnit')} * 60);
  }

  &:hover {
    background-color: ${th('colorBackgroundHue3')};

    ${HiddenCell} {
      opacity: 1;
    }
  }
`
// #endregion
