import React from 'react'
import { Item, Row, Text } from '@hindawi/ui'

const NoReviewersHint = () => (
  <Row mb={4} ml={4} mt={4}>
    <Item>
      <Text data-test-id="error-empty-state" emptyState>
        No reviewers invited yet.
      </Text>
    </Item>
  </Row>
)

export default NoReviewersHint
