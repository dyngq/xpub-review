import React from 'react'
import { Label } from '@hindawi/ui'

const TableHead = ({ showExtraColumnHead }) => (
  <thead>
    <tr>
      <th colSpan={5}>
        <Label>Full Name</Label>
      </th>
      <th>
        <Label>Invited on</Label>
      </th>
      <th>
        <Label>Responded on</Label>
      </th>
      <th>
        <Label>Submitted on</Label>
      </th>
      <th>
        <Label>Status</Label>
      </th>
      {showExtraColumnHead && <th>&nbsp;</th>}
    </tr>
  </thead>
)

export default TableHead
