import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export const HiddenCell = styled.td`
  opacity: 0;
  padding-top: calc(${th('gridUnit')} / 2);
`
