import { isEqual, orderBy } from 'lodash'

const orderInvitations = i => {
  switch (i.status) {
    case 'pending':
      return -1
    case 'accepted':
      return 0
    default:
      return 1
  }
}

export const orderReviewersByInvitation = reviewers =>
  orderBy(reviewers, orderInvitations)

export const sameReviewers = ({ reviewers }, { reviewers: nextReviewers }) =>
  isEqual(reviewers, nextReviewers)
