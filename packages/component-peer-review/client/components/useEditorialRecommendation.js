import { useMutation } from 'react-apollo'
import { mutations } from '../graphql'
import { refetchGetSubmission } from '../graphql/refetchQueries'

const useEditorialRecommendation = ({ manuscriptId, match }) => {
  const [requestRevision] = useMutation(mutations.requestRevision, {
    refetchQueries: [refetchGetSubmission(match)],
  })
  const [makeRecommendationToReject] = useMutation(
    mutations.makeRecommendationToReject,
    {
      refetchQueries: [refetchGetSubmission(match)],
    },
  )
  const [makeRecommendationToPublish] = useMutation(
    mutations.makeRecommendationToPublish,
    {
      refetchQueries: [refetchGetSubmission(match)],
    },
  )

  const handleEditorialRecommendation = (values, modalProps) => {
    modalProps.setFetching(true)
    let handleRecommendationFn
    switch (values.recommendation) {
      case 'reject':
        handleRecommendationFn = makeRecommendationToReject({
          variables: {
            manuscriptId,
            input: {
              messageForAuthor: values.public,
              messageForTriage: values.private,
            },
          },
        })
        break
      case 'publish':
        handleRecommendationFn = makeRecommendationToPublish({
          variables: {
            manuscriptId,
            input: {
              messageForAuthor: values.public,
              messageForTriage: values.private,
            },
          },
        })
        break
      case 'minor':
      case 'major':
        handleRecommendationFn = requestRevision({
          variables: {
            manuscriptId,
            content: values.public,
            type: values.recommendation,
          },
        })
        break
      default:
        return null
    }
    handleRecommendationFn
      .then(() => {
        modalProps.setFetching(false)
        modalProps.hideModal()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(e.message)
      })
  }

  return { handleEditorialRecommendation }
}

export default useEditorialRecommendation
