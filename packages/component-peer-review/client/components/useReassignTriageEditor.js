import { useMutation } from 'react-apollo'
import { mutations } from '../graphql/'
import {
  refetchGetAuditLogs,
  refetchGetSubmission,
  refetchGetTriageEditors,
} from '../graphql/refetchQueries'

const useReassignTriageEditor = ({ role, match, manuscript }) => {
  const [reassignTriageEditorMutation] = useMutation(
    mutations.reassignTriageEditor,
    {
      refetchQueries:
        role === 'admin'
          ? [
              refetchGetSubmission(match),
              refetchGetAuditLogs(match),
              refetchGetTriageEditors(match),
            ]
          : [refetchGetSubmission(match), refetchGetTriageEditors(match)],
    },
  )

  const reassignTriageEditor = (triageEditor, modalProps) => {
    modalProps.setFetching(true)
    reassignTriageEditorMutation({
      variables: {
        manuscriptId: manuscript.id,
        teamMemberId: triageEditor.id,
      },
    })
      .then(() => {
        modalProps.setFetching(true)
        modalProps.hideModal()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(e.message)
      })
  }

  return {
    reassignTriageEditor,
  }
}

export default useReassignTriageEditor
