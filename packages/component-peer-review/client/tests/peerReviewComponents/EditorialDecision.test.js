import React from 'react'
import { cleanup, fireEvent } from '@testing-library/react'

import { EditorialDecision } from '../..'
import { render } from '../testUtils'

const toggleMock = jest.fn()
const editorDecisions = [
  'publish',
  'returnToAcademicEditor',
  'revision',
  'reject',
]

describe('Triage Editor Decision', () => {
  afterEach(cleanup)

  it('should not call onSubmit when the message field is empty', () => {
    const onSubmitMock = jest.fn(() => Promise.resolve())

    const { selectOption, getByText } = render(
      <EditorialDecision
        editorDecisions={editorDecisions}
        onSubmit={onSubmitMock}
        toggle={toggleMock}
      />,
    )
    fireEvent.click(getByText('Your Editorial Decision'))
    selectOption('Reject')
    expect(onSubmitMock).toHaveBeenCalledTimes(0)
  })

  it('should call onSubmit with the correct reject decision & message', done => {
    const onSubmitMock = jest.fn(() => Promise.resolve())
    const { getByText, getByTestId, selectOption } = render(
      <EditorialDecision
        editorDecisions={editorDecisions}
        onSubmit={onSubmitMock}
        toggle={toggleMock}
      />,
    )

    fireEvent.click(getByText('Your Editorial Decision'))
    selectOption('Reject')
    fireEvent.change(getByTestId('triage-editor-decision-message'), {
      target: { value: 'bine pa' },
    })
    fireEvent.click(getByText('Submit decision'))

    setTimeout(() => {
      fireEvent.click(getByTestId(/modal-confirm/i))
      expect(onSubmitMock).toHaveBeenCalledTimes(1)
      done()
    })
  })

  it('should call onSubmit with the correct revision decision & message', done => {
    const onSubmitMock = jest.fn(() => Promise.resolve())
    const { getByTestId, selectOption, getByText } = render(
      <EditorialDecision
        editorDecisions={editorDecisions}
        onSubmit={onSubmitMock}
        toggle={toggleMock}
      />,
    )

    fireEvent.click(getByText('Your Editorial Decision'))
    selectOption('Request Revision')
    fireEvent.change(getByTestId('triage-editor-decision-message'), {
      target: { value: 'you can do better' },
    })

    fireEvent.click(getByTestId(/submit-triage-editor-decision/i))

    setTimeout(() => {
      fireEvent.click(getByTestId(/modal-confirm/i))
      expect(onSubmitMock).toHaveBeenCalledTimes(1)
      done()
    })
  })

  it('should call onSubmit with publish decision', done => {
    const onSubmitMock = jest.fn(() => Promise.resolve())
    const { getByTestId, selectOption, getByText } = render(
      <EditorialDecision
        editorDecisions={editorDecisions}
        onSubmit={onSubmitMock}
        toggle={toggleMock}
      />,
    )

    fireEvent.click(getByText('Your Editorial Decision'))
    selectOption('Publish')
    fireEvent.click(getByTestId(/submit-triage-editor-decision/i))

    setTimeout(() => {
      fireEvent.click(getByTestId(/modal-confirm/i))
      expect(onSubmitMock).toHaveBeenCalledTimes(1)
      done()
    })
  })
  it('should call onSubmit with the correct return to academic editor decision & message', done => {
    const onSubmitMock = jest.fn(() => Promise.resolve())
    const { getByTestId, selectOption, getByText } = render(
      <EditorialDecision
        editorDecisions={editorDecisions}
        onSubmit={onSubmitMock}
        toggle={toggleMock}
      />,
    )

    fireEvent.click(getByText('Your Editorial Decision'))
    selectOption('Return to Academic Editor')
    fireEvent.change(getByTestId('triage-editor-decision-message'), {
      target: { value: 'you can do better' },
    })

    fireEvent.click(getByTestId(/submit-triage-editor-decision/i))

    setTimeout(() => {
      fireEvent.click(getByTestId(/modal-confirm/i))
      expect(onSubmitMock).toHaveBeenCalledTimes(1)
      done()
    })
  })
})
