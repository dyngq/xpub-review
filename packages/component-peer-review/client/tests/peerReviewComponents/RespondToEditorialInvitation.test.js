import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent } from '@testing-library/react'

import { render } from '../testUtils'
import { RespondToEditorialInvitation } from '../..'

describe('Respond to editorial invitation', () => {
  afterEach(cleanup)

  it('should expand the box', async done => {
    const onSubmitMock = jest.fn()

    const { getByText } = render(
      <RespondToEditorialInvitation isVisible onSubmit={onSubmitMock} />,
    )
    fireEvent.click(getByText(/respond to editorial invitation/i))
    setTimeout(() => {
      expect(getByText(/respond to invitation/i)).toBeInTheDocument()
      done()
    })
  })

  it('should appear require when an option is not selected', async done => {
    const onSubmitMock = jest.fn()

    const { getByText, getAllByText } = render(
      <RespondToEditorialInvitation isVisible onSubmit={onSubmitMock} />,
    )
    fireEvent.click(getByText(/respond to editorial invitation/i))
    fireEvent.click(getByText(/respond to invitation/i))
    setTimeout(() => {
      expect(getAllByText(/required/i)).toBeTruthy()
      done()
    })
  })

  it('should appear the textarea for reason when selects no', async done => {
    const onSubmitMock = jest.fn()

    const { getByText } = render(
      <RespondToEditorialInvitation isVisible onSubmit={onSubmitMock} />,
    )
    fireEvent.click(getByText(/respond to editorial invitation/i))
    fireEvent.click(document.querySelector('input[value=no]'))
    setTimeout(() => {
      expect(getByText(/decline reason/i)).toBeInTheDocument()
      done()
    })
  })

  it('should all work good when accepts the invitation', async done => {
    const onSubmitMock = jest.fn()

    const { getByText, getByTestId } = render(
      <RespondToEditorialInvitation isVisible onSubmit={onSubmitMock} />,
    )

    fireEvent.click(getByText(/respond to editorial invitation/i))
    fireEvent.click(document.querySelector('input[value=yes]'))
    fireEvent.click(getByText(/respond to invitation/i))

    setTimeout(() => {
      fireEvent.click(getByTestId(/modal-confirm/i))
      expect(onSubmitMock).toHaveBeenCalledTimes(1)
      done()
    })
  })
})
