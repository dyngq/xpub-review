import React from 'react'
import { theme } from '@hindawi/ui'
import { ModalProvider } from 'component-modal'
import { ThemeProvider } from 'styled-components'
import { fireEvent, render as rtlRender, wait } from '@testing-library/react'
import { JournalProvider as HindawiContextProvider } from 'component-journal-info'
import '@testing-library/jest-dom/extend-expect'
import { MockedProvider } from '@apollo/react-testing'

import { publisher, journal } from './mockData'

export const render = ui => {
  const Component = () => (
    <HindawiContextProvider journal={journal} publisher={publisher}>
      <MockedProvider>
        <ModalProvider>
          <div id="ps-modal-root" />
          <ThemeProvider theme={theme}>{ui}</ThemeProvider>
        </ModalProvider>
      </MockedProvider>
    </HindawiContextProvider>
  )

  const utils = rtlRender(<Component />)
  return {
    ...utils,
    selectOption: value => {
      fireEvent.click(utils.container.querySelector(`button[type=button]`))
      wait(() => utils.getByText(value))
      fireEvent.click(utils.getByText(value))
    },
  }
}
