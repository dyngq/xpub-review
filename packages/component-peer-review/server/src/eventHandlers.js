const models = require('@pubsweet/models')
const {
  getSuggestedAcademicEditorUseCase,
} = require('component-model/src/useCases')

const events = require('component-events')
const {
  inviteAcademicEditorUseCase,
} = require('component-peer-review/server/src/useCases/inviteAcademicEditor')

const {
  initializeInviteAcademicEditorUseCase,
} = require('./useCases/inviteAcademicEditor/initializeInviteAcademicEditorUseCase')

const {
  ingestEditorSuggestionsUseCase,
  inviteSuggestedAcademicEditorUseCase,
} = require('./useCases')

module.exports = {
  async EditorSuggestionListGenerated(data) {
    const eventsService = events.initialize({ models })

    await ingestEditorSuggestionsUseCase
      .initialize({
        models,
        eventsService,
      })
      .execute(data)
  },

  async SubmissionEditorSuggestionsIngested(data) {
    await inviteSuggestedAcademicEditorHandler(data)
  },

  async SubmissionAcademicEditorDeclined(data) {
    await inviteSuggestedAcademicEditorHandler(data)
  },

  async SubmissionAcademicEditorInvitationExpired(data) {
    await inviteSuggestedAcademicEditorHandler(data)
  },
}

async function inviteSuggestedAcademicEditorHandler({ submissionId }) {
  const getSuggestedAcademicEditor = getSuggestedAcademicEditorUseCase.initialize(
    {
      models,
    },
  )

  return inviteSuggestedAcademicEditorUseCase
    .initialize({
      models,
      useCases: {
        getSuggestedAcademicEditor,
        inviteAcademicEditor: initializeInviteAcademicEditorUseCase(
          inviteAcademicEditorUseCase,
        ),
      },
    })
    .execute(submissionId)
}
