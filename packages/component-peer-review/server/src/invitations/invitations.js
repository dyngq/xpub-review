const config = require('config')

const { getExpectedDate } = require('component-date-service')

const {
  getEmailCopy: getAcademicEditorEmailCopy,
} = require('./academicEditor/emailCopy')
const { getEmailCopy: getReviewerEmailCopy } = require('./reviewer/emailCopy')

const {
  getInvitationEmailProps: getAcademicEditorInvitationProps,
} = require('./academicEditor/getInvitationEmailProps')
const {
  getInvitationEmailProps: getReviewerInvitationProps,
} = require('./reviewer/getInvitationEmailProps')

const baseUrl = config.get('pubsweet-client.baseUrl')

const initialize = ({ Email }) => ({
  async sendInvitationToAcademicEditor({
    journal,
    manuscript,
    triageEditor,
    academicEditor,
    submittingAuthor,
    authorTeamMembers,
    editorialAssistant,
    academicEditorLabel,
  }) {
    const { customId, title } = manuscript
    const emailProps = getAcademicEditorInvitationProps({
      journal,
      manuscript,
      triageEditor,
      academicEditor,
      editorialAssistant,
      subject: `${customId}: Invitation to edit a manuscript`,
      authorTeamMembers,
    })
    const targetUserName = triageEditor
      ? triageEditor.getName()
      : editorialAssistant.getName()
    const { paragraph, ...bodyProps } = getAcademicEditorEmailCopy({
      baseUrl,
      journal,
      targetUserName,
      academicEditorLabel,
      manuscriptTitle: title,
      emailType: 'academic-editor-assigned',
      authorName: submittingAuthor.getName(),
    })
    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)

    return email.sendEmail()
  },
  async sendInvitationToReviewerAfterMajorRevision({
    journal,
    authors,
    manuscript,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
    newVersionReviewers,
  }) {
    const submittingAuthorName = submittingAuthor.getName()
    const { name: journalName } = journal
    const { title, customId } = manuscript

    newVersionReviewers.forEach(reviewerMember => {
      const emailProps = getReviewerInvitationProps({
        manuscript,
        authors,
        journalName,
        academicEditor,
        editorialAssistant,
        reviewer: reviewerMember,
      })

      const { paragraph, ...bodyProps } = getReviewerEmailCopy({
        emailType: 'reviewer-invitation-after-revision',
        titleText: `the manuscript titled <strong>"${title}"</strong> by <strong>${submittingAuthorName}</strong> et al.`,
        expectedDate: getExpectedDate({
          timestamp: new Date(),
          daysExpected: 14,
        }),
      })

      emailProps.bodyProps = bodyProps
      emailProps.content.subject = `${customId}: Review invitation: New Version`

      const email = new Email(emailProps)

      return email.sendEmail()
    })
  },
  async sendInvitationToReviewer({
    journal,
    reviewer,
    academicEditor,
    authors,
    manuscript,
    editorialAssistant,
    academicEditorLabel,
    submittingAuthorName,
  }) {
    const { name: journalName } = journal

    const emailProps = getReviewerInvitationProps({
      reviewer,
      manuscript,
      authors,
      academicEditor,
      journalName,
      editorialAssistant,
    })

    const { title } = manuscript

    const emailType = 'reviewer-invitation'
    const titleText = `A manuscript titled <strong>"${title}"</strong> by <strong>${submittingAuthorName}</strong> et al.`

    const { paragraph, ...bodyProps } = getReviewerEmailCopy({
      emailType,
      titleText,
      journalName,
      academicEditorLabel,
      expectedDate: getExpectedDate({
        timestamp: new Date(),
        daysExpected: 14,
      }),
    })
    emailProps.bodyProps = bodyProps

    const email = new Email(emailProps)

    await email.sendEmail()
  },
})
module.exports = { initialize }
