const initialize = () => ({
  getCopy({
    comments,
    titleText,
    emailType,
    journalName,
    targetUserName,
    approvalEditorName,
    academicEditorLabel,
    editorialAssistantEmail,
  }) {
    let paragraph
    let hasLink = true
    let hasIntro = true
    let hasSignature = true
    switch (emailType) {
      case 'EQA-manuscript-published':
        paragraph = `${titleText} has been accepted for publication by ${approvalEditorName}.<br/><br/>
      Please complete QA screening so that manuscript can be sent to production.<br/><br/>
      To review this decision, please visit the manuscript details page <br/><br/>`
        break
      case 'authors-manuscript-rejected-no-review':
        hasLink = false
        paragraph = `I regret to inform you that your manuscript has been rejected for publication in ${journalName} for the following reason:<br/><br/>
          ${comments}<br/><br/>
          Thank you for your submission, and please do consider submitting again in the future.`
        break
      case 'reviewers-manuscript-rejected':
        paragraph = `Thank you for your review of ${titleText} for ${journalName}. After taking into account the reviews and the recommendation of the ${academicEditorLabel}, I can confirm this article has now been rejected. <br/><br/>
        No further action is required at this time. To see more details about this decision please view the manuscript details page. <br/><br/>
        If you have any questions about this decision, please email them to ${editorialAssistantEmail} as soon as possible. Thank you for reviewing for ${journalName}.<br/><br/>`
        break
      case 'authors-manuscript-rejected-after-review':
        hasLink = false
        paragraph = `The peer review of your manuscript titled "${titleText}" has now been completed.<br/><br/>
      Please find our editorial comments below.<br/><br/>
      ${comments}<br/><br/>
      Thank you for your submission to ${journalName}.<br/><br/>`
        break
      case 'academic-editor-manuscript-rejected':
        hasIntro = false
        paragraph = `${targetUserName} has confirmed your decision to reject ${titleText}.<br/><br/>
      No further action is required at this time. To review this decision, please visit the manuscript details page.<br/><br/>
      Thank you for handling this manuscript on behalf of ${journalName}.`
        break
      case 'academic-editor-manuscript-returned':
        hasIntro = false
        paragraph = `${targetUserName} has responded with comments regarding your editorial recommendation on ${titleText}.<br/><br/>
      ${comments}<br/><br/>
      Please review these comments and take action on the manuscript details page. <br/><br/>`
        break
      case 'triage-editor-manuscript-rejected':
        hasIntro = false
        hasSignature = false
        paragraph = `Dr. ${targetUserName} has rejected ${titleText}.<br/><br/>
          No action is required at this time. To see the requested changes, please visit the manuscript details page.`
        break
      case 'triage-editor-contribution':
        paragraph = `Thank you for submitting your editorial decision on ${titleText} <br/><br/>
        On behalf of the ${journalName} editorial team, thank you for your continued support.`
        break
      case 'academic-editor-contribution':
        paragraph = `Thank you for submitting your editorial decision on ${titleText} <br/><br/>
         On behalf of the ${journalName} editorial team, thank you for your continued support.`
        break
      default:
        throw new Error(`The ${emailType} email type is not defined.`)
    }
    return { paragraph, hasLink, hasIntro, hasSignature }
  },
})

module.exports = {
  initialize,
}
