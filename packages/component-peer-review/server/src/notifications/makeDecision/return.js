const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifyAcademicEditor({
    journal,
    manuscript,
    approvalEditor,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
    comment: { content },
  }) {
    const { name: journalName } = journal
    const { customId, title } = manuscript
    const submittingAuthorName = submittingAuthor.getName()

    const approvalEditorName = approvalEditor.getName()
    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
      emailType: 'academic-editor-manuscript-returned',
      comments: content,
      targetUserName: approvalEditor.getName(),
      journalName,
    })
    const emailProps = getPropsService.getProps({
      manuscript,
      toUser: academicEditor,
      fromEmail: `${approvalEditorName} <${editorialAssistant.alias.email}>`,
      subject: `${customId}: Editorial decision returned with comments`,
      paragraph,
      signatureName: approvalEditorName,
    })
    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = { initialize }
