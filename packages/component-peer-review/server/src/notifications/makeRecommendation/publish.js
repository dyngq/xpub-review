const { get } = require('lodash')

const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifyTriageEditor({
    journal,
    manuscript,
    triageEditor,
    staffMember,
    academicEditor,
    submittingAuthor,
    messageForTriage,
    academicEditorLabel,
  }) {
    const { name: journalName } = journal
    const editorName = academicEditor.getName()
    const comments = messageForTriage
      ? `The ${academicEditorLabel} provided the following comments: "${messageForTriage}"`
      : ''
    const staffMemberEmail = get(staffMember, 'alias.email')

    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      comments,
      journalName,
      emailType: 'triage-editor-recommend-to-publish',
      titleText: `the manuscript titled "${
        manuscript.title
      }" by ${submittingAuthor.getName()} `,
      targetUserName: academicEditor.getLastName(),
    })

    const emailProps = getPropsService.getProps({
      manuscript,
      toUser: triageEditor,
      paragraph,
      subject: `${manuscript.customId}: Recommendation to publish`,
      fromEmail: `${editorName} <${staffMemberEmail}>`,
      signatureName: editorName,
      journalName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
  async notifyReviewers({
    journal,
    manuscript,
    staffMember,
    academicEditor,
    submittingAuthor,
    pendingReviewers,
    acceptedReviewers,
  }) {
    const { name: journalName } = journal
    const titleText = `the manuscript titled "${
      manuscript.title
    }" by ${submittingAuthor.getName()}`

    const staffEmail = get(staffMember, 'alias.email')
    const editorName = academicEditor.getName()

    const acceptedReviewersEmailBody = getEmailCopyService.getCopy({
      emailType: 'accepted-reviewers-after-recommendation',
      titleText,
      journalName,
      staffEmail,
    })

    const pendingReviewersEmailBody = getEmailCopyService.getCopy({
      emailType: 'pending-reviewers-after-recommendation',
      titleText,
      journalName,
      staffEmail,
    })

    const buildSendEmailFunction = emailBodyProps => async reviewer => {
      const { paragraph, ...bodyProps } = emailBodyProps
      const emailProps = getPropsService.getProps({
        manuscript,
        toUser: reviewer,
        fromEmail: `${journalName} <${staffEmail}>`,
        subject: `${manuscript.customId}: Review no longer required`,
        paragraph,
        signatureName: editorName,
        journalName,
      })

      emailProps.bodyProps = bodyProps
      const email = new Email(emailProps)
      await email.sendEmail()
    }

    return Promise.all([
      ...acceptedReviewers.map(
        buildSendEmailFunction(acceptedReviewersEmailBody),
      ),
      ...pendingReviewers.map(
        buildSendEmailFunction(pendingReviewersEmailBody),
      ),
    ])
  },
})

module.exports = {
  initialize,
}
