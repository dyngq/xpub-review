const initialize = () => ({
  getCopy({
    comments,
    titleText,
    emailType,
    journalName,
    targetUserName,
    editorialAssistantEmail,
  }) {
    let paragraph
    let hasLink = true
    let hasIntro = true
    let hasSignature = true
    switch (emailType) {
      case 'submitting-author':
        paragraph = `In order for ${titleText} to proceed to the review process, there needs to be a revision. <br/><br/>
        ${comments}<br/><br/>
        For more information about what is required, please click the link below.<br/><br/>`
        break
      case 'reviewers':
        hasLink = false
        paragraph = `I appreciate any time you may have spent reviewing ${titleText}. However, I am prepared to make an editorial decision and your review is no longer required at this time. I apologize for any inconvenience. <br/><br/>
        If you have comments on this manuscript you believe I should see, please email them to ${editorialAssistantEmail} as soon as possible. <br/><br/>
        Thank you for your interest and I hope you will consider reviewing for ${journalName} again.`
        break
      case 'triage-editor':
        hasIntro = false
        hasSignature = false
        paragraph = `Dr. ${targetUserName} has asked the authors to submit a revised version of ${titleText}.<br/><br/>
        No action is required at this time. To see the requested changes, please visit the manuscript details page.`
        break
      default:
        throw new Error(`The ${emailType} email type is not defined.`)
    }
    return { paragraph, hasLink, hasIntro, hasSignature }
  },
})

module.exports = {
  initialize,
}
