const { get } = require('lodash')

const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifyAcademicEditor({
    draft,
    journal,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { name: journalName } = journal

    const titleText = get(draft, 'title', '')
    const targetUserName = submittingAuthor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      titleText,
      journalName,
      emailType: 'academic-editor',
      targetUserName,
    })

    const emailProps = getPropsService.getProps({
      manuscript: draft,
      subject: `${draft.customId}: Revision submitted`,
      toUser: academicEditor,
      paragraph,
      fromEmail: `${journalName} <${editorialAssistant.alias.email}>`,
      signatureName: editorialAssistant.getName(),
      journalName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
  async notifyTriageEditor({
    draft,
    journal,
    triageEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { name: journalName } = journal

    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      titleText: get(draft, 'title', ''),
      emailType: 'triage-editor',
      targetUserName: submittingAuthor.getName(),
      journalName,
    })
    const emailProps = getPropsService.getProps({
      manuscript: draft,
      toUser: triageEditor,
      fromEmail: `${journalName} <${editorialAssistant.alias.email}>`,
      subject: `${draft.customId}: Revision submitted`,
      signatureName: editorialAssistant.getName(),
      paragraph,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = { initialize }
