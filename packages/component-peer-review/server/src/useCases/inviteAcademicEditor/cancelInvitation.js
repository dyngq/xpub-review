const initialize = ({
  logEvent,
  jobsService,
  eventsService,
  notificationService,
  models: { TeamMember, Journal, Manuscript, Team, Job, User, PeerReviewModel },
}) => ({
  async execute({ teamMemberId, userId }) {
    const academicEditorMember = await TeamMember.find(teamMemberId)
    const manuscript = await Manuscript.findManuscriptByTeamMember(teamMemberId)

    // Throw early
    if (academicEditorMember.status !== TeamMember.Statuses.pending) {
      throw new ValidationError('User already responded to the invitation.')
    }
    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    // Jobs
    const academicEditorMemberJobs = await Job.findAllByTeamMember(
      academicEditorMember.id,
    )
    await jobsService.cancelJobs(academicEditorMemberJobs)

    // Academic Editor
    const submissionAcademicEditorTeams = await Team.findAllBy({
      role: Team.Role.academicEditor,
      submissionId: manuscript.submissionId,
    })
    await Promise.all(
      submissionAcademicEditorTeams.map(async team => {
        await team.delete()
      }),
    )

    // Manuscript
    manuscript.updateStatus(Manuscript.Statuses.submitted)
    await manuscript.save()

    // Notifications
    const journal = await Journal.find(manuscript.journalId, 'peerReviewModel')
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId: manuscript.id,
      sectionId: manuscript.sectionId,
    })

    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )
    academicEditorMember.user = await User.find(academicEditorMember.userId)
    const academicEditorLabel = await manuscript.getEditorLabel({
      Team,
      TeamMember,
      PeerReviewModel,
      role: Team.Role.academicEditor,
    })

    notificationService.notifyAcademicEditor({
      journal,
      manuscript,
      triageEditor,
      submittingAuthor,
      editorialAssistant,
      academicEditorLabel,
      academicEditor: academicEditorMember,
    })

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionAcademicEditorCancelled',
    })

    // Activity Log
    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.invitation_revoked,
      objectType: logEvent.objectType.user,
      objectId: academicEditorMember.userId,
    })
  },
})

const authsomePolicies = [
  'isAuthenticated',
  'isTriageEditor',
  'isEditorialAssistant',
  'admin',
]

module.exports = {
  initialize,
  authsomePolicies,
}
