const { Promise } = require('bluebird')

function initialize({
  models: { Manuscript, EditorSuggestion, TeamMember },
  eventsService,
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, submissionId, editorSuggestions }) {
    const isSuggestionsListInSyncWithDB = await TeamMember.isSuggestionsListInSyncWithTeamMembers(
      editorSuggestions,
    )

    if (!isSuggestionsListInSyncWithDB) {
      return
    }

    const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
    })
    if (!Manuscript.InProgressStatuses.includes(lastManuscript.status)) {
      return
    }

    const suggestions = await Promise.map(
      editorSuggestions,
      async ({ editorId, score }) => {
        const suggestionAlreadySavedInDB = await EditorSuggestion.findOneBy({
          queryObject: {
            teamMemberId: editorId,
            manuscriptId,
          },
        })

        if (suggestionAlreadySavedInDB) return suggestionAlreadySavedInDB

        return new EditorSuggestion({
          manuscriptId,
          teamMemberId: editorId,
          score,
        })
      },
    )
    await EditorSuggestion.saveAll(suggestions)

    eventsService.publishSubmissionEvent({
      submissionId,
      eventName: 'SubmissionEditorSuggestionsIngested',
    })
  }
}

module.exports = { initialize }
