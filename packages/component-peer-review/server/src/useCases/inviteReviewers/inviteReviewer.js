const uuid = require('uuid')
const config = require('config')

const removalDays = config.get('reminders.reviewer.acceptInvitation.remove')
const timeUnit = config.get('reminders.reviewer.acceptInvitation.timeUnit')

const initialize = ({
  models: {
    Job,
    Team,
    User,
    Journal,
    Identity,
    TeamMember,
    Manuscript,
    PeerReviewModel,
    ReviewerSuggestion,
  },
  logEvent,
  jobsService,
  eventsService,
  emailJobsService,
  invitationsService,
  removalJobsService,
}) => ({
  async execute({ manuscriptId, input, userId }) {
    const manuscript = await Manuscript.find(manuscriptId)

    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    const userIdentity = await Identity.findOneByEmail(input.email)
    let user = userIdentity ? await User.find(userIdentity.userId) : undefined
    let identity = userIdentity

    // Don't allow editors as reviewers on special issues
    if (user && manuscript.specialIssueId) {
      const specialIssueEditorRoles = await TeamMember.findAllBySpecialIssueAndUserAndRoles(
        {
          userId: user.id,
          specialIssueId: manuscript.specialIssueId,
          roles: [Team.Role.triageEditor, Team.Role.academicEditor],
        },
      )
      if (specialIssueEditorRoles.length)
        throw new Error(
          'This user cannot be invited because they are already part of the Special Issue team.',
        )
    }
    if (!user) {
      user = new User({
        isActive: true,
        agreeTc: true,
        isSubscribedToEmails: true,
        unsubscribeToken: uuid.v4(),
        confirmationToken: uuid.v4(),
        defaultIdentityType: 'local',
        passwordResetToken: uuid.v4(),
      })
      await user.save()

      identity = new Identity({
        type: 'local',
        isConfirmed: false,
        passwordHash: null,
        givenNames: input.givenNames,
        surname: input.surname,
        email: input.email,
        aff: input.aff || '',
        userId: user.id,
        country: input.country || '',
      })
      await identity.save()
      user.assignIdentity(identity)
    }

    let manuscriptReviewersTeam = await Team.findOneBy({
      queryObject: {
        manuscriptId: manuscript.id,
        role: Team.Role.reviewer,
      },
    })

    const reviewers = await TeamMember.findAllByManuscriptAndRoleAndExcludedStatuses(
      {
        role: Team.Role.reviewer,
        manuscriptId,
        excludedStatuses: [
          TeamMember.Statuses.expired,
          TeamMember.Statuses.removed,
        ],
      },
    )

    let teamMember
    if (!manuscriptReviewersTeam) {
      manuscriptReviewersTeam = new Team({
        manuscriptId,
        role: Team.Role.reviewer,
      })
      manuscript.updateStatus(Manuscript.Statuses.reviewersInvited)
      await manuscriptReviewersTeam.save()
      await manuscript.save()
    } else {
      if (reviewers.length === 0) {
        manuscript.updateStatus(Manuscript.Statuses.reviewersInvited)
        await manuscript.save()
      }
      teamMember = await TeamMember.findOneByManuscriptAndRoleAndUser({
        manuscriptId,
        userId: user.id,
        role: Team.Role.reviewer,
      })
    }
    if (teamMember && teamMember.status !== TeamMember.Statuses.expired) {
      throw new ValidationError(
        `User ${identity.email} is already invited as ${Team.Role.reviewer}`,
      )
    }
    let reviewerMembers = []

    if (teamMember && teamMember.status === TeamMember.Statuses.expired) {
      teamMember.updateProperties({ status: TeamMember.Statuses.pending })
      await teamMember.save()
    } else {
      reviewerMembers = await TeamMember.findAllByManuscriptAndRole({
        role: Team.Role.reviewer,
        manuscriptId,
      })
      teamMember = new TeamMember({
        alias: input,
        userId: user.id,
        teamId: manuscriptReviewersTeam.id,
        position: reviewerMembers.length + 1,
      })
      teamMember.user = user
      await teamMember.save()
    }

    const reviewerSuggestion = await ReviewerSuggestion.findOneBy({
      queryObject: {
        email: input.email,
        manuscriptId,
      },
    })
    if (reviewerSuggestion) {
      reviewerSuggestion.isInvited = true
      await reviewerSuggestion.save()
    }

    let staffMember = await TeamMember.findOneByManuscriptAndRoleAndStatus({
      manuscriptId,
      role: Team.Role.editorialAssistant,
      status: TeamMember.Statuses.active,
    })
    if (!staffMember) {
      staffMember = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      })
    }

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
      },
    )
    const academicEditorJobs = await Job.findAllByTeamMember(academicEditor.id)
    const staffMemberJobs = await Job.findAllByTeamMember(staffMember.id)
    teamMember.user = user
    user.identities = userIdentity ? [userIdentity] : [identity]

    if (reviewerMembers.length + 1 >= config.minimumNumberOfInvitedReviewers) {
      jobsService.cancelJobs(academicEditorJobs)
      jobsService.cancelStaffMemberJobs({
        staffMemberJobs,
        manuscriptId,
      })
    }

    const journal = await Journal.find(manuscript.journalId)
    const submittingAuthor = await TeamMember.findSubmittingAuthor(manuscriptId)
    const authors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.author,
    })

    const academicEditorLabel = await manuscript.getEditorLabel({
      Team,
      TeamMember,
      PeerReviewModel,
      role: Team.Role.academicEditor,
    })

    invitationsService.sendInvitationToReviewer({
      authors,
      journal,
      manuscript,
      academicEditor,
      academicEditorLabel,
      reviewer: teamMember,
      editorialAssistant: staffMember,
      submittingAuthorName: submittingAuthor.getName(),
    })

    await emailJobsService.scheduleEmailsWhenReviewerIsInvited({
      journal,
      manuscript,
      authors,
      academicEditor,
      reviewer: teamMember,
      editorialAssistant: staffMember,
      submittingAuthorName: submittingAuthor.getName(),
    })

    await removalJobsService.scheduleRemovalJob({
      timeUnit,
      days: removalDays,
      invitationId: teamMember.id,
      manuscriptId: manuscript.id,
    })

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionReviewerInvited',
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.reviewer_invited,
      objectType: logEvent.objectType.user,
      objectId: teamMember.user.id,
    })
  },
})

const authsomePolicies = [
  'isAuthenticated',
  'isAcademicEditorOnManuscript',
  'isEditorialAssistant',
  'admin',
]

module.exports = {
  initialize,
  authsomePolicies,
}
