const { Promise } = require('bluebird')

module.exports = {
  async uploadToMTS({
    manuscriptId,
    models: { Manuscript },
    sendPackage,
    isRejected = false,
  }) {
    const manuscript = await Manuscript.find(manuscriptId)

    try {
      const manuscriptVersions = await Manuscript.findManuscriptsBySubmissionId(
        {
          submissionId: manuscript.submissionId,
          excludedStatus: Manuscript.Statuses.deleted,
          eagerLoadRelations: `[
            files,
            section, 
            articleType,
            teams.members.user.identities,
            specialIssue.section,
            journal.[peerReviewModel,teams.members.user.identities,preprints],
            reviews.[
              member.team
              comments.files,
            ]
          ]`,
        },
      )

      await Promise.each(manuscriptVersions, async manuscript =>
        sendPackage({
          manuscript,
          isEQA: true,
          isManuscriptRejected: isRejected,
        }),
      )
    } catch (e) {
      throw new Error(e)
    }
  },
}
