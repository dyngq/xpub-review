const requestRevisionUseCase = require('./requestRevision')
const requestMinorOrMajorUseCase = require('./requestMinorOrMajor')
const cancelAllJobsUseCase = require('./cancelAllJobs')

module.exports = {
  cancelAllJobsUseCase,
  requestRevisionUseCase,
  requestMinorOrMajorUseCase,
}
