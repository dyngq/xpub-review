const minorRevisionUseCase = require('./minor')
const majorRevisionUseCase = require('./major')
const submitRevisionUseCase = require('./submit')
const revisionRevisionUseCase = require('./regular')
const updateDraftRevisionUseCase = require('./updateDraft')
const noReviewersRevisionUseCase = require('./noReviewers')

module.exports = {
  minorRevisionUseCase,
  majorRevisionUseCase,
  submitRevisionUseCase,
  revisionRevisionUseCase,
  updateDraftRevisionUseCase,
  noReviewersRevisionUseCase,
}
