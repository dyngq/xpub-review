const initialize = ({ notificationService, eventsService }) => ({
  async execute({
    draft,
    journal,
    academicEditor,
    submittingAuthor,
    ManuscriptStatuses,
    editorialAssistant,
  }) {
    draft.updateStatus(ManuscriptStatuses.academicEditorAssigned)
    await draft.save()

    notificationService.notifyAcademicEditor({
      draft,
      journal,
      academicEditor,
      submittingAuthor,
      editorialAssistant,
    })
    eventsService.publishSubmissionEvent({
      submissionId: draft.submissionId,
      eventName: 'SubmissionRevisionSubmitted',
    })
  },
})

module.exports = { initialize }
