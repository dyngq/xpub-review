const models = require('@pubsweet/models')
const useCases = require('./')

const resolver = {
  Mutation: {
    async updatePreprintValue(_, { manuscriptId, preprintValue }) {
      return useCases.updatePreprintValueUseCase
        .initialize({ models })
        .execute({ manuscriptId, preprintValue })
    },
  },
}

module.exports = resolver
