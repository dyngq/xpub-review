const initialize = ({ models }) => ({
  async execute({ manuscriptId, preprintValue }) {
    const { Manuscript } = models

    const manuscript = await Manuscript.find(manuscriptId)
    manuscript.preprintValue = preprintValue
    await manuscript.save()
  },
})

const authsomePolicies = [
  'isAuthorOnManuscript',
  'isEditorialAssistant',
  'admin',
]

module.exports = {
  initialize,
  authsomePolicies,
}
