const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')
const Chance = require('chance')

const chance = new Chance()

const { removeAcademicEditorUseCase } = require('../../src/useCases')

const jobsService = {
  cancelJobs: jest.fn(),
  cancelStaffMemberJobs: jest.fn(),
}

const notificationService = {
  notifyAuthor: jest.fn(),
  notifyReviewers: jest.fn(),
  notifyAcademicEditor: jest.fn(),
}
const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  academic_editor_removed: 'academic_editor_removed',
}
logEvent.objectType = { user: 'user' }

describe('Revoke academic editor', () => {
  it('removes the academic editor from the manuscript', async () => {
    const {
      Team,
      Journal,
      Manuscript,
      TeamMember,
      PeerReviewModel,
      Review,
      Comment,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.admin,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.academicEditorAssigned,
      },
      Manuscript,
    })

    const academicEditor = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.accepted },
      role: Team.Role.academicEditor,
    })

    const review = fixtures.generateReview({
      properties: {
        teamMemberId: academicEditor.id,
        manuscriptId: manuscript.id,
        recommendation: 'minor',
      },
      Review,
    })
    const comment = fixtures.generateComment({
      properties: {
        reviewId: review.id,
        type: 'public',
        content: chance.sentence(),
        files: [],
      },
      Comment,
    })
    review.comments = [comment]

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        status: TeamMember.Statuses.pending,
        isSubmitting: true,
      },
      role: Team.Role.author,
    })

    await removeAcademicEditorUseCase
      .initialize({
        notificationService,
        models,
        logEvent,
        jobsService,
      })
      .execute({ teamMemberId: academicEditor.id })

    expect(notificationService.notifyAuthor).toHaveBeenCalledTimes(1)
    expect(notificationService.notifyAcademicEditor).toHaveBeenCalledTimes(1)
    expect(notificationService.notifyReviewers).toHaveBeenCalledTimes(1)
    expect(manuscript.status).toEqual(Manuscript.Statuses.submitted)
  })
})
