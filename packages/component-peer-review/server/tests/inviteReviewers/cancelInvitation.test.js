const {
  getTeamRoles,
  generateManuscript,
  generateTeamMember,
  getManuscriptStatuses,
  getTeamMemberStatuses,
} = require('component-generators')

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  invitation_revoked: 'revoked invitation sent to',
}
logEvent.objectType = { user: 'user' }

const { cancelReviewerInvitationUseCase } = require('../../src/useCases')

const notificationService = {
  notifyReviewer: jest.fn(),
}
const jobsService = {
  cancelJobs: jest.fn(),
}
const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

describe('Cancel reviewer invitation', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('deletes the only reviewer and the team', async () => {
    const {
      PeerReviewModel,
      Journal,
      Team,
      TeamMember,
      Manuscript,
      User,
      Identity,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })
    const reviewer = fixtures.generateUser({ User, Identity })
    const identity = fixtures.generateIdentity({
      properties: { userId: reviewer.id },
      Identity,
    })
    reviewer.identities = [identity]

    const manuscript = fixtures.generateManuscript({
      properties: { journalId: journal.id },
      Manuscript,
    })
    const manuscriptTeam = fixtures.generateTeam({
      properties: { role: Team.Role.reviewer, manuscriptId: manuscript.id },
      Team,
    })
    manuscript.teams.push(manuscriptTeam)
    manuscriptTeam.manuscript = manuscript

    const teamMember = fixtures.generateTeamMember({
      properties: {
        userId: reviewer.id,
        teamId: manuscriptTeam.id,
        status: TeamMember.Statuses.pending,
      },
      TeamMember,
    })
    teamMember.user = reviewer
    teamMember.team = manuscriptTeam
    teamMember.setAlias(reviewer.defaultIdentity)

    manuscriptTeam.members.push(teamMember)

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.pending, isSubmitting: true },
      role: Team.Role.author,
    })

    const academicEditorMember = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.accepted },
      role: Team.Role.academicEditor,
    })

    await cancelReviewerInvitationUseCase
      .initialize({
        models,
        logEvent,
        jobsService,
        eventsService,
        notificationService,
      })
      .execute({
        manuscriptId: manuscript.id,
        teamMemberId: teamMember.id,
        userId: academicEditorMember.userId,
      })

    const reviewerTeam = fixtures.teams.find(
      team => team.role === Team.Role.reviewer,
    )

    expect(notificationService.notifyReviewer).toHaveBeenCalledTimes(1)
    expect(reviewerTeam).toBeUndefined()
  })

  it('deletes the reviewer but leaves the team when there is another removed reviewer', async () => {
    const models = {
      Job: {
        findAllByTeamMember: jest.fn(),
      },
      Journal: {
        find: jest.fn(),
      },
      Manuscript: {
        findManuscriptByTeamMember: jest.fn(),
        Statuses: getManuscriptStatuses(),
      },
      ReviewerSuggestion: {
        findOneBy: jest.fn(),
      },
      TeamMember: {
        find: jest.fn(),
        Statuses: getTeamMemberStatuses(),
        findAllByStatuses: jest.fn(),
        findSubmittingAuthor: jest.fn(),
        findOneByManuscriptAndRoleAndStatus: jest.fn(),
      },
      User: {
        find: jest.fn(),
      },
      Team: {
        find: jest.fn(),
        Role: getTeamRoles(),
      },
    }

    const { Manuscript, Team, TeamMember } = models
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.reviewersInvited,
    })

    const pendingReviewer = generateTeamMember({
      team: { role: Team.Role.reviewer, delete: jest.fn() },
      status: TeamMember.Statuses.pending,
    })
    const coiReviewer = generateTeamMember({
      team: { role: Team.Role.reviewer },
      status: TeamMember.Statuses.removed,
    })
    const academicEditor = generateTeamMember({
      team: { role: Team.Role.academicEditor },
      status: TeamMember.Statuses.accepted,
    })
    jest.spyOn(TeamMember, 'find').mockResolvedValue(pendingReviewer)
    jest
      .spyOn(Manuscript, 'findManuscriptByTeamMember')
      .mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'findAllByStatuses').mockResolvedValue([coiReviewer])

    await cancelReviewerInvitationUseCase
      .initialize({
        models,
        logEvent,
        jobsService,
        eventsService,
        notificationService,
      })
      .execute({
        manuscriptId: manuscript.id,
        teamMemberId: pendingReviewer.id,
        userId: academicEditor.userId,
      })

    expect(notificationService.notifyReviewer).toHaveBeenCalledTimes(1)
    expect(pendingReviewer.delete).toHaveBeenCalledTimes(1)
    expect(pendingReviewer.team.delete).toHaveBeenCalledTimes(0)
  })
})
