const models = require('@pubsweet/models')
const { inviteReviewerUseCase } = require('../../src/useCases/inviteReviewers')

const {
  Job,
  Team,
  User,
  Journal,
  Identity,
  TeamMember,
  Manuscript,
  ReviewerSuggestion,
} = models

const jobsService = {}
const eventsService = {
  publishSubmissionEvent: jest.fn(),
}
const emailJobsService = {
  scheduleEmailsWhenReviewerIsInvited: jest.fn(),
}
const invitationsService = {
  sendInvitationToReviewer: jest.fn(),
}
const removalJobsService = {
  scheduleRemovalJob: jest.fn(),
}
const logEvent = jest.fn()
logEvent.actions = {}
logEvent.objectType = {}

describe('Invite reviewer', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('adds a reviewer in the reviewer team', async () => {
    jest.spyOn(Manuscript, 'find').mockResolvedValue({
      id: 'some-manuscript-id',
      getEditorLabel: jest.fn(),
      updateStatus: jest.fn(),
      save: jest.fn(),
    })
    jest.spyOn(Identity, 'findOneByEmail').mockResolvedValue({})
    jest.spyOn(User, 'find').mockResolvedValue({ id: 'some-user-id' })
    jest
      .spyOn(Team, 'findOneBy')
      .mockResolvedValue({ id: 'some-reviewer-team-id' })
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRoleAndExcludedStatuses')
      .mockResolvedValue([])
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndUser')
      .mockResolvedValue({
        id: 'some-reviewer-id',
        status: 'expired',
        updateProperties: jest.fn(),
        save: jest.fn(),
      })
    jest.spyOn(ReviewerSuggestion, 'findOneBy').mockResolvedValue()
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValueOnce({})
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValueOnce({ id: 'some-academic-editor-id' })
    jest.spyOn(Job, 'findAllByTeamMember').mockResolvedValue()
    jest.spyOn(Journal, 'find').mockResolvedValue()
    jest
      .spyOn(TeamMember, 'findSubmittingAuthor')
      .mockResolvedValue({ getName: jest.fn() })
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue()

    await inviteReviewerUseCase
      .initialize({
        models,
        logEvent,
        jobsService,
        eventsService,
        emailJobsService,
        invitationsService,
        removalJobsService,
      })
      .execute({
        manuscriptId: 'some-manuscript-id',
        userId: 'some-user-id',
        input: {},
      })

    expect(invitationsService.sendInvitationToReviewer).toHaveBeenCalled()
    expect(
      emailJobsService.scheduleEmailsWhenReviewerIsInvited,
    ).toHaveBeenCalled()
    expect(removalJobsService.scheduleRemovalJob).toHaveBeenCalled()
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalled()
    expect(logEvent).toHaveBeenCalled()
  })
  it('throws an error if the reviewer is an editor in the special issue board', async () => {
    jest.spyOn(Manuscript, 'find').mockResolvedValue({
      id: 'some-manuscript-id',
      specialIssueId: 'some-special-issue-id',
    })
    jest.spyOn(Identity, 'findOneByEmail').mockResolvedValue({})
    jest.spyOn(User, 'find').mockResolvedValue({ id: 'some-user-id' })
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndUserAndRoles')
      .mockResolvedValue([
        {
          id: 'some-editor-id',
        },
      ])

    const result = inviteReviewerUseCase
      .initialize({
        models,
        logEvent,
        jobsService,
        eventsService,
        emailJobsService,
        invitationsService,
        removalJobsService,
      })
      .execute({
        manuscriptId: 'some-manuscript-id',
        userId: 'some-user-id',
        input: {},
      })

    // eslint-disable-next-line jest/valid-expect
    expect(result).rejects.toThrow(
      'This user cannot be invited because they are already part of the Special Issue team.',
    )
  })
})
