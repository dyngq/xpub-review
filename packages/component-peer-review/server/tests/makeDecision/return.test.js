const {
  getTeamRoles,
  generateJournal,
  getCommentTypes,
  generateManuscript,
  generateTeamMember,
  getRecommendations,
  getManuscriptStatuses,
  getTeamMemberStatuses,
  getManuscriptNonActionableStatuses,
} = require('component-generators')

const {
  makeDecisionToReturnUseCase,
} = require('../../src/useCases/makeDecision')

let models = {}
let Journal = {}
let Manuscript = {}
let TeamMember = {}

const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const logger = {
  error: jest.fn(),
}

const trx = {
  commit: jest.fn(),
  rollback: jest.fn(),
}

const transaction = {
  start: jest.fn(() => trx),
}

const logEvent = jest.fn(async () => {})
logEvent.actions = {
  manuscript_returned: 'manuscript_returned',
}
logEvent.objectType = { manuscript: 'manuscript' }

const notificationService = {
  notifyAcademicEditor: jest.fn(),
}

describe('Make decision to publish', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    models = {
      Journal: {
        find: jest.fn(),
      },
      Manuscript: {
        knex: jest.fn(),
        find: jest.fn(),
        Statuses: getManuscriptStatuses(),
        NonActionableStatuses: getManuscriptNonActionableStatuses(),
      },
      Team: {
        Role: getTeamRoles(),
      },
      TeamMember: {
        findOneByRole: jest.fn(),
        findTriageEditor: jest.fn(),
        findSubmittingAuthor: jest.fn(),
        findOneByJournalAndUser: jest.fn(),
        findAllByManuscriptAndRole: jest.fn(),
        findOneByManuscriptAndUser: jest.fn(),
        findAllByManuscriptAndRoleAndStatus: jest.fn(),
        findOneByManuscriptAndRoleAndStatus: jest.fn(),
        Statuses: getTeamMemberStatuses(),
      },
      Review: jest.fn(), // mock constructor
      Comment: jest.fn(), // mock constructor
    }
    models.Review.Recommendations = getRecommendations()
    models.Comment.Types = getCommentTypes()
    ;({ Journal, Manuscript, TeamMember } = models)
  })
  it('Throws an error if the manuscript is in a wrong status', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.published,
    })
    const editor = generateTeamMember({
      userId: 'editor-id',
    })

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)

    const res = makeDecisionToReturnUseCase.initialize({ models }).execute({
      manuscriptId: manuscript.id,
      userId: editor.id,
      content: 'Manuscript returned',
    })

    return expect(res).rejects.toThrow(
      'Cannot return a manuscript in the current status.',
    )
  })
  it('Executes the main flow', async () => {
    const journal = generateJournal({
      id: 'journal-id',
    })
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.makeDecision,
      $query: jest.fn(() => ({
        updateAndFetch: jest.fn(() => manuscript),
      })),
      $relatedQuery: jest.fn(() => ({
        insert: jest.fn(),
        insertAndFetch: jest.fn(() => manuscript),
      })),
    })
    const editor = generateTeamMember({
      userId: 'editor-id',
    })

    jest.spyOn(Journal, 'find').mockResolvedValue(journal)
    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndUser')
      .mockResolvedValue(editor)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue({})

    await makeDecisionToReturnUseCase
      .initialize({
        models,
        logger,
        logEvent,
        transaction,
        eventsService,
        notificationService,
      })
      .execute({
        manuscriptId: manuscript.id,
        userId: editor.userId,
        content: 'Returning manuscript',
      })

    expect(trx.commit).toBeCalledTimes(1)
    expect(trx.rollback).toBeCalledTimes(0)
  })
})
