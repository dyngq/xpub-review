// const Promise = require('bluebird')
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { updateDraftReviewUseCase } = require('../../src/useCases/submitReview')

const chance = new Chance()
describe('Update draft review use case', () => {
  it('updates a draft review', async () => {
    const {
      Team,
      Review,
      Comment,
      Journal,
      Manuscript,
      TeamMember,
      PeerReviewModel,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.underReview,
      },
      Manuscript,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    const reviewer = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.reviewer,
      input: { status: TeamMember.Statuses.accepted },
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.accepted },
    })

    const review = fixtures.generateReview({
      properties: {
        teamMemberId: reviewer.id,
        manuscriptId: manuscript.id,
      },
      Review,
    })
    const comment = await fixtures.generateComment({
      properties: {
        reviewId: review.id,
        type: Comment.Types.public,
        content: chance.sentence(),
      },
      Comment,
    })

    const reviewType = chance.pickone([
      Review.Recommendations.minor,
      Review.Recommendations.major,
      Review.Recommendations.reject,
      Review.Recommendations.publish,
    ])

    const input = {
      recommendation: reviewType,
      submitted: false,
      comments: [
        {
          id: comment.id,
          type: Comment.Types.public,
          content: chance.sentence(),
        },
      ],
    }

    const result = await updateDraftReviewUseCase
      .initialize({
        models,
      })
      .execute({
        reviewId: review.id,
        input,
        userId: reviewer.userId,
      })
    expect(result.comments[0].type).toContain(Comment.Types.public)
    expect(result.comments[0].content).not.toEqual(null)
    expect(reviewType).toEqual(result.recommendation)
  })
})
