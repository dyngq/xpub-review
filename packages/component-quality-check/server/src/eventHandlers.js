const models = require('@pubsweet/models')
const { logEvent } = require('component-activity-log/server')
const { sendPackage } = require('component-mts-package')
const events = require('component-events')
const { transaction } = require('objection')

const useCases = require('./useCases')
const notificationService = require('./notifications/notification')

const COI_HANDLERS = {
  reviewers: 'handleCOIBetweenAuthorAndReviewersUseCase',
  triageEditor: 'handleCOIBetweenAuthorAndTriageEditorUseCase',
  academicEditor: 'handleCOIBetweenAuthorAndAcademicEditorUseCase',
}

module.exports = {
  async SubmissionQualityCheckFilesRequested(data) {
    return useCases.requestQualityChecksUseCase
      .initialize({ models, logEvent })
      .execute(data)
  },
  async SubmissionQualityCheckPassed(data) {
    return useCases.createPackageForProductionUseCase
      .initialize({ models, sendPackage, notificationService })
      .execute({ data })
  },
  async SubmissionPeerReviewCycleCheckPassed(data) {
    return useCases.peerReviewCycleCheckPassedUseCase
      .initialize({ models, notificationService })
      .execute({ data })
  },
  async SubmissionQualityCheckRTCd(data) {
    const eventsService = events.initialize({ models })

    return useCases.rejectQualityCheckUseCase
      .initialize({ models, eventsService, logEvent, notificationService })
      .execute({ data })
  },
  async PeerReviewCycleCheckingProcessSentToPeerReview(data) {
    const eventsService = events.initialize({ models })

    const {
      submissionId,
      checks: {
        peerReviewCycle: {
          conflictOfInterest: {
            type,
            triageEditor = {},
            academicEditor = {},
            reviewers = [],
          },
        },
      },
    } = data

    const useCase = useCases[COI_HANDLERS[type]]
    if (!useCase) return

    return useCase
      .initialize({
        models,
        logEvent,
        useCases,
        transaction,
        eventsService,
        notificationService,
      })
      .execute({
        submissionId,
        triageEditorId: triageEditor.id,
        academicEditorId: academicEditor.id,
        reviewerIds: reviewers.map(r => r.id),
      })
  },
}
