const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')
const { logEvent } = require('component-activity-log/server')
const events = require('component-events')

const useCases = require('./useCases')

const resolvers = {
  Query: {},
  Mutation: {
    async submitQualityChecks(_, { submissionId }, ctx) {
      const eventsService = events.initialize({ models })
      return useCases.submitQualityChecksUseCase
        .initialize({ models, logEvent, eventsService })
        .execute({ submissionId, userId: ctx.user })
    },
    async updateQualityChecksDraft(_, { manuscriptId, autosaveInput }, ctx) {
      return useCases.updateQualityChecksDraftUseCase
        .initialize(models)
        .execute({ manuscriptId, autosaveInput })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
