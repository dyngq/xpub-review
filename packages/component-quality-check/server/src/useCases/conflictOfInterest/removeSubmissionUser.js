const { Promise } = require('bluebird')

const initialize = ({ models }) => ({
  async execute({ submissionId, role, userId, trx }) {
    const { TeamMember } = models

    const teamMembers = await TeamMember.findAllBySubmissionAndRoleAndUser({
      submissionId,
      role,
      userId,
    })

    if (!teamMembers.length) return

    teamMembers.forEach(
      teamMember => (teamMember.status = TeamMember.Statuses.removed),
    )

    return Promise.each(teamMembers, async teamMember =>
      teamMember.$query(trx).update(teamMember),
    )
  },
})

module.exports = {
  initialize,
}
