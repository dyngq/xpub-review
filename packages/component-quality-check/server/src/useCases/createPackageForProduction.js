const { uploadToMTS } = require('./uploadToMTS')

const initialize = ({
  models: { Manuscript, TeamMember, User, Journal, Team, ArticleType },
  sendPackage,
  notificationService,
}) => ({
  async execute({ data }) {
    const {
      submissionId,
      qc,
      qcLeaders = [],
      es,
      esLeaders = [],
      figureFilesCount,
    } = data

    const screeners = [
      ...qcLeaders.map(e => ({ ...e, role: 'qcLeader' })),
      ...esLeaders.map(e => ({ ...e, role: 'esLeader' })),
      ...(qc ? [{ ...qc, role: 'qc' }] : []),
      ...(es ? [{ ...es, role: 'es' }] : []),
    ]

    const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
      eagerLoadRelations: `[
           files,
           section,
           articleType,
           teams.members.[team,user.identities],
           specialIssue.section
           journal.[teams.members.team,peerReviewModel,preprints],
           reviews.[
             member.team
             comments.files,
            ],
          ]`,
    })
    lastManuscript.updateProperties({
      status: Manuscript.Statuses.published,
    })
    await lastManuscript.save()

    await uploadToMTS({
      submissionId,
      models: { Manuscript },
      sendPackage,
    })

    await sendPackage({
      manuscript: lastManuscript,
      isForProduction: true,
      screeners,
      figureFilesCount,
    })

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: lastManuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    notificationService.notifyAuthorWhenMaterialChecksIsCompleted({
      editorialAssistant,
      manuscript: lastManuscript,
    })

    await applicationEventBus.publishMessage({
      event: 'SubmissionPackageCreated',
      data: {
        journalId: lastManuscript.journalId,
        manuscriptId: lastManuscript.id,
        customId: lastManuscript.customId,
      },
    })
  },
})

module.exports = {
  initialize,
}
