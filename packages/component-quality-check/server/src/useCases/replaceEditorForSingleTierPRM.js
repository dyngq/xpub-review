const {
  getSuggestedAcademicEditorUseCase,
} = require('component-model/src/useCases')
const {
  initializeInviteAcademicEditorUseCase,
} = require('component-peer-review/server/src/useCases/inviteAcademicEditor/initializeInviteAcademicEditorUseCase')

const {
  inviteSuggestedAcademicEditorUseCase,
  inviteAcademicEditorUseCase,
} = require('component-peer-review/server/src/useCases/inviteAcademicEditor')

const initialize = ({ models }) => ({
  async execute({ manuscriptId }) {
    const manuscript = await models.Manuscript.find(manuscriptId)

    if (!manuscript.allowAcademicEditorAutomaticInvitation) {
      manuscript.allowAcademicEditorAutomaticInvitation = true
      await manuscript.save()
    }

    const getSuggestedAcademicEditor = getSuggestedAcademicEditorUseCase.initialize(
      {
        models,
      },
    )
    return inviteSuggestedAcademicEditorUseCase
      .initialize({
        models,
        useCases: {
          getSuggestedAcademicEditor,
          inviteAcademicEditor: initializeInviteAcademicEditorUseCase(
            inviteAcademicEditorUseCase,
          ),
        },
      })
      .execute(manuscript.submissionId)
  },
})

module.exports = {
  initialize,
}
