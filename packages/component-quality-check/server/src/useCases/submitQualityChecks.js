const initialize = ({ models: { Manuscript }, logEvent, eventsService }) => ({
  async execute({ submissionId, userId }) {
    const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      order: 'desc',
      orderByField: 'version',
      submissionId,
    })
    const lastManuscript = manuscripts[1]
    if (lastManuscript.status !== Manuscript.Statuses.qualityChecksRequested) {
      throw new ValidationError(
        `Cannot submit Quality Checks in the current status`,
      )
    }

    manuscripts[0].updateProperties({
      status: Manuscript.Statuses.qualityChecksSubmitted,
      qualityChecksSubmittedDate: new Date().toISOString(),
    })
    await manuscripts[0].save()

    await lastManuscript.updateStatus(Manuscript.Statuses.olderVersion).save()

    logEvent({
      userId,
      manuscriptId: lastManuscript.id,
      action: logEvent.actions.quality_checks_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: lastManuscript.id,
    })

    eventsService.publishSubmissionEvent({
      submissionId: lastManuscript.submissionId,
      eventName: 'SubmissionQualityChecksSubmitted',
    })
  },
})

module.exports = {
  initialize,
  authsomePolicies: ['hasAccessToSubmission', 'admin'],
}
