const { Promise } = require('bluebird')

module.exports = {
  async uploadToMTS({
    submissionId,
    models: { Manuscript },
    sendPackage,
    isRejected = false,
  }) {
    const manuscriptVersions = await Manuscript.findManuscriptsBySubmissionId({
      submissionId,
      excludedStatus: Manuscript.Statuses.deleted,
      eagerLoadRelations: `[
        files,
        section,
        articleType,
        teams.members.[team,user.identities],
        specialIssue.section
        journal.[teams.members.team,peerReviewModel,preprints],
        reviews.[
          member.team
          comments.files,
         ],
       ]`,
    })
    await Promise.each(manuscriptVersions, async manuscript =>
      sendPackage({
        manuscript,
        isEQA: true,
        isManuscriptRejected: isRejected,
      }),
    )
  },
}
