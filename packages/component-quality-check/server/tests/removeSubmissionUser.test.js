const {
  getTeamRoles,
  generateManuscript,
  generateTeamMember,
  getTeamMemberStatuses,
} = require('component-generators')

const {
  removeSubmissionUserUseCase,
} = require('../src/useCases/conflictOfInterest')

let models = {}
let Team = {}
let TeamMember = {}

const trx = {}

describe('Remove submission user use-case', () => {
  beforeEach(() => {
    jest.clearAllMocks()

    models = {
      Team: {
        Role: getTeamRoles(),
      },
      TeamMember: {
        Statuses: getTeamMemberStatuses(),
        findAllBySubmissionAndRoleAndUser: jest.fn(),
      },
    }
    ;({ Team, TeamMember } = models)
  })

  it('Replaces editors on single tier PRM', async () => {
    const manuscript = generateManuscript()
    const academicEditor = generateTeamMember({
      $query: jest.fn(() => ({ update: jest.fn() })),
    })

    jest
      .spyOn(TeamMember, 'findAllBySubmissionAndRoleAndUser')
      .mockResolvedValue([academicEditor])

    await removeSubmissionUserUseCase
      .initialize({
        models,
      })
      .execute({
        submissionId: manuscript.submissionId,
        role: Team.Role.academicEditor,
        userId: academicEditor.userId,
        trx,
      })

    expect(academicEditor.status).toEqual(TeamMember.Statuses.removed)
  })

  it('Returns if no user is found on submission', async () => {
    const manuscript = generateManuscript()
    const academicEditor = generateTeamMember({
      status: TeamMember.Statuses.active,
    })

    jest
      .spyOn(TeamMember, 'findAllBySubmissionAndRoleAndUser')
      .mockResolvedValue([])

    await removeSubmissionUserUseCase
      .initialize({
        models,
      })
      .execute({
        submissionId: manuscript.submissionId,
        role: Team.Role.academicEditor,
        userId: academicEditor.userId,
        trx,
      })

    expect(academicEditor.status).toEqual(TeamMember.Statuses.active)
  })
})
