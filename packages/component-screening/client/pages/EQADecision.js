import React from 'react'
import { H2, Button } from '@pubsweet/ui'
import { Modal } from 'component-modal'
import { Text, Row, ShadowedBox } from '@hindawi/ui'
import { compose, withHandlers, withProps } from 'recompose'

import withGQL from '../graphql'
import { parseSearchParams } from '../utils'
import {
  ReturnManuscriptToApprovalEditorModal,
  ApproveEQAModal,
} from '../components'

const EQADecision = ({
  handleReturnToApprovalEditor,
  handleApproveEQA,
  hasPeerReview,
  isAcademicEditor,
  canReturn,
}) => {
  const hasButton = JSON.parse(hasPeerReview)
  const canReturnManuscript = JSON.parse(canReturn)
  const hasAcademicEditor = JSON.parse(isAcademicEditor)
  return (
    <ShadowedBox center mt={6} width={130}>
      <H2 mt={3}>Editorial decision</H2>

      <Row justify="center" mt={2}>
        <Text secondary>Take a decision for manuscript.</Text>
      </Row>

      <Row justify="space-around" mb={6} mt={6}>
        <Modal
          component={ReturnManuscriptToApprovalEditorModal}
          handleReturnToApprovalEditor={handleReturnToApprovalEditor}
          modalKey="returnToApprovalEditor"
        >
          {showModal =>
            canReturnManuscript &&
            hasButton && (
              <Button
                data-test-id="reject-button"
                mr={2}
                onClick={showModal}
                secondary
                width={48}
              >
                {hasAcademicEditor ? 'Return to AE' : 'Return to CE'}
              </Button>
            )
          }
        </Modal>

        <Modal
          component={ApproveEQAModal}
          handleApproveEQA={handleApproveEQA}
          modalKey="approveEQA"
        >
          {showModal => (
            <Button
              data-test-id="accept-button"
              onClick={showModal}
              primary
              width={48}
            >
              Accept
            </Button>
          )}
        </Modal>
      </Row>
    </ShadowedBox>
  )
}

export default compose(
  withGQL,
  withProps(({ history }) => {
    const {
      token,
      canReturn,
      manuscriptId,
      hasPeerReview,
      isAcademicEditor,
    } = parseSearchParams(history.location.search)
    return { token, manuscriptId, hasPeerReview, isAcademicEditor, canReturn }
  }),
  withHandlers({
    handleReturnToApprovalEditor: ({
      returnToApprovalEditor,
      token,
      manuscriptId,
      history,
    }) => ({ reason }, { setFetching, setError, hideModal }) => {
      setFetching(true)
      returnToApprovalEditor({
        variables: {
          manuscriptId,
          input: {
            token,
            reason,
          },
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
          history.push('/info-page', {
            content: 'Manuscript decision submitted. Thank you!',
            title: 'Editorial decision',
          })
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
    handleApproveEQA: ({ approveEQA, token, manuscriptId, history }) => ({
      setFetching,
      setError,
      hideModal,
    }) => {
      setFetching(true)
      approveEQA({
        variables: {
          manuscriptId,
          input: {
            token,
          },
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
          history.push('/info-page', {
            content: 'Manuscript decision submitted. Thank you!',
            title: 'Editorial decision',
          })
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
  }),
)(EQADecision)
