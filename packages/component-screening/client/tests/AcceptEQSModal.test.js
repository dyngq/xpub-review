import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent } from '@testing-library/react'

import { render } from './testUtils'
import AcceptEQSModal from '../components/AcceptEQSModal'

describe('a default client test', () => {
  afterEach(cleanup)

  it('should submit on click OK', done => {
    const onSubmitMock = jest.fn()
    const { getByText } = render(
      <AcceptEQSModal handleApproveEQS={onSubmitMock} />,
    )

    fireEvent.click(getByText('OK'))

    setTimeout(() => {
      expect(onSubmitMock).toHaveBeenCalledTimes(1)
      done()
    })
  })
})
