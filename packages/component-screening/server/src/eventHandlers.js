const useCases = require('./use-cases')
const models = require('@pubsweet/models')

const config = require('config')
const Email = require('@pubsweet/component-email-templating')
const { sendPackage } = require('component-mts-package')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')
const urlService = require('./urlService/urlService')
const events = require('component-events')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const approveEQSNotifications = require('./notifications/approveEQS')
const getEmailCopy = require('./notifications/getEmailCopy')
const getProps = require('./notifications/getProps')

module.exports = {
  async SubmissionScreeningPassed(data) {
    const eventsService = events.initialize({ models })
    const getEmailCopyService = getEmailCopy.initialize()
    const getPropsService = getProps.initialize({
      baseUrl,
      urlService,
      footerText,
      unsubscribeSlug,
      getModifiedText,
    })
    const notificationService = approveEQSNotifications.initialize({
      Email,
      getEmailCopyService,
      getPropsService,
    })

    // #endregion
    return useCases.approveEQSByEventUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        eventsService,
        notificationService,
      })
      .execute({ data })
  },
  async SubmissionScreeningRTCd(data) {
    return useCases.declineEQSByEventUseCase
      .initialize({ models, logEvent, sendPackage })
      .execute({ data })
  },
  async SubmissionScreeningReturnedToDraft(data) {
    return useCases.returnToDraftUseCase
      .initialize({ models, logEvent })
      .execute({ data })
  },
  async SubmissionScreeningVoid(data) {
    return useCases.voidManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({ data })
  },
}
