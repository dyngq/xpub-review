const initialize = ({
  eventsService,
  manuscriptStatuses,
  getEligibleUserUseCase,
  models: { TeamMember, Team, User, SpecialIssue },
}) => ({
  async execute({
    journalId,
    sectionId,
    manuscriptId,
    submissionId,
    specialIssueId,
  }) {
    const triageEditor = await getEligibleUserUseCase.execute({
      journalId,
      sectionId,
      specialIssueId,
      manuscriptStatuses,
      role: Team.Role.triageEditor,
      models: { TeamMember, SpecialIssue },
      teamMemberStatuses: [TeamMember.Statuses.active],
    })

    const queryObject = {
      manuscriptId,
      role: Team.Role.triageEditor,
    }
    const triageEditorTeam = await Team.findOrCreate({
      queryObject,
      options: queryObject,
    })
    const triageEditorUser = await User.find(triageEditor.userId, 'identities')

    const manuscriptTriageEditor = triageEditorTeam.addMember({
      user: triageEditorUser,
      options: {
        status: TeamMember.Statuses.active,
      },
    })
    await manuscriptTriageEditor.save()
    await triageEditorTeam.save()

    eventsService.publishSubmissionEvent({
      submissionId,
      eventName: 'SubmissionTriageEditorAssigned',
    })
  },
})

module.exports = {
  initialize,
}
