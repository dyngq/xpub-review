const service = require('./server/dist/keycloakService')
const server = require('./server/dist/useKeycloakBearer')

module.exports = {
  server,
  ...service,
}
