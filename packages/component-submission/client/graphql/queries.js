import gql from 'graphql-tag'

import { draftManuscriptDetails, activeJournals } from './fragments'

export const getManuscriptAndActiveJournals = gql`
  query manuscript($manuscriptId: String!) {
    manuscript(manuscriptId: $manuscriptId) {
      ...draftManuscriptDetails
    }
    getActiveJournals {
      ...activeJournals
    }
  }
  ${draftManuscriptDetails}
  ${activeJournals}
`

export const getActiveJournals = gql`
  query getActiveJournals {
    getActiveJournals {
      ...activeJournals
    }
  }
  ${activeJournals}
`

export const autosaveState = gql`
  query autosaveState {
    autosave @client {
      error
      updatedAt
      inProgress
    }
  }
`
