const config = require('config')
const { services } = require('helper-service')
const Email = require('@pubsweet/component-email-templating')
const { get } = require('lodash')

const { getEmailCopy } = require('./emailCopy')
const { getModifiedText } = require('component-transform-text')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const resetPath = config.get('invite-reset-password.url')
const staffEmail = config.get('staffEmail')
const footerText = config.get('emailFooterText.registeredUsers')
const unconfirmedUsersFooterText = config.get(
  'emailFooterText.unregisteredUsers',
)
const bccAddress = config.get('bccAddress')

module.exports = {
  async sendToConfirmedAuthors(
    { alias, user },
    { manuscript, submittingAuthor, editorialAssistant, journal },
  ) {
    const { name: journalName } = journal
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'confirmed-authors',
      manuscript,
      journalName,
      submittingAuthor,
    })
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : journalName
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: alias.email,
        name: alias.surname,
      },
      content: {
        subject: `Manuscript submitted to ${journalName}`,
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        ctaText: 'LOGIN',
        paragraph,
        signatureName: editorialAssistantName,
        signatureJournal: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: alias.email,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async sendToUnconfirmedAuthors(
    { alias, user },
    { manuscript, submittingAuthor, editorialAssistant, journal },
  ) {
    const { name: journalName } = journal
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail
    const editorialAssistantName = editorialAssistant.getName()

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'unconfirmed-authors',
      manuscript,
      journalName,
      submittingAuthor,
    })

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: alias.email,
        name: alias.surname,
      },
      content: {
        subject: `Manuscript submitted to ${journalName}`,
        ctaLink: services.createUrl(baseUrl, resetPath, {
          userId: user.id,
          confirmationToken: user.confirmationToken,
          ...alias,
        }),
        ctaText: 'CREATE ACCOUNT',
        paragraph,
        signatureName: editorialAssistantName,
        signatureJournal: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
        footerText: getModifiedText(
          unconfirmedUsersFooterText,
          {
            pattern: '{recipientEmail}',
            replacement: alias.email,
          },
          { pattern: '{journalName}', replacement: journalName },
        ),
      },
      bodyProps,
    })

    return email.sendEmail()
  },

  async sendSubmittingAuthorConfirmation({
    journal,
    manuscript,
    submittingAuthor,
    editorialAssistant,
  }) {
    const emailType = 'submitting-author-manuscript-submitted'
    const { name: journalName } = journal

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      manuscript,
      journalName,
    })
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : journalName
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail
    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: submittingAuthor.alias.email,
        name: `${submittingAuthor.alias.surname}`,
      },
      content: {
        subject: `Manuscript submitted to ${journalName}`,
        paragraph,
        signatureName: editorialAssistantName,
        signatureJournal: journalName,
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        ctaText: 'MANUSCRIPT DETAILS',
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: submittingAuthor.user.id,
          token: submittingAuthor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: submittingAuthor.alias.email,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },

  async notifyEditorsWhenNewManuscriptSubmitted({
    journal,
    editor,
    submittingAuthor,
    editorialAssistant,
    manuscript,
  }) {
    const { name: journalName } = journal
    const editorName = editor.getName()
    const editorEmail = get(editor, 'alias.email')
    const editorialAssistantName = editorialAssistant.getName()
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || get(journal, 'email')

    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName,
      manuscript,
      submittingAuthor,
      emailType: 'editors-new-manuscript-submitted',
    })

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      toUser: {
        email: editorEmail,
        name: editorName,
      },
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      content: {
        subject: `${manuscript.customId}: New manuscript submitted`,
        paragraph,
        signatureName: editorialAssistantName,
        signatureJournal: journalName,
        ctaText: 'MANUSCRIPT DETAILS',
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: get(editor, 'user.id', ''),
          token: get(editor, 'user.unsubscribeToken', ''),
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: editorEmail,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
}
