const MAX_CUSTOM_ID_TRIES = 100

const initialize = ({ Manuscript, Team, User, TeamMember }) => ({
  async execute({
    input: { journalId, sectionId, specialIssueId, customId },
    userId,
  }) {
    const user = await User.find(userId, 'identities')

    const hasStaffRole = await user.hasStaffRole()
    const hasEditorialAssistantRole = !!(await TeamMember.findOneByUserAndRoleAndStatus(
      {
        userId,
        status: TeamMember.Statuses.pending,
        role: Team.Role.editorialAssistant,
      },
    ))
    const isAdmin = !!(await TeamMember.findOneByUserAndRole({
      userId,
      role: Team.Role.admin,
    }))
    if (!customId) {
      customId = await Manuscript.generateUniqueCustomId(
        Manuscript,
        MAX_CUSTOM_ID_TRIES,
      )
    }

    if (!customId) {
      throw new Error(
        `Unable to generate a customId after ${MAX_CUSTOM_ID_TRIES} tries.`,
      )
    }

    const manuscript = new Manuscript({
      journalId,
      sectionId,
      specialIssueId,
      customId,
    })

    manuscript.teams = []
    if (hasEditorialAssistantRole || isAdmin) {
      const team = new Team({
        role: Team.Role.submittingStaffMember,
        manuscriptId: manuscript.id,
      })
      manuscript.teams.push(team)
      team.addMember({
        user,
        options: {
          status: TeamMember.Statuses.pending,
        },
      })
    }
    if (!hasStaffRole) {
      const team = new Team({
        role: Team.Role.author,
        manuscriptId: manuscript.id,
      })
      manuscript.teams.push(team)
      team.addMember({
        user,
        options: {
          isSubmitting: true,
          isCorresponding: true,
        },
      })
    }

    const savedManuscript = await manuscript.saveGraph({ relate: true })
    return savedManuscript.toDTO()
  },
})
const authsomePolicies = ['isAuthenticated']
module.exports = {
  initialize,
  authsomePolicies,
}
