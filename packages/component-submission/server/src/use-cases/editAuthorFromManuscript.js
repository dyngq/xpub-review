const { orderBy } = require('lodash')

const initialize = ({
  models: { Manuscript, TeamMember, Team },
  logEvent,
}) => ({
  execute: async ({
    params: {
      manuscriptId,
      authorTeamMemberId,
      authorInput: { isCorresponding, isSubmitting, ...alias },
    },
    userId,
  }) => {
    const editedAuthor = await TeamMember.find(authorTeamMemberId)
    await TeamMember.handleCorrespondingAuthor({
      manuscriptId,
      editedAuthor,
      inputCorresponding: isCorresponding,
    })
    editedAuthor.updateProperties({ alias })
    await editedAuthor.save()

    const manuscript = await Manuscript.find(manuscriptId)
    if (manuscript.status !== Manuscript.Statuses.draft) {
      logEvent({
        userId,
        manuscriptId,
        action: logEvent.actions.author_edited,
        objectType: logEvent.objectType.user,
        objectId: editedAuthor.userId,
      })
    }
    const authors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.author,
    })
    return orderBy(authors, 'position', 'asc').map(a => a.toDTO())
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
