const initialize = ({ Journal }) => ({
  execute: async _ => (await Journal.getAllActiveJournals()) || [],
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
