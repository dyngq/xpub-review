const submitManuscriptUseCase = require('./submitManuscript')
const editManuscriptUseCase = require('./editManuscript')
const getActiveJournalsUseCase = require('./getActiveJournals')
const updateManuscriptFileUseCase = require('./updateManuscriptFile')
const addAuthorToManuscriptUseCase = require('./addAuthorToManuscript')
const createDraftManuscriptUseCase = require('./createDraftManuscript')
const updateDraftManuscriptUseCase = require('./updateDraftManuscript')
const editAuthorFromManuscriptUseCase = require('./editAuthorFromManuscript')
const removeAuthorFromManuscriptUseCase = require('./removeAuthorFromManuscript')
const assignEditorialAssistantOnManuscriptUseCase = require('./assignEditorialAssistantOnManuscript')

module.exports = {
  submitManuscriptUseCase,
  editManuscriptUseCase,
  getActiveJournalsUseCase,
  updateManuscriptFileUseCase,
  createDraftManuscriptUseCase,
  updateDraftManuscriptUseCase,
  addAuthorToManuscriptUseCase,
  editAuthorFromManuscriptUseCase,
  removeAuthorFromManuscriptUseCase,
  assignEditorialAssistantOnManuscriptUseCase,
}
