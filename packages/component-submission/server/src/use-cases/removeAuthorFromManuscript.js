const { findIndex, sortBy } = require('lodash')

const initialize = ({ models: { Team, Manuscript }, logEvent }) => ({
  execute: async ({ manuscriptId, authorTeamMemberId, userId }) => {
    const authorTeam = await Team.findOneBy({
      queryObject: { manuscriptId, role: Team.Role.author },
      eagerLoadRelations: 'members.[user.[identities]]',
    })
    const authorTeamMember = authorTeam.members.find(
      m => m.id === authorTeamMemberId,
    )
    authorTeam.removeMember(authorTeamMemberId)

    const teamHasCorrespondingAuthor = authorTeam.members.some(
      a => a.isCorresponding,
    )
    if (!teamHasCorrespondingAuthor) {
      const submittingAuthorIndex = findIndex(authorTeam.members, [
        'isSubmitting',
        true,
      ])
      authorTeam.members[submittingAuthorIndex].isCorresponding = true
    }

    await authorTeam.saveGraph({ relate: true, noDelete: false })
    const manuscript = await Manuscript.find(manuscriptId)
    if (manuscript.status !== 'draft') {
      logEvent({
        userId,
        manuscriptId,
        action: logEvent.actions.author_removed,
        objectType: logEvent.objectType.user,
        objectId: authorTeamMember.userId,
      })
    }

    return sortBy(authorTeam.members, 'position').map(a => a.toDTO())
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
