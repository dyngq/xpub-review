const { uploadManuscriptToMTS } = require('./uploadManuscriptToMTS')

module.exports = {
  execute: async ({
    journal,
    manuscript,
    Manuscript,
    sendPackage,
    eventsService,
    submittingAuthor,
    editorialAssistant,
    notificationService,
  }) => {
    manuscript.updateProperties({
      status: Manuscript.Statuses.makeDecision,
      submittedDate: new Date().toISOString(),
    })

    await manuscript.save()

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionSubmitted',
    })

    uploadManuscriptToMTS({
      manuscriptId: manuscript.id,
      Manuscript,
      sendPackage,
    })

    notificationService.notifyEditorsWhenNewManuscriptSubmitted({
      journal,
      manuscript,
      submittingAuthor,
      editorialAssistant,
    })
  },
}
