const logger = require('@pubsweet/logger')

const { uploadManuscriptToMTS } = require('./uploadManuscriptToMTS')

module.exports = {
  execute: async ({
    Team,
    journal,
    TeamMember,
    Manuscript,
    manuscript,
    submittingAuthor,
    authorTeamMembers,
    sendPackage,
    eventsService,
    editorialAssistant,
    notificationService,
  }) => {
    await manuscript.submitManuscript()
    logger.info(
      `Manuscript ${manuscript.id} has technicalCheckToken ${manuscript.technicalCheckToken} before saving`,
    )
    if (!manuscript.technicalCheckToken) {
      throw new ConflictError('Something went wrong. Please try again.')
    }
    await manuscript.save()
    logger.info(
      `Manuscript ${manuscript.id} has technicalCheckToken ${manuscript.technicalCheckToken} after saving`,
    )

    uploadManuscriptToMTS({
      manuscriptId: manuscript.id,
      Manuscript,
      sendPackage,
    })

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionSubmitted',
    })

    const coAuthors = authorTeamMembers.filter(author => !author.isSubmitting)
    coAuthors.forEach(async author => {
      const isConfirmed = await author.user.isUserConfirmed()
      if (isConfirmed) {
        notificationService.sendToConfirmedAuthors(author, {
          journal,
          manuscript,
          submittingAuthor,
          editorialAssistant,
        })
      } else {
        notificationService.sendToUnconfirmedAuthors(author, {
          journal,
          manuscript,
          submittingAuthor,
          editorialAssistant,
        })
      }
    })
  },
}
