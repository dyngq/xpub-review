process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
process.env.PUBLISHER_NAME = 'hindawi'
const Chance = require('chance')

const chance = new Chance()

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const useCases = require('../src/use-cases')

const makeMockFn = () => jest.fn(async () => {})
const logEvent = () => jest.fn(async () => {})
const eventsService = {
  publishSubmissionEvent: jest.fn(async () => {}),
}

logEvent.actions = {
  manuscript_submitted: 'manuscript_submitted',
}
logEvent.objectType = { manuscript: 'manuscript' }

const notificationService = {
  sendToConfirmedAuthors: makeMockFn(),
  sendToUnconfirmedAuthors: makeMockFn(),
  sendSubmittingAuthorConfirmation: makeMockFn(),
  notifyEditorsWhenNewManuscriptSubmitted: jest.fn(),
}
const sendPackage = makeMockFn()

const { submitManuscriptUseCase } = require('../src/use-cases')

describe('Submit Manuscript Use Case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('should update status and publication dates', async () => {
    const {
      PeerReviewModel,
      Journal,
      Team,
      TeamMember,
      Manuscript,
      ArticleType,
    } = models

    const hasPeerReview = true

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    const editorialAssistant = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    await dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })
    await fixtures.generateArticleTypes(ArticleType)
    const articleType = fixtures.getArticleTypeByPeerReview(hasPeerReview)

    const draftManuscript = await fixtures.generateManuscript({
      properties: {
        version: '1',
        status: Manuscript.Statuses.draft,
        journalId: journal.id,
        articleTypeId: articleType.id,
      },
      Manuscript,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript: draftManuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: Team.Role.author,
    })
    draftManuscript.journal = journal

    models.TeamMember.findAllByManuscriptParentAndRole = jest
      .fn()
      .mockReturnValue([editorialAssistant])
    models.TeamMember.findAllWithWorkloadByManuscriptParent = jest
      .fn()
      .mockReturnValue([editorialAssistant])

    await submitManuscriptUseCase
      .initialize({
        models,
        sendPackage,
        eventsService,
        notificationService,
        logEvent,
        useCases,
      })
      .execute({ manuscriptId: draftManuscript.id })

    expect(draftManuscript.status).toBe('technicalChecks')
    expect(draftManuscript.technicalCheckToken).toBeDefined()
    expect(sendPackage).toHaveBeenCalledTimes(1)
    /* 
      publishSubmissionEvent is called:
      once for SubmissionEditorialAssistantAssigned
      and once for SubmissionSubmitted
    */
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledTimes(2)
  })
  it('should auto assign a RIPE for the specific manuscripts', async () => {
    const {
      PeerReviewModel,
      Journal,
      Team,
      TeamMember,
      Manuscript,
      ArticleType,
    } = models

    fixtures.generateArticleTypes(ArticleType)
    const articleTypesWithRIPE = fixtures.articleTypes.filter(articleType =>
      ArticleType.TypesWithRIPE.includes(articleType.name),
    )

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    await dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })
    await dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.researchIntegrityPublishingEditor,
    })
    const articleType = chance.pickone(articleTypesWithRIPE)
    const draftManuscript = await fixtures.generateManuscript({
      properties: {
        version: '1',
        status: Manuscript.Statuses.draft,
        journalId: journal.id,
        articleTypeId: articleType.id,
      },
      Manuscript,
    })
    draftManuscript.articleType = articleType

    await dataService.createUserOnManuscript({
      models,
      manuscript: draftManuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: Team.Role.author,
    })
    draftManuscript.journal = journal

    await submitManuscriptUseCase
      .initialize({
        models,
        sendPackage,
        notificationService,
        logEvent,
        eventsService,
        useCases,
      })
      .execute({ manuscriptId: draftManuscript.id })

    const manuscript = fixtures.manuscripts.find(
      manuscript => manuscript.id === draftManuscript.id,
    )

    const ripeTeam = fixtures.teams.filter(
      team =>
        team.role === Team.Role.researchIntegrityPublishingEditor &&
        manuscript.id === draftManuscript.id,
    )

    expect(manuscript.status).toEqual(Manuscript.Statuses.makeDecision)
    expect(ripeTeam).toBeDefined()
  })
  it('should create editorial assistants on manuscript', async () => {
    const { PeerReviewModel, Journal, Manuscript, ArticleType, Team } = models
    const prm = fixtures.generatePeerReviewModel({ PeerReviewModel })
    const journal = fixtures.generateJournal({
      Journal,
      properties: {
        peerReviewModelId: prm.id,
      },
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    await dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })
    await fixtures.generateArticleTypes(ArticleType)
    const hasPeerReview = true
    const articleType = fixtures.getArticleTypeByPeerReview(hasPeerReview)

    const draftManuscript = await fixtures.generateManuscript({
      properties: {
        version: '1',
        status: Manuscript.Statuses.draft,
        journalId: journal.id,
        articleTypeId: articleType.id,
      },
      Manuscript,
    })
    draftManuscript.journal = journal

    await submitManuscriptUseCase
      .initialize({
        models,
        sendPackage,
        eventsService,
        notificationService,
        logEvent,
        useCases,
      })
      .execute({ manuscriptId: draftManuscript.id })

    const editorialAssistantManuscriptTeam = fixtures.getTeamByRoleAndManuscriptId(
      {
        id: draftManuscript.id,
        role: Team.Role.editorialAssistant,
      },
    )
    expect(editorialAssistantManuscriptTeam).toBeDefined()
    expect(editorialAssistantManuscriptTeam.members).toHaveLength(1)
  })
})
