const getModifiedText = (text, ...args) => {
  args.map(
    arg => (text = text.replace(new RegExp(arg.pattern, 'g'), arg.replacement)),
  )

  return text
}
module.exports = {
  getModifiedText,
}
