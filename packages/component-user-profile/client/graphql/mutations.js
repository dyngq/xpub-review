import gql from 'graphql-tag'

export const updateUser = gql`
  mutation updateUser($input: EditProfileInput) {
    updateUser(input: $input)
  }
`
export const subscribeToEmails = gql`
  mutation subscribeToEmails {
    subscribeToEmails
  }
`
export const unsubscribeToEmails = gql`
  mutation subscribeToEmails($input: UnsubscribeInput) {
    unsubscribeToEmails(input: $input)
  }
`
export const changePassword = gql`
  mutation changePassword($input: ChangePasswordInput!) {
    changePassword(input: $input) {
      token
    }
  }
`
export const unlinkOrcid = gql`
  mutation unlinkOrcid {
    unlinkOrcid
  }
`
