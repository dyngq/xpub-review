const initialize = ({ models: { Identity }, eventsService }) => ({
  execute: async ({ userId }) => {
    const identities = await Identity.findBy({ userId })
    const orcidIdentity = identities.find(i => i.type === 'orcid')
    if (!orcidIdentity) {
      throw new Error('There is no Orcid account linked.')
    }
    await orcidIdentity.delete()
    eventsService.publishUserEvent({
      userId,
      eventName: 'UserORCIDRemoved',
    })
  },
})
const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
