const files = require('./files')
const users = require('./users')
const teams = require('./teams')
const reviews = require('./reviews')
const journals = require('./journals')
const comments = require('./comments')
const sections = require('./sections')
const auditLogs = require('./auditLogs')
const identities = require('./identities')
const teamMembers = require('./teamMembers')
const manuscripts = require('./manuscripts')
const articleTypes = require('./articleTypes')
const specialIssues = require('./specialIssues')
const peerReviewModels = require('./peerReviewModels')
const journalArticleTypes = require('./journalArticleTypes')
const reviewerSuggestions = require('./reviewerSuggestions')

module.exports = {
  ...users,
  ...files,
  ...teams,
  ...reviews,
  ...journals,
  ...comments,
  ...sections,
  ...auditLogs,
  ...identities,
  ...teamMembers,
  ...manuscripts,
  ...articleTypes,
  ...specialIssues,
  ...peerReviewModels,
  ...journalArticleTypes,
  ...reviewerSuggestions,
}
