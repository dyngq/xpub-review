const specialIssues = []
const generateSpecialIssue = ({ properties, SpecialIssue }) => {
  const specialIssue = new SpecialIssue(properties || {})
  specialIssues.push(specialIssue)

  return specialIssue
}

module.exports = { specialIssues, generateSpecialIssue }
