const teamMembers = []

const generateTeamMember = ({ properties, TeamMember }) => {
  const teamMember = new TeamMember(properties || {})

  teamMembers.push(teamMember)

  return teamMember
}

const findTeamMember = (teamId, userId) =>
  teamMembers.find(
    teamMember => teamMember.userId === userId && teamMember.teamId === teamId,
  )
const findTeamMembers = userId =>
  teamMembers.filter(teamMember => teamMember.userId === userId)

module.exports = {
  teamMembers,
  findTeamMember,
  findTeamMembers,
  generateTeamMember,
}
