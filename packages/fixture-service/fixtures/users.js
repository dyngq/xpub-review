const { has, assign } = require('lodash')

const identities = require('./identities')

const users = []

const generateUser = ({ properties, identityProperties, User, Identity }) => {
  const user = new User(properties || {})
  const identity = identities.generateIdentity({
    properties: {
      userId: user.id,
      ...(identityProperties || {}),
    },
    Identity,
  })
  identity.user = user

  if (has(properties, ['isConfirmed'])) {
    assign(identity, properties)
  }

  user.identities.push(identity)

  users.push(user)

  return user
}

module.exports = { users, generateUser }
