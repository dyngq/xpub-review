const Chance = require('chance')

const chance = new Chance()
const fixtures = require('../fixtures')

const { journalArticleTypes } = fixtures
const { findOneByMock } = require('./repositoryMocks')

class JournalArticleType {
  constructor(props) {
    this.id = chance.guid()
    this.journalId = props.journalId || null
    this.articleTypeId = props.articleTypeId || null
    this.created = props.created || Date.now()
  }

  static async findOrCreate({ options, queryObject }) {
    let journalArticleType = await this.findOneBy({
      queryObject,
    })

    if (!journalArticleType) {
      journalArticleType = new this(options)
    }

    return journalArticleType
  }

  static findOneBy = values =>
    findOneByMock(values, 'journalArticleTypes', fixtures)

  async save() {
    journalArticleTypes.push(this)
    return Promise.resolve(this)
  }

  toDTO() {
    return {
      ...this,
    }
  }
}

module.exports = JournalArticleType
