const Chance = require('chance')
const { assign, filter, includes } = require('lodash')

const chance = new Chance()
const fixtures = require('../fixtures')

const { users } = fixtures
const { findMock, findOneByMock, findAllMock } = require('./repositoryMocks')

class User {
  constructor(props) {
    this.id = chance.guid()
    this.isActive = props.isActive === undefined ? true : props.isActive
    this.isSubscribedToEmails = props.isSubscribedToEmails
    this.defaultIdentityType = props.defaultIdentityType || 'local'
    this.confirmationToken = props.confirmationToken || chance.guid()
    this.invitationToken = props.invitationToken || null
    this.passwordResetTimestamp = props.passwordResetTimestamp || null
    this.passwordResetToken = props.passwordResetToken || null
    this.unsubscribeToken = props.unsubscribeToken || chance.guid()
    this.agreeTc = props.agreeTc
    this.identities = props.identities || []
    this.teamMemberships = props.teamMemberships || []
  }

  static async findAllWithDefaultIdentity() {
    const identityUsers = users.map(user => {
      const localIdentity = user.defaultIdentity
      return {
        id: user.id,
        isActive: user.isActive,
        name: {
          surname: localIdentity.surname,
          givenNames: localIdentity.givenNames,
        },
        aff: localIdentity.aff,
        isConfirmed: localIdentity.isConfirmed,
        email: localIdentity.email,
      }
    })

    return {
      results: identityUsers,
      total: users.length,
    }
  }

  static async findOneWithDefaultIdentity(userId) {
    const user = users.find(({ id }) => id === userId)
    const localIdentity = user.defaultIdentity

    return {
      id: user.id,
      isActive: user.isActive,
      name: {
        surname: localIdentity.surname,
        givenNames: localIdentity.givenNames,
      },
      aff: localIdentity.aff,
      isConfirmed: localIdentity.isConfirmed,
      email: localIdentity.email,
    }
  }

  static async findByEmailOrName({ input }) {
    const identities = filter(
      fixtures.identities,
      i =>
        includes(i.email, input) ||
        includes(i.surname, input) ||
        includes(i.givenNames, input),
    )
    const userIds = identities.map(i => i.userId)

    return users.filter(u => includes(userIds, u.id))
  }

  static find = id => findMock(id, 'users', fixtures)
  static findOneBy = values => findOneByMock(values, 'users', fixtures)
  static findAll = ({ orderByField, order, queryObject }) =>
    findAllMock('users', fixtures, orderByField, order, queryObject)

  findTeamMemberByRole(role) {
    if (!this.teamMemberships) {
      throw new Error('User team memberships are required.')
    }

    return this.teamMemberships.find(tm => tm.team.role === role)
  }

  assignIdentity(identity) {
    this.identities = this.identities || []
    this.identities.push(identity)
  }

  async save() {
    users.push(this)
    return Promise.resolve(this)
  }

  async saveRecursively() {
    await this.save()

    await Promise.all(this.identities.map(async identity => identity.save()))
  }

  async saveGraph() {
    await this.save()

    await Promise.all(this.identities.map(async identity => identity.save()))
  }

  async insertWithIdentity(identity) {
    await this.save()
    this.identities = [identity]
    await Promise.all(this.identities.map(async identity => identity.save()))
    return this
  }

  get defaultIdentity() {
    if (!this.identities) {
      throw new Error('Identities are required.')
    }

    return this.identities.find(
      identity => identity.type === this.defaultIdentityType,
    )
  }

  updateProperties(properties) {
    assign(this, properties)
    return this
  }

  async updateWithIdentity(identity) {
    this.identities.push(this)
    return this
  }

  toDTO() {
    return {
      ...this,
      identities: this.identities.map(identity => identity.toDTO()),
    }
  }
}

module.exports = User
