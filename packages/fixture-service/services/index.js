const tokenService = require('./tokenService')
const dataService = require('./dataService')

module.exports = {
  tokenService,
  dataService,
}
