const chance = require('chance')()

const tokenService = {
  create: () => chance.hash(),
}

module.exports = tokenService
