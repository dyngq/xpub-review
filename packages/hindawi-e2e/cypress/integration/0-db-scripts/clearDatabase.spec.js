describe('Clear DB', () => {
  it('Clear all database', () => {
    cy.task('clearDatabase').should('equal', true)
    cy.exec('yarn cypress:migrate', { timeout: 300000 })
      .its('code')
      .should('eq', 0)
    cy.exec('yarn cypress:seeddb', { timeout: 30000 })
    cy.task('createJournal').should('equal', true)
    cy.task('assignArticleType').should('equal', true)
  })
})
