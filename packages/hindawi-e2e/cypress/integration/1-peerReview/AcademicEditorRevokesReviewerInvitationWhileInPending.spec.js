describe('AcademicEditor revokes reviewer invitation while invitation is in pending', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
  })

  it('Should accept invitation as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { academicEditor } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsAcademicEditor('yes')
  })

  it('Invite reviewers as AcademicEditor', function inviteReviewer() {
    const { reviewer, academicEditor } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.inviteReviewer({ reviewer, academicEditor })
  })

  it('Revoke reviewer invitation while invitation is in pending as AcademicEditor', function revokeReviewer() {
    const { reviewer, academicEditor } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.get(`div[label="Reviewer Details & Reports"]`)
      .click()
      .contains('PENDING')
      .click()
    cy.get('[data-test-id="revoke-icon"]')
      .eq(0)
      .should('be.visible')
      .click()
    cy.get(`[ data-test-id="modal-cancel"]`).click()
    cy.get(`div[label="Reviewer Details & Reports"]`)
      .click()
      .contains('PENDING')
      .last()
      .click()
    cy.get('[data-test-id="revoke-icon"]')
      .eq(1)
      .should('be.visible')
      .last()
      .click()
    cy.get(`[ data-test-id="modal-confirm"]`).click()

    cy.get(`div[label="Reviewer Details & Reports"]`)
      .should('be.visible')
      .should('contain', 'Reviewer1 Rev1')
      .should('not.contain', 'Reviewer2 Rev2')
      .should('contain', 'Reviewer Rev')
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).should(
      'not.be.visible',
    )
  })
})
