describe('Author edits before EQS', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Author creates draft', () => {
    cy.createDraftViaAPI()
  })

  it('Author edits manuscript before EQS', function editManuscript() {
    const { author, fragment } = this
    cy.loginApi(author.email, author.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.editManuscript(fragment)
    cy.get('[type="button"]')
      .contains('SUBMIT MANUSCRIPT')
      .click()
    cy.findByText('AGREE & SUBMIT').click()
    cy.visit('/')
    cy.get(
      `[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`,
    ).contains('Fragment 2 - modified')
  })
})
