describe('Request Manuscript Revision Triage Editor', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Triage Editor successfully request manuscript revision from author', function requestManuscriptRevision() {
    const { triageEditor } = this
    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.triageEditorMakesDecision('Request Revision')
  })

  it('Author submit revision', async function authorSubmitRevision() {
    const { author, statuses, updatedFragment } = this
    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.revisionRequested.author)
    cy.authorSubmitRevision({ updatedFragment })
    cy.visit('/')
    cy.get(`[data-test-id="dashboard-list-items"] > div`)
      .first()
      .click()
    cy.contains('Response to Revision Request')
    cy.contains('Your Reply')
    cy.checkStatus(statuses.academicEditorInvited.author)
  })
})
