describe('User submit manuscript from hindawi.com website', () => {
  beforeEach(() => {
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('journals/journal').as('journal')

    cy.clearLocalStorage()
  })
  it('Succesfully preselect journal when code is in url', function submitFromURL() {
    const { academicEditor, journal } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit(`/submit?journal=${journal.code}`)
  })
  it('No selection if a journal with specific code is not found', function submitFromURLWithoutSelection() {
    const { academicEditor } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/submit?journal=SOMECODE')
    cy.get('[data-test-id="journal-select"] button span')
    cy.contains('Select journal')
  })
})
