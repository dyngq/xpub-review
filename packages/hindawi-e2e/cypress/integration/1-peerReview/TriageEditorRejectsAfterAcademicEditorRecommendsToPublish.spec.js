describe('Triage Editor rejects after AcademicEditor recommends to publish ', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
  })

  it('Should accept invitation as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { academicEditor } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsAcademicEditor('yes')
  })

  it('Invite reviewers as AcademicEditor', function inviteReviewer() {
    const { reviewer, academicEditor } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.inviteReviewer({ reviewer, academicEditor })
  })

  it('Give a review for publish as Reviewer', function minorRevisionReview() {
    const { reviewer } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.respondToInvitationAsReviewer('yes')
    cy.reload()
    cy.submitReview('Publish')
  })

  it('AcademicEditor makes recommendation to publish', function academicEditorMakesRecommendationToPublish() {
    const { academicEditor } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.academicEditorMakesRecommendation('Publish')
  })

  it('Triage Editor makes decision to reject', function rejectManuscript() {
    const { triageEditor, author, admin, statuses } = this

    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    return cy.triageEditorMakesDecision('Reject').then(() => {
      cy.checkStatus(statuses.rejected.triageEditor)

      cy.loginApi(author.email, author.password)
      cy.visit('/')
      cy.get(
        `[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`,
      ).click()
      cy.checkStatus(statuses.rejected.author)
        .get(`h3`)
        .contains('Editorial Comments')
        .should('be.visible')
      cy.contains(Cypress.env('triageEditorDecisionText'))

      cy.loginApi(admin.email, admin.password, true)
      cy.visit('/')
      cy.get('[data-test-id="undefined-filter"]')
        .first()
        .click()
        .then(() => {
          cy.get('[data-test-id="undefined-all"]').click({ force: true })
        })
      cy.get(
        `[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`,
      ).click()
      cy.checkStatus(statuses.rejected.admin)
        .get(`h3`)
        .contains('Editorial Comments')
        .should('be.visible')
      cy.contains(Cypress.env('triageEditorDecisionText'))
    })
  })
})
