describe('Triage Editor revokes AcademicEditor after reviewers send reports', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
  })

  it('Should accept invitation as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { statuses, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
    cy.respondToInvitationAsAcademicEditor('yes')
    cy.wait(4000)

    cy.checkStatus(statuses.academicEditorAssigned.academicEditor)
  })

  it('Invite reviewers as AcademicEditor', function inviteReviewer() {
    const { statuses, reviewer, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit(`/`)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.inviteReviewer({ reviewer, academicEditor })
    cy.checkStatus(statuses.reviewersInvited.academicEditor)
  })

  it('Give a review for publish as Reviewer', function submitMajReview() {
    const { reviewer, academicEditor, statuses } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('yes')
    cy.reload()
    cy.checkStatus(statuses.underReview.reviewer)

    cy.submitReview('Publish')

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit(`/`)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
    cy.checkStatus(statuses.reviewCompleted.academicEditor)
  })

  it('Triage Editor revokes AcademicEditor after the reviewers submitted reports', function TriageEditorRevokesAcademicEditorAfterReviewersSubmitedReports() {
    const { triageEditor, academicEditor, reviewer, statuses } = this
    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit(`/`)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.underReview.triageEditor)

    cy.contains('Reviewer Details & Reports').click()
    cy.findByText('Recommendation').contains('Recommendation')
    cy.findByText('Report').contains('Report')

    cy.contains('Your Editorial Decision').click()
    cy.contains('Please select').click()
    cy.contains('Reject')
    cy.contains('Please select').click({ force: true })

    cy.triageEditorRevokesAcademicEditor()
    cy.checkStatus(statuses.submitted.triageEditor)

    cy.contains('Reviewer Details & Reports').should('not.be.visible')
    cy.contains('Assign Academic Editor')

    cy.get('button')
      .contains('INVITE')
      .click({ force: true })

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit(`/`)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).should(
      'not.be.visible',
    )

    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit(`/`)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).should(
      'not.be.visible',
    )

    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).should(
      'not.be.visible',
    )
  })
})
