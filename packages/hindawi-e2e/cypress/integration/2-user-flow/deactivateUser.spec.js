describe('Deactivate User', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
  })

  it('Deactivate succesfully a user', function deactivateUser() {
    const { admin } = this

    cy.visit('/')
    cy.get('[data-test-id="login-email"]').type(
      `${Cypress.env('email')}${admin.email}`,
    )
    cy.get('[data-test-id="login-password"]').type(`${Cypress.env('password')}`)
    cy.get('[data-test-id="login-button"]').click()

    cy.get('[data-test-id="admin-menu-button"]').click()
    cy.get('[data-test-id="admin-dropdown-dashboard"]').click()
    cy.contains('Users').click()
    cy.get('.icn_icn_moreDefault')
      .eq(1)
      .click({ force: true })
    cy.get('[data-test-id="dropdown-option"]')
      .eq(1)
      .click({ force: true })
    cy.findByText('Are you sure you want to deactivate user?').click()
    cy.get('[data-test-id="modal-confirm"]').click()

    cy.get('#ps-modal-root').should('be.empty')
  })
})
