const academicEditorMakesRecommendation = recommendation => {
  switch (recommendation) {
    case 'Publish': {
      cy.contains('Your Editorial Recommendation').click()
      cy.contains(recommendation).should('be.visible')
      cy.contains('Choose in the list').click()
      cy.get('[role="option"]')
        .contains(recommendation)
        .click()
      cy.contains('Submit Recommendation').click()
      cy.get(`[data-test-id="modal-confirm"]`).click()
      break
    }

    case 'Reject': {
      cy.contains(`Reviewer Reports`)
        .should('be.visible')
        .click()
      cy.contains('Your Editorial Recommendation').click()
      cy.contains('Choose in the list').click()
      cy.get('[role="option"]')
        .contains('Reject')
        .click()

      Cypress.env('academicEditorRecommendationText-author', 'loreimpsum!')
      Cypress.env(
        'academicEditorRecommendationText-triageEditor',
        'dolorsitamet!',
      )

      cy.get(
        '[data-test-id="editorial-recommendation-message-for-author"] textarea',
      ).type(Cypress.env('academicEditorRecommendationText-author'))

      cy.get(
        '[data-test-id="editorial-recommendation-message-for-triage-editor"] textarea',
      )
        .should('be.visible')
        .type(Cypress.env('academicEditorRecommendationText-triageEditor'))

      cy.get('[data-test-id="button-editorial-recommendation-submit')
        .should('be.visible')
        .click()

      cy.get('[data-test-id="modal-confirm"]')
        .should('be.visible')
        .click()
      break
    }

    case 'Minor Revision': {
      cy.contains('Your Editorial Recommendation')
        .should('be.visible')
        .click()

      Cypress.env('academicEditorRecommendationText', 'loreimpsum!')

      cy.get(`[role="listbox"]`)
        .find('button')
        .contains('Choose in the list')
        .click()

      cy.get('[role="option"]')
        .contains(recommendation)
        .should('be.visible')
        .click()

      cy.get(
        '[data-test-id="editorial-recommendation-message-for-author"] textarea',
      ).type(Cypress.env('academicEditorRecommendationText'))

      cy.contains('Request Revision').click()

      cy.contains('Minor Revision Requested?')

      cy.get('[data-test-id="modal-confirm"]').click()
      break
    }

    case 'Major Revision': {
      cy.contains('Your Editorial Recommendation')
        .should('be.visible')
        .click()

      Cypress.env('academicEditorRecommendationText', 'loreimpsum!')

      cy.get(`[role="listbox"]`)
        .find('button')
        .contains('Choose in the list')
        .click()

      cy.get('[role="option"]')
        .contains(recommendation)
        .should('be.visible')
        .click()

      cy.get(
        '[data-test-id="editorial-recommendation-message-for-author"] textarea',
      ).type(Cypress.env('academicEditorRecommendationText'))

      cy.contains('Request Revision').click()

      cy.contains('Major Revision Requested?')

      cy.get('[data-test-id="modal-confirm"]').click()
      break
    }

    default: {
      cy.get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('be.visible')
        .click()
      cy.get('[data-test-id="contextual-box-editorial-comments"]')
        .find('[role="listbox"]')
        .click()
      cy.get('[data-test-id="contextual-box-editorial-comments"]')
        .find('[role="option"]')
        .contains(recommendation)
        .click()

      Cypress.env('academicEditorRecommendationText', 'loreimpsum!')

      cy.get('[data-test-id="button-editorial-recommendation-submit')
        .should('be.visible')
        .click()

      cy.get(
        '[data-test-id="contextual-box-editorial-recommendation-response"]',
      )
        .should('be.visible')
        .should('contain', 'Required')

      cy.get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('be.visible')
        .contains('Your Editorial Recommendation')
        .click()

      cy.get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('be.visible')
        .click()

      cy.get('[data-test-id="button-editorial-recommendation-submit')
        .should('be.visible')
        .click()

      cy.get(
        '[data-test-id="contextual-box-editorial-recommendation-response"]',
      )
        .should('be.visible')
        .should('contain', 'Required')

      cy.get('[data-test-id="editorial-recommendation-message-for-author"]')
        .should('be.visible')
        .type(Cypress.env('academicEditorRecommendationText'))
      cy.get('[data-test-id="button-editorial-recommendation-submit')
        .should('be.visible')
        .click()
      cy.get('[data-test-id="modal-confirm"]')
        .should('be.visible')
        .click()
    }
  }
}
Cypress.Commands.add(
  'academicEditorMakesRecommendation',
  academicEditorMakesRecommendation,
)
