const respondToInvitationAsReviewer = response => {
  switch (response) {
    case 'DECLINE': {
      cy.get(`[value=${response}]`).click()
      cy.get(`[type="button"]`)
        .contains('Respond to Invitation')
        .click()
      cy.contains('Decline this invitation?').should('be.visible')
      cy.get('[data-test-id="modal-confirm"]').click()
      break
    }
    default: {
      cy.get(`[value=${response}]`).click()
      cy.get(`[type="button"]`)
        .contains('Respond to Invitation')
        .click()
      cy.contains('Please confirm your agreement.').should('be.visible')
      cy.get('[data-test-id="modal-confirm"]').click()
    }
  }
}

Cypress.Commands.add(
  'respondToInvitationAsReviewer',
  respondToInvitationAsReviewer,
)
